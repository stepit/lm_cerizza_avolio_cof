# Council of Four
lm_6: Pietro Avolio and Stefano Cerizza
### Implemented technologies
Standard features:

* Regole Complete
* RMI
* Socket
* CLI
* GUI

Advanced features:

* Ripristino sessione giocatore

### Run instructions
Project is divided into three maven modules:

* server
* client
* cs_commons

Server main method can be found in the following class: 
```
it.stepit.server.controller.Server
``` 
When started the server creates socket and RMI connections listeners on localhost address using 1099 and 29999 ports.


Client main method can be found in the following class:
```
it.stepit.client.Main
```
When started the client asks the user to select between CLI or GUI interfaces.

### JAR files
Maven _mvn package_ command will produce jar with full dependencies as specified in pom.xml file.

After packaging, client JAR can be found in the following directory:
```
client/target/client-jar-with-dependencies.jar
```

After packaging, server JAR can be found in the following directory:
```
server/target/server-jar-with-dependencies.jar
```


JAR files can be run using the following command:
```
java -jar *jar-file-name*
```
Main methods will be directly executed.
Generated JAR files can also be downloaded directly from [download tab](https://bitbucket.org/stepit/lm_cerizza_avolio_cof/downloads) in BitBucket repository.

### Development environment
Project was developed under Windows 10 O.S.
GUI was tested and is optimized for Full HD (1920x1280) resolutions. Results on different resolution are not guaranteed.