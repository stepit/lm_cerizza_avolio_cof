package it.stepit.client;

import com.google.common.collect.ImmutableList;
import it.stepit.client.communication.UserLoginInterface;
import it.stepit.client.communication.rmi.RmiInitialization;
import it.stepit.client.communication.socket.SocketInitialization;
import it.stepit.client.exceptions.ClientException;
import it.stepit.client.exceptions.CommandNotFoundException;
import it.stepit.client.ui.cli.CommandLine;
import it.stepit.client.ui.cli.commands.CommandsFactory;
import it.stepit.cscommons.communication.metaobjects.*;
import it.stepit.cscommons.communication.metaobjects.map.MetaRegion;
import it.stepit.cscommons.communication.metaobjects.map.MetaTown;
import it.stepit.cscommons.exceptions.ClientServerCommunicationException;
import it.stepit.cscommons.game.GameUpdate;
import it.stepit.cscommons.game.RewardParamsRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.util.*;

public class CliClient extends Client {
    private static final Logger LOGGER = LoggerFactory.getLogger(CliClient.class);
    private Thread commandListeningThread;

    private CliClient(){
    }

    public static Client initialize(){
        if( Client.instance == null ){
            return new CliClient();
        }

        return Client.instance;
    }

    @Override
    public void begin() {
        CommandLine.println("One more choice... SOCKET or RMI?");

        if( "socket".equalsIgnoreCase( CommandLine.getInput() ) ){
            try {
                new SocketInitialization().start();
            } catch (ClientServerCommunicationException e) {
                LOGGER.error("Fatal exception during Socket initialization", e);
            }

        }else{
            CommandLine.println("Ok, let's go...");

            try {
                new RmiInitialization().start();
            } catch (ClientServerCommunicationException e) {
                LOGGER.error("Fatal exception during RMI initialization", e);
            }
        }
    }

    @Override
    public void onServerDisconnection(Throwable t) {
        LOGGER.error("Server disconnection", t);
        System.exit(0);
    }

    @Override
    public void loginRequest(UserLoginInterface loginInterface) {
        CommandLine.println("Ok dude, we need to authenticate now. What's your username?");
        try {
            loginInterface.doLogin( CommandLine.getInput() );
        } catch (ClientServerCommunicationException e) {
            LOGGER.error("Unable to communicate with server", e);
        }
    }

    @Override
    public void loginResult(boolean success, String message, UserLoginInterface loginInterface) {
        if( success ){
            CommandLine.clean();
            if("RJN".equals(message)) //SHITTY!!
                CommandLine.println("Joining your previous game. Please wait...");
            else
                this.startupMenu();
        }else{
            CommandLine.println("Authentication faild: " + message );
            this.loginRequest(loginInterface);
        }
    }

    @Override
    public void onGameUpdate(GameUpdate update) throws ClientException {
        MetaGame newMetaGame = update.getGame();
        this.setGame(newMetaGame);

        this.setPlayer( newMetaGame.getPlayer( this.getUsername() ) );

        switch(newMetaGame.getGameState()){
            case STATE_UNSTARTED:
                CommandLine.println(update.getMessage());
                CommandLine.println(newMetaGame.getPlayersCount() + " players in the room...");
                break;

            case STATE_JUST_STARTED:
                CommandLine.clean();
                CommandLine.printMap(newMetaGame);
                CommandLine.println(CommandLine.ANSI_YELLOW, "The game starts! " + newMetaGame.getGameStatus().getCurrentPlayer().getUsername() + " moves!");
                this.startCommandsListening();
                break;

            case STATE_STARTED:
                CommandLine.println(CommandLine.ANSI_YELLOW, update.getMessage());
                this.startCommandsListening();
                break;

            case STATE_ABORTED:
                CommandLine.println(CommandLine.ANSI_YELLOW, update.getMessage());
                CommandLine.println("Game aborted. Can't continue with no players! :(");
                break;

            case STATE_ENDED:
                CommandLine.println(CommandLine.ANSI_CYAN, update.getMessage());
                CommandLine.println();
                this.printFinalLeaderboard();
                break;

            default:
                LOGGER.warn("Unhandled GameStatusCode in GameUpdate MetaGame");
                break;
        }
    }

    private void startupMenu(){
        CommandLine.println("Welcome to Council of Four, " + this.getUsername());
        CommandLine.println();
        CommandLine.println("Please, make a selection:");
        CommandLine.println("0 - Start a new game");
        CommandLine.println("1 - Join a game");
        CommandLine.println("2 - Exit");

        try {
            switch (CommandLine.getIntInput(0, 2)) {
                case 0:
                    this.createGame();
                    break;

                case 1:
                    this.joinGame();
                    break;


                case 2:
                default:
                    CommandLine.println("Bye bye! :)");
                    System.exit(0);
                    break;
            }
        } catch ( ClientServerCommunicationException e ){
            LOGGER.error("Fatal exception while communicating with server", e);
            System.exit(0);
        }
    }

    private void createGame() throws ClientServerCommunicationException {
        CommandLine.print("Choose a name for the game: ");
        String gameName = CommandLine.getInput();

        CommandLine.println("Choose a game configuration:");
        ArrayList<String> availableGameConfigurations = new ArrayList<>( this.outInterface.getAvailableGameConfigurations() );
        String selectedGameConfiguration = availableGameConfigurations.get( CommandLine.createSingleSelectionList( availableGameConfigurations ));

        this.setGame( this.outInterface.createGame(gameName, selectedGameConfiguration) );
        CommandLine.clean();
        CommandLine.println("Waiting for the game to start...");

    }

    private void joinGame() throws ClientServerCommunicationException {
        MetaGame game = this.outInterface.joinGame();

        if( game == null ){
            CommandLine.println("No games to join. Please, create a new one");
            this.createGame();
        }else{
            CommandLine.clean();
            CommandLine.println("Waiting for the game to start...");
            CommandLine.println( game.getPlayersCount() + " players in the room...");
        }
    }

    private void startCommandsListening(){
        if( this.commandListeningThread == null || !this.commandListeningThread.isAlive() ) {
            CommandLine.println("Use /help for a list of available commands");

            Runnable commandListeningRunnable = () -> {
                String input;
                while ((input = CommandLine.getInput()) != null && !Client.instance.getGame().isEndend() && !Client.instance.getGame().isAborted() ) {
                    handleInput(input);
                }
            };

            this.commandListeningThread = new Thread( commandListeningRunnable );
            this.commandListeningThread.start();
        }
    }

    private void handleInput(String input){
        if (input.startsWith("/")) {
            String[] arguments = input.substring(1).split(" ");
            try {
                CommandsFactory.getCommand(arguments[0], Arrays.copyOfRange(arguments, 1, arguments.length)).perform();
            } catch (CommandNotFoundException e) {
                CommandLine.println("Use /help for a list of all available commands");
            }
        } else {
            CommandLine.println("Use /help for a list of all available commands");
        }
    }

    private void printFinalLeaderboard(){
        for( Map.Entry<String, Integer> entry : this.getGame().getGameStatus().getFinalLeaderboard().entrySet() ){
            CommandLine.println( String.format( "%s %s", entry.getKey(), entry.getValue() ) );
        }
    }

    /***************************
     * USER INTERACTION METHODS *
     ***************************/

    @Override
    public MetaBalcony selectBalcony(@Nullable String requestMessage) {
        if(requestMessage != null && !requestMessage.isEmpty())
            CommandLine.println(requestMessage);

        List<String> balconies =  new ArrayList<>();
        balconies.add("King's balcony:\t" + this.getGame().getGameStatus().getKing().getBalcony().getTextDescriptor());
        for(MetaRegion r : this.getGame().getGameStatus().getGameMap().getRegions()){
            balconies.add(r.getName() + " balcony:\t" + r.getBalcony().getTextDescriptor());
        }

        int userSelection = CommandLine.createSingleSelectionList( balconies );
        if( userSelection == 0 )
            return this.getGame().getGameStatus().getKing().getBalcony();
        else
            return this.getGame().getGameStatus().getGameMap().getRegions().get(userSelection-1).getBalcony();
    }

    @Override
    public MetaCouncillor selectFreeCouncillor(@Nullable String requestMessage) {
        if(requestMessage != null && !requestMessage.isEmpty())
            CommandLine.println(requestMessage);

        List<MetaCouncillor> councillors = ImmutableList.copyOf( this.getGame().getGameStatus().getFreeCouncillors() );
        return councillors.get( CommandLine.createSingleSelectionList(councillors) );
    }

    @Override
    public MetaRegion selectRegion(@Nullable String requestMessage) {
        if(requestMessage != null && !requestMessage.isEmpty())
            CommandLine.println(requestMessage);

        ImmutableList<MetaRegion> regions = this.getGame().getGameStatus().getGameMap().getRegions();
        return regions.get( CommandLine.createSingleSelectionList(regions) );
    }

    @Override
    public MetaBusinessCard selectRegionBusinessCard(MetaRegion region, @Nullable String requestMessage) {
        List<MetaBusinessCard> regionBuyableBusinessCards = region.getBuyableBusinessCards();
        if(requestMessage != null && !requestMessage.isEmpty())
            CommandLine.println(requestMessage);

        if(regionBuyableBusinessCards.isEmpty()){
            CommandLine.print(CommandLine.ANSI_RED, "There are no buyable business cards in selected region");
            return null;
        }else{
            return region.getBuyableBusinessCards().get( CommandLine.createSingleSelectionList(region.getBuyableBusinessCards()) );
        }
    }

    @Override
    public Collection<MetaPoliticCard> selectPoliticCards(@Nullable String requestMessage) {
        if(requestMessage != null && !requestMessage.isEmpty())
            CommandLine.println(requestMessage);

        Collection<MetaPoliticCard> result = new HashSet<>();
        ImmutableList<MetaPoliticCard> playerPoliticCard = ImmutableList.copyOf(Client.instance.getPlayer().getPoliticsCards());

        if(playerPoliticCard.isEmpty()){
            CommandLine.println(CommandLine.ANSI_RED, "You don't have any politic card");
        }else {
            for (Integer i : CommandLine.createMultipleSelectionList(playerPoliticCard, 1, 4)) {
                result.add(playerPoliticCard.get(i));
            }
        }

        return result;
    }

    @Override
    public List<MetaTown> selectTown(int amount, String requestMessage){
        if(amount <= 0)
            throw new IllegalArgumentException("Illegal negative or zero value for amount param");

        if(requestMessage != null && !requestMessage.isEmpty())
            CommandLine.println(requestMessage);

        ArrayList<MetaTown> res = new ArrayList<>();
        ImmutableList<MetaTown> towns = ImmutableList.copyOf(this.getGame().getGameStatus().getGameMap().getTowns());
        if(amount == 1) {
            res.add(towns.get(CommandLine.createSingleSelectionList(towns)));
        }else{
            for( Integer i : CommandLine.createMultipleSelectionList(towns, amount, amount) ){
                res.add( towns.get(i) );
            }
        }

        return res;
    }

    @Override
    public MetaBusinessCard selectBusinessCard(@Nullable String requestMessage) {
        ImmutableList<MetaBusinessCard> playerBusinessCards = ImmutableList.copyOf(this.getPlayer().getBusinessCards());
        if(playerBusinessCards.isEmpty()){
            CommandLine.println(CommandLine.ANSI_RED, "You don't have any business card");
            return null;

        }else{
            if(requestMessage != null && !requestMessage.isEmpty())
                CommandLine.println(requestMessage);

            return playerBusinessCards.get( CommandLine.createSingleSelectionList(playerBusinessCards) );
        }
    }

    @Override
    public void handleRewardParamsRequest(RewardParamsRequest request) {
        LOGGER.debug("Received request for active rewards");
    }
}