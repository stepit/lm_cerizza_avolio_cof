package it.stepit.client;

import com.google.common.collect.ImmutableList;
import it.stepit.client.communication.OutboundCommunicationInterface;
import it.stepit.client.communication.UserLoginInterface;
import it.stepit.client.exceptions.ClientException;
import it.stepit.cscommons.communication.metaobjects.*;
import it.stepit.cscommons.communication.metaobjects.map.MetaRegion;
import it.stepit.cscommons.communication.metaobjects.map.MetaTown;
import it.stepit.cscommons.game.GameUpdate;
import it.stepit.cscommons.game.RewardParamsRequest;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public abstract class Client {
    public static Client instance; //NOSONAR
    private String username;
    private MetaPlayer player;
    private MetaGame game;
    private ArrayList<String> gameMessages;
    protected OutboundCommunicationInterface outInterface;

    public Client(){
        instance = this;
        this.gameMessages = new ArrayList<>();
    }

    public final OutboundCommunicationInterface getOutInterface() {
        return outInterface;
    }

    public final void setOutInterface(OutboundCommunicationInterface outInterface){
        this.outInterface = outInterface;
    }

    public final String getUsername(){
        return this.username;
    }

    public final void setUsername(String username){
        this.username = username.toLowerCase();
    }

    public final MetaGame getGame() {
        return game;
    }

    public final void setGame(MetaGame game){
        this.game = game;
    }

    public final MetaPlayer getPlayer() {
        return player;
    }

    public final void setPlayer(MetaPlayer player) throws ClientException {
        if(player == null)
            throw new ClientException("Can't set client player to null value");

        this.player = player;
    }

    public final void addGameMessage(String message){
        this.gameMessages.add(message);
    }

    public final List<String> getGameMessages(){
        return ImmutableList.copyOf( this.gameMessages );
    }

    /********************
     * ABSTRACT METHODS *
     ********************/

    public abstract void begin();

    /**
     * Handles a login request. Asks the user for username (maybe one day password)
     * then calls doLogin on the passed loginInterface.
     *
     * @param loginInterface The loginInterface that requested the login
     */
    public abstract void loginRequest(UserLoginInterface loginInterface);

    /**
     * Receives login result.
     *
     * @param success A boolean value indicating if the login was successfully or not
     * @param message A textual message
     * @param loginInterface The loginInterface that called the method. Need to recall <italic>loginRequest(...)</italic>
     */
    public abstract void loginResult(boolean success, @Nullable String message, UserLoginInterface loginInterface);

    /**
     * Event fired when client losed connection with server.
     * @param t Throwable exception raised when the disconnection happens
     */
    public abstract void onServerDisconnection(Throwable t);

    /**
     * Event fired when the client receives a new GameUpdate from the server.
     * @param update The GameUpdate object
     * @throws ClientException
     */
    public abstract void onGameUpdate(GameUpdate update) throws ClientException;

    /**
     * Asks the usere to select a balcony.
     *
     * @return The selected balcony. Null if the user didn't select any balcony or if selected more than one balcony.
     */
    public abstract MetaBalcony selectBalcony(@Nullable String requestMessage);

    /**
     * Asks the use to select a councillor from free councillors
     *
     * @return The selected councillor. Null if the user didn't select any free councillor or if selected more than one free councillor
     */
    public abstract MetaCouncillor selectFreeCouncillor(@Nullable String requestMessage);

    /**
     * Asks the user to select a region.
     *
     * @return The selected region. Null if the user didn't select any region or if selected more than one region.
     */
    public abstract MetaRegion selectRegion(@Nullable String requestMessage);

    /**
     * Asks the user to pick a business card from region
     *
     * @param region The region to extract business cards from
     * @return The selected MetaBusinessCard. Null if the user didn't select any business card or if selected more than one business card.
     */
    public abstract MetaBusinessCard selectRegionBusinessCard(MetaRegion region, @Nullable String requestMessage);

    /**
     * Asks the user to pick from 1 to 4 of it's politic cards
     *
     * @return A collection (maybe empty) of selected politic cards
     */
    public abstract Collection<MetaPoliticCard> selectPoliticCards(@Nullable String requestMessage);

    /**
     * Asks the user to select an <integer>amount</integer> of towns
     *
     * @return A list of selected towns. Maybe empty.
     */
    public abstract List<MetaTown> selectTown(int amount, @Nullable String requestMessage);

    /**
     * Asks the user to pikc one of its business cards
     *
     * @return The select Business Card, null if the player has no business cards or if he selected more than one business card.
     */
    public abstract MetaBusinessCard selectBusinessCard(@Nullable String requestMessage);

    /**
     * Handles a request for rewards that requires user selections.
     * @param request The request to handle
     */
    public abstract void handleRewardParamsRequest(RewardParamsRequest request);
}
