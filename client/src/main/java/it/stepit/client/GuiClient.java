package it.stepit.client;

import it.stepit.client.communication.UserLoginInterface;
import it.stepit.client.exceptions.ClientException;
import it.stepit.client.ui.gui.Gui;
import it.stepit.client.ui.gui.controllers.GamePaneController;
import it.stepit.client.ui.gui.controllers.StartPaneController;
import it.stepit.cscommons.communication.metaobjects.*;
import it.stepit.cscommons.communication.metaobjects.map.MetaRegion;
import it.stepit.cscommons.communication.metaobjects.map.MetaTown;
import it.stepit.cscommons.exceptions.ClientServerCommunicationException;
import it.stepit.cscommons.game.GameUpdate;
import it.stepit.cscommons.game.RewardParamsRequest;
import javafx.application.Application;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class GuiClient extends Client {
    private static final Logger LOGGER = LoggerFactory.getLogger(GuiClient.class);
    private StartPaneController startPaneController;
    private GamePaneController gamePaneController;

    private GuiClient(){
    }

    public static Client initialize(){
        if( Client.instance == null ){
            return new GuiClient();
        }

        return Client.instance;
    }

    @Override
    public void begin() {
        Application.launch(Gui.class, (String) null);
    }

    @Override
    public void loginRequest(UserLoginInterface loginInterface) {
        try {
            loginInterface.doLogin( this.startPaneController.getUserInputUsername() );
        } catch (ClientServerCommunicationException e) {
            LOGGER.error("Unable to connect to the server", e);
            this.startPaneController.onLoginResult(false, "Unable to connect to the server");
        }
    }

    @Override
    public void loginResult(boolean success, @Nullable String message, UserLoginInterface loginInterface) {
        if(success && "RJN".equals(message))
            this.startPaneController.onGameRejoin();
        else
            this.startPaneController.onLoginResult(success, message);
    }

    @Override
    public void onServerDisconnection(Throwable t) {
        if( this.gamePaneController != null ){
            this.gamePaneController.onServerDisconnection();
        }else if( this.startPaneController != null ){
            this.startPaneController.onServerDisconnection();
        }
    }

    @Override
    public void onGameUpdate(GameUpdate update) throws ClientException {
        MetaGame oldMetaGame = this.getGame();
        MetaGame newMetaGame = update.getGame();
        this.setGame(newMetaGame);

        this.setPlayer( newMetaGame.getPlayer( this.getUsername() ) );

        switch(newMetaGame.getGameState()){
            case STATE_UNSTARTED:
                this.startPaneController.onUnstartedGameUpdate(newMetaGame.getPlayersCount(), update.getMessage());
                break;

            case STATE_JUST_STARTED:
                this.addGameMessage( "Game started! "  + this.getGame().getGameStatus().getCurrentPlayer().getUsername() + " moves" );
                this.startPaneController.switchToGamePane();
                break;

            case STATE_STARTED:
                this.addGameMessage(update.getMessage());
                if( this.gamePaneController == null ) {
                    this.startPaneController.switchToGamePane();
                } else {
                    this.gamePaneController.addMessage(update.getMessage(), this.getGameMessages().size() - 1);
                    this.gamePaneController.redrawNeeded(oldMetaGame, newMetaGame);
                }
                break;

            case STATE_ABORTED:
                this.gamePaneController.onGameAborted();
                break;

            case STATE_ENDED:
                this.gamePaneController.onGameEnded();
                break;

            default:
                LOGGER.warn("Unhandled GameStatusCode in GameUpdate metaGame");
                break;
        }
    }

    @Override
    public MetaBalcony selectBalcony(@Nullable String requestMessage) {
        Collection<MetaBalcony> selectedRegions = this.gamePaneController.getSelectedElements(MetaBalcony.class);
        if(selectedRegions.size() != 1)
            return null;
        else
            return selectedRegions.iterator().next();
    }

    @Override
    public MetaCouncillor selectFreeCouncillor(@Nullable String requestMessage) {
        Collection<MetaCouncillor> selectedRegions = this.gamePaneController.getSelectedElements(MetaCouncillor.class);
        if(selectedRegions.size() != 1)
            return null;
        else
            return selectedRegions.iterator().next();
    }

    @Override
    public MetaRegion selectRegion(@Nullable String requestMessage) {
        Collection<MetaRegion> selectedRegions = this.gamePaneController.getSelectedElements(MetaRegion.class);
        if(selectedRegions.size() != 1)
            return null;
        else
            return selectedRegions.iterator().next();
    }

    @Override
    public MetaBusinessCard selectRegionBusinessCard(MetaRegion region, @Nullable String requestMessage) {
        return selectBusinessCard(requestMessage);
    }

    @Override
    public Collection<MetaPoliticCard> selectPoliticCards(@Nullable String requestMessage) {
        return this.gamePaneController.getSelectedElements(MetaPoliticCard.class);
    }

    @Override
    public List<MetaTown> selectTown(int amount, @Nullable String requestMessage) {
        Collection<MetaTown> selectedTowns = this.gamePaneController.getSelectedElements(MetaTown.class);
        return new ArrayList<>( selectedTowns );
    }

    @Override
    public MetaBusinessCard selectBusinessCard(@Nullable String requestMessage) {
        Collection<MetaBusinessCard> selectedBusinessCards = this.gamePaneController.getSelectedElements(MetaBusinessCard.class);

        if(selectedBusinessCards.size() == 1)
            return new ArrayList<>( selectedBusinessCards ).get(0);
        else
            return null;
    }

    @Override
    public void handleRewardParamsRequest(RewardParamsRequest request) {
        LOGGER.debug("Received request for active rewards");
    }

    /**********************************/
    /* Specific methods for CliClient */
    /**********************************/

    public void setStartPanelControl(StartPaneController controller){
        this.startPaneController = controller;
    }

    public void setGamePaneController(GamePaneController controller) {
        this.gamePaneController = controller;
    }

    public void onStageClosed(){
        System.exit(0); //NOSONAR
    }
}
