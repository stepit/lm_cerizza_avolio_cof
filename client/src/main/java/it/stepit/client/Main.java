package it.stepit.client;

import it.stepit.cscommons.exceptions.ClientServerCommunicationException;
import it.stepit.client.ui.cli.CommandLine;

import java.io.IOException;

public class Main {

    private Main(){ }

    public static void main(String[] args) throws IOException, ClientServerCommunicationException {
        CommandLine.println("Welcome to COF Client!");
        CommandLine.println();
        CommandLine.println("First of all: CLI or GUI?");

        if( "cli".equalsIgnoreCase( CommandLine.getInput() )){
            CommandLine.println("Ok CLI... Here we go!");
            CliClient.initialize().begin();

        }else{
            CommandLine.println("Ok GUI... Here we go!");
            GuiClient.initialize().begin();
        }
    }
}
