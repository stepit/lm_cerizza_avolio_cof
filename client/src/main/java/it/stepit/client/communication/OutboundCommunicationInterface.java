package it.stepit.client.communication;

import it.stepit.cscommons.communication.metaobjects.MetaGame;
import it.stepit.cscommons.communication.metaobjects.MetaGameAction;
import it.stepit.cscommons.communication.metaobjects.MetaMarketOffer;
import it.stepit.cscommons.exceptions.ClientServerCommunicationException;
import it.stepit.cscommons.utils.RichBoolean;

import java.util.Set;

public interface OutboundCommunicationInterface {

    /**
     * Get all available game configurations from the server.
     *
     * @return A list of available configurations
     * @throws ClientServerCommunicationException
     */
    Set<String> getAvailableGameConfigurations() throws ClientServerCommunicationException;

    /**
     * Asks the server to create a new game
     *
     * @param gameName A name for the game (currently unused but could be useful)
     * @param configuration A configuration name
     * @return The newly created game
     * @throws ClientServerCommunicationException
     */
    MetaGame createGame( String gameName, String configuration ) throws ClientServerCommunicationException;

    /**
     * Asks the server to join into an existing game
     *
     * @return An unstarted game if there is any, null otherwise
     * @throws ClientServerCommunicationException
     */
    MetaGame joinGame() throws ClientServerCommunicationException;

    /**
     * Delivers a game action to the server
     *
     * @return A richboolean indicating the result of the action
     * @throws ClientServerCommunicationException
     */
    RichBoolean takeGameAction(MetaGameAction gameAction) throws ClientServerCommunicationException;

    /**
     * Delivers a market offer to the server
     *
     * @param marketOffer The market offer to deliver
     * @return A richboolean idcating the result of the deliver
     * @throws ClientServerCommunicationException
     */
    RichBoolean sendMarketOffer(MetaMarketOffer marketOffer) throws ClientServerCommunicationException;

    /**
     * Requests market buyable offers
     *
     * @return A set containing all market buyable actions
     */
    Set<MetaMarketOffer> getMarketOffers() throws ClientServerCommunicationException;

    /**
     * Sends a request to buy a market offer.
     *
     * @param marketOffer The market offer to buy
     * @return A RichBoolean containing the result of the request
     * @throws ClientServerCommunicationException
     */
    RichBoolean buyMarketOffer(MetaMarketOffer marketOffer) throws ClientServerCommunicationException;

    /**
     * Sends the request to pass market mode buy phase
     * @return A RichBoolean containing the result of the request
     * @throws ClientServerCommunicationException
     */
    RichBoolean passMarketBuyPhase() throws ClientServerCommunicationException;
}
