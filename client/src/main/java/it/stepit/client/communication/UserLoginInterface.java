package it.stepit.client.communication;

import it.stepit.cscommons.exceptions.ClientServerCommunicationException;

@FunctionalInterface
public interface UserLoginInterface {
    void doLogin(String username) throws ClientServerCommunicationException;
}
