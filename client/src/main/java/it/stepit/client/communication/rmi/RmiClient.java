package it.stepit.client.communication.rmi;

import it.stepit.client.Client;
import it.stepit.client.exceptions.ClientException;
import it.stepit.cscommons.communication.metaobjects.map.MetaTown;
import it.stepit.cscommons.communication.rmi.RmiClientInterface;
import it.stepit.cscommons.game.GameUpdate;
import it.stepit.cscommons.game.RewardParamsRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Collection;

/**
 * Here is where we receive all server messages.
 */
class RmiClient extends UnicastRemoteObject implements RmiClientInterface {
    private static final Logger LOGGER = LoggerFactory.getLogger(RmiClient.class);

    RmiClient() throws RemoteException {
        //RMI implementation requires this method.
    }

    @Override
    public boolean heartbeat() throws RemoteException{
        return true;
    }

    @Override
    public void updateGame(GameUpdate gameUpdate) {
        try {
            Client.instance.onGameUpdate(gameUpdate);
        } catch (ClientException e) {
            LOGGER.error("ClientException while delivering gameUpdate object", e);
        }
    }

    @Override
    public void requestRewardParams(RewardParamsRequest paramsRequest) throws RemoteException {
        Client.instance.handleRewardParamsRequest(paramsRequest);
    }
}

