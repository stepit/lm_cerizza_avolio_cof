package it.stepit.client.communication.rmi;

import it.stepit.client.Client;
import it.stepit.cscommons.exceptions.ClientServerCommunicationException;
import it.stepit.client.communication.UserLoginInterface;
import it.stepit.cscommons.communication.rmi.RmiHandshakeInterface;
import it.stepit.cscommons.communication.rmi.RmiServerInterface;
import it.stepit.cscommons.utils.RichBoolean;
import it.stepit.client.ui.ConnectionInitializationListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

public class RmiInitialization implements UserLoginInterface{
    private static final Logger LOGGER = LoggerFactory.getLogger(RmiInitialization.class);
    private static final String RMI_SERVER_ADDRESS = "127.0.0.1"; //NOSONAR
    private final ConnectionInitializationListener connectionInitializationListener;
    private RmiHandshakeInterface handshakeObject;

    public RmiInitialization() {
        this(null);
    }

    public RmiInitialization(ConnectionInitializationListener connectionInitializationListener) {
        this.connectionInitializationListener = connectionInitializationListener;
    }

    public void start() throws ClientServerCommunicationException {
        if( this.connectionInitializationListener != null )
            this.connectionInitializationListener.onInit();

        this.handshake();
    }

    private void handshake() throws ClientServerCommunicationException {
        try {
            this.handshakeObject = (RmiHandshakeInterface) Naming.lookup("rmi://" + RMI_SERVER_ADDRESS + "/handshake");

            if( this.handshakeObject != null && this.handshakeObject.hello() ){
                Client.instance.loginRequest(this);
            }else{
                LOGGER.error("handshakeObject is null or hello() returned false");
                if( this.connectionInitializationListener != null )
                    this.connectionInitializationListener.onError(null);

                throw new ClientServerCommunicationException();
            }

        } catch (NotBoundException | MalformedURLException | RemoteException e) {
            LOGGER.error("Error while obtaining RMI handshake object", e);
            if( this.connectionInitializationListener != null )
                this.connectionInitializationListener.onError(e);

            throw new ClientServerCommunicationException(e);
        }
    }

    @Override
    public void doLogin(String username) throws ClientServerCommunicationException {
        RmiClient rmiClient = null;
        try {
            rmiClient = new RmiClient();
            RichBoolean loginResult = this.handshakeObject.login(username, rmiClient);

            if(loginResult.getBooleanValue()) {
                Client.instance.setUsername(username);
                this.acquireServer();
            }

            Client.instance.loginResult( loginResult.getBooleanValue(), loginResult.getMessage(), this );
        } catch (RemoteException e) {
            LOGGER.error("RemoteException during RMI login", e);
            throw new ClientServerCommunicationException(e);
        }
    }

    private void acquireServer() throws RemoteException, ClientServerCommunicationException {
        RmiServerInterface rmiServer = this.handshakeObject.acquireServer( Client.instance.getUsername() );

        if( rmiServer != null ){
            Client.instance.setOutInterface( new RmiOutbound(rmiServer) );
            if( this.connectionInitializationListener != null )
                this.connectionInitializationListener.onSuccess();
        }else{
            throw new ClientServerCommunicationException("Unable to acquire server. Please, authenticate() first!");
        }
    }
}
