package it.stepit.client.communication.rmi;

import it.stepit.client.Client;
import it.stepit.client.communication.OutboundCommunicationInterface;
import it.stepit.cscommons.communication.metaobjects.MetaGame;
import it.stepit.cscommons.communication.metaobjects.MetaGameAction;
import it.stepit.cscommons.communication.metaobjects.MetaMarketOffer;
import it.stepit.cscommons.communication.rmi.RmiServerInterface;
import it.stepit.cscommons.exceptions.ClientServerCommunicationException;
import it.stepit.cscommons.utils.RichBoolean;

import java.rmi.RemoteException;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class RmiOutbound implements OutboundCommunicationInterface {
    private static final int SERVER_HEARTBEAT_SECONDS_RATE = 5;
    private final RmiServerInterface rmiServer;

    public RmiOutbound(RmiServerInterface rmiServer) {
        this.rmiServer = rmiServer;

        Runnable serverConnectionCheck = () -> {
            try {
                rmiServer.heartbeat();
            } catch (RemoteException e) {
                Client.instance.onServerDisconnection(e);
            }
        };

        ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
        scheduler.scheduleAtFixedRate(serverConnectionCheck, 0, SERVER_HEARTBEAT_SECONDS_RATE, TimeUnit.SECONDS);
    }

    @Override
    public Set<String> getAvailableGameConfigurations() throws ClientServerCommunicationException{
        try {
            return rmiServer.getAvailableGameConfigurations();
        } catch (RemoteException e) {
            throw new ClientServerCommunicationException(e);
        }
    }

    @Override
    public MetaGame createGame(String gameName, String configuration) throws ClientServerCommunicationException {
        try {
            return rmiServer.createGame(gameName, configuration);
        } catch (RemoteException e) {
            throw new ClientServerCommunicationException(e);
        }
    }

    @Override
    public MetaGame joinGame() throws ClientServerCommunicationException {
        try {
            return rmiServer.joinGame();
        } catch (RemoteException e) {
            throw new ClientServerCommunicationException(e);
        }
    }

    @Override
    public RichBoolean takeGameAction(MetaGameAction gameAction) throws ClientServerCommunicationException {
        try {
            return this.rmiServer.takeGameAction(gameAction);
        } catch (RemoteException e) {
            throw new ClientServerCommunicationException(e);
        }
    }

    @Override
    public RichBoolean sendMarketOffer(MetaMarketOffer marketOffer) throws ClientServerCommunicationException {
        try {
            return this.rmiServer.sendMarketOffer(marketOffer);
        } catch (RemoteException e) {
            throw new ClientServerCommunicationException(e);
        }
    }

    @Override
    public Set<MetaMarketOffer> getMarketOffers() throws ClientServerCommunicationException {
        try {
            return this.rmiServer.getMarketOffers();
        } catch (RemoteException e) {
            throw new ClientServerCommunicationException(e);
        }
    }

    @Override
    public RichBoolean buyMarketOffer(MetaMarketOffer marketOffer) throws ClientServerCommunicationException {
        try {
            return this.rmiServer.buyMarketOffer(marketOffer);
        } catch (RemoteException e) {
            throw new ClientServerCommunicationException(e);
        }
    }

    @Override
    public RichBoolean passMarketBuyPhase() throws ClientServerCommunicationException {
        try {
            return this.rmiServer.passMarketBuyPhase();
        } catch (RemoteException e) {
            throw new ClientServerCommunicationException(e);
        }
    }
}
