package it.stepit.client.communication.socket;

import it.stepit.client.Client;
import it.stepit.client.exceptions.ClientException;
import it.stepit.cscommons.communication.socket.SocketObject;
import it.stepit.cscommons.communication.socket.SocketObjectsBuffer;
import it.stepit.cscommons.exceptions.ClientServerCommunicationException;
import it.stepit.cscommons.game.GameUpdate;
import it.stepit.cscommons.game.RewardParamsRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.IOException;
import java.io.ObjectInputStream;

class SocketInbound extends Thread {
    private static final Logger LOGGER = LoggerFactory.getLogger(SocketInbound.class);
    private final ObjectInputStream socketIn;
    private final SocketObjectsBuffer objectsBuffer;
    private boolean started;

    SocketInbound(ObjectInputStream socketIn, SocketObjectsBuffer objectsBuffer) throws IOException {
        this.socketIn = socketIn;
        this.objectsBuffer = objectsBuffer;
    }

    @Override
    public void run(){
        Object input;

        try {
            while ( (input = socketIn.readObject()) != null ) {
                this.handleInput((SocketObject) input);
            }
        } catch (IOException e) {
            Client.instance.onServerDisconnection( e );
        } catch (ClassNotFoundException e) {
            LOGGER.warn("ClassNotFoundException", e);
        }
    }

    @Override
    public synchronized void start() {
        this.started = true;
        super.start();
    }

    public boolean isStarted() {
        return started;
    }

    private void handleInput(SocketObject input){
        switch(input.getObjectIdentifier()){
            case GAME_UPDATE:
                this.handleGameUpdate((GameUpdate) input.getTransportedObject());
                break;

            case SELECT_TOWNS_REQUEST:
                try {
                    ((SocketOutbound) Client.instance.getOutInterface()).sendSelectedTowns(Client.instance.selectTown((int) input.getTransportedObject(), null));
                } catch (ClientServerCommunicationException e) {
                    LOGGER.debug("Unable to send response to: " + input.getObjectIdentifier());
                }
                break;


            case REWARD_PARAMS_REQUEST:
                Client.instance.handleRewardParamsRequest( (RewardParamsRequest) input.getTransportedObject() );
                break;

            default:
                this.objectsBuffer.publishObject(input);
                break;
        }
    }

    private void handleGameUpdate(GameUpdate gameUpdate){
        try {
            Client.instance.onGameUpdate(gameUpdate);
        } catch (ClientException e) {
            LOGGER.error("Can't deliver game update", e);
        }
    }
}
