package it.stepit.client.communication.socket;

import it.stepit.client.Client;
import it.stepit.client.communication.UserLoginInterface;
import it.stepit.client.ui.ConnectionInitializationListener;
import it.stepit.cscommons.communication.socket.SocketObjectsBuffer;
import it.stepit.cscommons.exceptions.ClientServerCommunicationException;
import it.stepit.cscommons.utils.RichBoolean;

import java.io.*;
import java.net.Socket;

public class SocketInitialization implements UserLoginInterface{
    private static final String SOCKET_SERVER_ADDRESS = "127.0.0.1"; //NOSONAR
    private static final int SOCKET_SERVER_PORT = 29999;
    private final ConnectionInitializationListener connectionInitializationListener;

    private SocketOutbound socketOutbound;
    private SocketInbound socketInbound;

    public SocketInitialization(){
        this(null);
    }

    public SocketInitialization(ConnectionInitializationListener connectionInitializationListener) {
        this.connectionInitializationListener = connectionInitializationListener;
    }

    public void start() throws ClientServerCommunicationException {
        if( this.connectionInitializationListener != null )
            this.connectionInitializationListener.onInit();

        try {
            Socket socket = new Socket(SOCKET_SERVER_ADDRESS, SOCKET_SERVER_PORT); //NOSONAR

            SocketObjectsBuffer socketObjectsBuffer = new SocketObjectsBuffer();
            this.socketOutbound = new SocketOutbound(new ObjectOutputStream(socket.getOutputStream()), socketObjectsBuffer);
            this.socketInbound = new SocketInbound(new ObjectInputStream(socket.getInputStream()), socketObjectsBuffer);

        } catch (IOException e) {
            if( this.connectionInitializationListener != null )
                this.connectionInitializationListener.onError(e);

            throw new ClientServerCommunicationException("Unable to init socket", e);
        }

        Client.instance.loginRequest(this);
    }

    @Override
    public void doLogin(String username) throws ClientServerCommunicationException {
        if ( !this.socketInbound.isStarted() )
            this.socketInbound.start();

        RichBoolean loginResult = this.socketOutbound.performLogin(username);

        if( loginResult.getBooleanValue() ){
            if( this.connectionInitializationListener != null )
                this.connectionInitializationListener.onSuccess();

            Client.instance.setUsername( username );
            Client.instance.setOutInterface( this.socketOutbound );
        }

        Client.instance.loginResult( loginResult.getBooleanValue(), loginResult.getMessage(), this );
    }
}
