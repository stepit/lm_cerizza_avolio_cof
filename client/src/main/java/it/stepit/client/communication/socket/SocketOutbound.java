package it.stepit.client.communication.socket;

import it.stepit.client.communication.OutboundCommunicationInterface;
import it.stepit.cscommons.communication.metaobjects.MetaGame;
import it.stepit.cscommons.communication.metaobjects.MetaGameAction;
import it.stepit.cscommons.communication.metaobjects.MetaMarketOffer;
import it.stepit.cscommons.communication.metaobjects.map.MetaTown;
import it.stepit.cscommons.communication.socket.SocketObject;
import it.stepit.cscommons.communication.socket.SocketObjectIdentifier;
import it.stepit.cscommons.communication.socket.SocketObjectsBuffer;
import it.stepit.cscommons.exceptions.ClientServerCommunicationException;
import it.stepit.cscommons.utils.RichBoolean;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.Set;

class SocketOutbound implements OutboundCommunicationInterface{
    private final SocketObjectsBuffer objectsBuffer;
    private final ObjectOutputStream socketOut;

    SocketOutbound(ObjectOutputStream socketOut, SocketObjectsBuffer objectsBuffer) {
        this.objectsBuffer = objectsBuffer;
        this.socketOut = socketOut;
    }

    public RichBoolean performLogin(String username) throws ClientServerCommunicationException {
        this.sendObject(SocketObjectIdentifier.LOGIN_REQUEST, username);
        return (RichBoolean) this.objectsBuffer.waitForObject(SocketObjectIdentifier.LOGIN_RESPONSE).getTransportedObject();
    }

    @Override
    public Set<String> getAvailableGameConfigurations() throws ClientServerCommunicationException {
        this.sendObject(SocketObjectIdentifier.GAME_CONFIGURATIONS_REQUEST, null);
        return (Set<String>) this.objectsBuffer.waitForObject(SocketObjectIdentifier.GAME_CONFIGURATIONS_RESPONSE).getTransportedObject();
    }

    @Override
    public MetaGame createGame(String gameName, String configuration) throws ClientServerCommunicationException {
        HashMap<Integer, Object> objectParams = new HashMap<>();
        objectParams.put(SocketObject.PARAM_NEW_GAME_NAME, gameName);
        objectParams.put(SocketObject.PARAM_GAME_CONFIGURATION_NAME, configuration);

        this.sendObject(SocketObjectIdentifier.CREATE_GAME_REQEST, objectParams);
        return (MetaGame) this.objectsBuffer.waitForObject(SocketObjectIdentifier.GAME_RESPONSE).getTransportedObject();
    }

    @Override
    public MetaGame joinGame() throws ClientServerCommunicationException {
        this.sendObject(SocketObjectIdentifier.JOIN_GAME_REQUEST, null);
        return (MetaGame) this.objectsBuffer.waitForObject(SocketObjectIdentifier.GAME_RESPONSE).getTransportedObject();
    }

    @Override
    public RichBoolean takeGameAction(MetaGameAction gameAction) throws ClientServerCommunicationException {
        this.sendObject(SocketObjectIdentifier.GAME_ACTION, gameAction);
        return (RichBoolean) this.objectsBuffer.waitForObject(SocketObjectIdentifier.GAME_ACTION_RESULT).getTransportedObject();
    }

    @Override
    public RichBoolean sendMarketOffer(MetaMarketOffer marketOffer) throws ClientServerCommunicationException {
        this.sendObject(SocketObjectIdentifier.PUBLISH_MARKET_OFFER, marketOffer);
        return (RichBoolean) this.objectsBuffer.waitForObject(SocketObjectIdentifier.PUBLISH_MARKET_OFFER_RESULT).getTransportedObject();
    }

    @Override
    public Set<MetaMarketOffer> getMarketOffers() throws ClientServerCommunicationException {
        this.sendObject(SocketObjectIdentifier.REQUEST_MARKET_OFFERS, null);
        return (Set<MetaMarketOffer>) this.objectsBuffer.waitForObject(SocketObjectIdentifier.REQUEST_MARKET_OFFERS_RESPONSE).getTransportedObject();
    }

    @Override
    public RichBoolean buyMarketOffer(MetaMarketOffer marketOffer) throws ClientServerCommunicationException {
        this.sendObject(SocketObjectIdentifier.BUY_MARKET_OFFER, marketOffer);
        return (RichBoolean) this.objectsBuffer.waitForObject(SocketObjectIdentifier.BUY_MARKET_OFFER_RESPONSE).getTransportedObject();
    }

    @Override
    public RichBoolean passMarketBuyPhase() throws ClientServerCommunicationException {
        this.sendObject(SocketObjectIdentifier.PASS_MARKET_BUY_PHASE, null);
        return (RichBoolean) this.objectsBuffer.waitForObject(SocketObjectIdentifier.PASS_MARKET_BUY_PHASE_RESULT).getTransportedObject();
    }

    public void sendSelectedTowns(Collection<MetaTown> selectedTowns) throws ClientServerCommunicationException{
        this.sendObject(SocketObjectIdentifier.SELECT_TOWNS_RESPONSE, (Serializable) selectedTowns);
    }

    private void sendObject(SocketObjectIdentifier objectIdentifier, Serializable objectToTransport) throws ClientServerCommunicationException {
        SocketObject socketObject = new SocketObject(objectIdentifier, objectToTransport);
        try {
            this.socketOut.writeObject(socketObject);
        } catch (IOException e) {
            throw new ClientServerCommunicationException("Unable to send SocketObject", e);
        }
    }
}
