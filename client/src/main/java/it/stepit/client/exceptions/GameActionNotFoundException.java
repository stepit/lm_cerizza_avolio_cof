package it.stepit.client.exceptions;

public class GameActionNotFoundException extends Exception{
    public GameActionNotFoundException() {
        super();
    }

    public GameActionNotFoundException(String message) {
        super(message);
    }

    public GameActionNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public GameActionNotFoundException(Throwable cause) {
        super(cause);
    }
}
