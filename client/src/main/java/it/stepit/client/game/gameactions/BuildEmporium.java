package it.stepit.client.game.gameactions;

import it.stepit.client.Client;
import it.stepit.cscommons.communication.metaobjects.MetaBusinessCard;
import it.stepit.cscommons.communication.metaobjects.map.MetaTown;

import java.util.List;

class BuildEmporium extends GameAction{

    BuildEmporium() {
        super("BuildEmporiumByBusinessCard");
    }

    @Override
    public void acquireParams(Client client) {
        List<MetaTown> selectedTowns = client.selectTown(1, "Select a town:");

        if(selectedTowns.isEmpty() || selectedTowns.size() > 1)
            return;

        MetaBusinessCard selectedBusinessCard = client.selectBusinessCard("Pick one of yours business cards to build an emporium in " + selectedTowns.get(0).getName());

        if(selectedBusinessCard == null)
            return;

        this.addActionParam(selectedTowns.get(0));
        this.addActionParam(selectedBusinessCard);
        this.setReady();
    }

    @Override
    public String getActionDescription() {
        return "Select a town and one of yours business card";
    }
}
