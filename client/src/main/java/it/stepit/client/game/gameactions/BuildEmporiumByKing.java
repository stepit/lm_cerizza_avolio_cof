package it.stepit.client.game.gameactions;

import it.stepit.client.Client;
import it.stepit.cscommons.communication.metaobjects.MetaPoliticCard;
import it.stepit.cscommons.communication.metaobjects.map.MetaTown;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;

class BuildEmporiumByKing extends GameAction{

    BuildEmporiumByKing() {
        super("BuildEmporiumByKing");
    }

    @Override
    public void acquireParams(Client client) {
        this.addActionParam(client.getGame().getGameStatus().getKing());

        List<MetaTown> selectedTowns = client.selectTown(1, "Select a town to move the king to:");

        if(selectedTowns.isEmpty() || selectedTowns.size() != 1)
            return;

        Collection<MetaPoliticCard> selectedPlayerPoliticsCards = client.selectPoliticCards("Select from 1 to 4 politic cards to corrupt king balcony");

        if(selectedPlayerPoliticsCards.isEmpty() || selectedPlayerPoliticsCards.size() > 4)
            return;

        this.addActionParam(selectedTowns.get(0));
        this.addActionParam((Serializable) selectedPlayerPoliticsCards);
        this.setReady();
    }

    @Override
    public String getActionDescription() {
        return "Select a town to move the king to and from 1 to 4 of yours politic card to corrupt king balcony";
    }
}
