package it.stepit.client.game.gameactions;

import it.stepit.client.Client;
import it.stepit.cscommons.communication.metaobjects.MetaBusinessCard;
import it.stepit.cscommons.communication.metaobjects.MetaPoliticCard;
import it.stepit.cscommons.communication.metaobjects.map.MetaRegion;
import java.io.Serializable;
import java.util.Collection;

class BuyBusinessCard extends GameAction {

    BuyBusinessCard() {
        super("BuyBusinessCard");
    }

    @Override
    public void acquireParams(Client client) {
        MetaRegion selectedRegion = client.selectRegion("Select a region:");

        if(selectedRegion == null)
            return;

        MetaBusinessCard selectedBusinessCard = client.selectRegionBusinessCard(selectedRegion, "Select the business card to buy:");

        if(selectedBusinessCard == null)
            return;

        Collection<MetaPoliticCard> selectedPlayerPoliticsCards = client.selectPoliticCards("Select from 1 to 4 politic cards to corrupt the balcony:");

        if(selectedPlayerPoliticsCards.isEmpty() || selectedPlayerPoliticsCards.size() > 4 )
            return;

        this.addActionParam(selectedRegion);
        this.addActionParam(selectedBusinessCard);
        this.addActionParam((Serializable) selectedPlayerPoliticsCards);
        this.setReady();
    }

    @Override
    public String getActionDescription() {
        return "Select a region, the business card to buy and from 1 to 4 of yours politic cards";
    }
}
