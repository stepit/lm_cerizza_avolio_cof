package it.stepit.client.game.gameactions;

import it.stepit.client.Client;
import it.stepit.cscommons.communication.metaobjects.map.MetaRegion;

class ChangeBusinessCards extends GameAction {

    ChangeBusinessCards() {
        super("ChangeBusinessCards");
    }

    @Override
    public void acquireParams(Client client) {
        MetaRegion selectedRegion = client.selectRegion("Select a region:");

        if(selectedRegion == null)
            return;

        this.addActionParam(selectedRegion);
        this.setReady();
    }

    @Override
    public String getActionDescription() {
        return "Select the region in which you want to change business cards";
    }
}
