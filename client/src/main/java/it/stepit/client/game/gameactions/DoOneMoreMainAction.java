package it.stepit.client.game.gameactions;

import it.stepit.client.Client;

class DoOneMoreMainAction extends GameAction {

    DoOneMoreMainAction() {
        super("DoOneMoreMainAction");
        this.setReady();
    }

    @Override
    public void acquireParams(Client client) {
        this.setReady();
    }

    @Override
    public String getActionDescription() {
        return "Gives you the ability to perform one more main action";
    }
}
