package it.stepit.client.game.gameactions;

import it.stepit.client.Client;
import it.stepit.cscommons.communication.metaobjects.MetaBalcony;
import it.stepit.cscommons.communication.metaobjects.MetaCouncillor;

class ElectCouncillor extends GameAction {

    ElectCouncillor(String className) {
        super(className);
    }

    ElectCouncillor() {
        super("ElectCouncillor");
    }

    @Override
    public void acquireParams(Client client) {
        MetaBalcony selectedBalcony =  client.selectBalcony("Select a balcony:");

        if(selectedBalcony == null)
            return;

        MetaCouncillor selectedFreeCouncillor = client.selectFreeCouncillor("Select a free councillor:");

        if(selectedFreeCouncillor == null)
            return;

        this.addActionParam(selectedBalcony);
        this.addActionParam(selectedFreeCouncillor);
        this.setReady();;
    }

    @Override
    public String getActionDescription() {
        return "Select the balcony and a free councillor";
    }
}
