package it.stepit.client.game.gameactions;

import it.stepit.client.Client;

public class EngageAssistant extends GameAction {

    protected EngageAssistant() {
        super("EngageAssistant");
        this.setReady();
    }

    @Override
    public void acquireParams(Client client) {
        this.setReady();
    }

    @Override
    public String getActionDescription() {
        return "Gives you one assistant more";
    }
}
