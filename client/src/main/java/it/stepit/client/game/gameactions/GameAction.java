package it.stepit.client.game.gameactions;

import it.stepit.client.Client;
import it.stepit.client.exceptions.ClientException;
import it.stepit.cscommons.communication.metaobjects.MetaGameAction;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public abstract class GameAction {
    private String className;
    private boolean ready = false;
    private List<Object> actionParams;

    protected GameAction(String className){
        this.className = className;
        this.actionParams = new ArrayList<>();
    }

    public abstract void acquireParams(Client client);

    public abstract String getActionDescription();

    protected final void setReady(){
        this.ready = true;
    }

    public final boolean isReady(){
        return className != null && actionParams != null && this.ready;
    }

    protected final void addActionParam(Serializable obj){
        this.actionParams.add(obj);
    }

    public final MetaGameAction buildMetaGameAction() throws ClientException {
        if( !this.isReady() )
            throw new ClientException("Can't build meta game action: action is not ready");

        return new MetaGameAction( this.className, this.actionParams );
    }
}
