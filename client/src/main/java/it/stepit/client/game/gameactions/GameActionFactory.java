package it.stepit.client.game.gameactions;

import it.stepit.client.exceptions.GameActionNotFoundException;

public class GameActionFactory {

    private GameActionFactory(){
        //Private constructor to hide default one
    }

    public static GameAction getGameAction(String s) throws GameActionNotFoundException {
        switch(s.toLowerCase()){
            case "electcouncillor":
            case "abelectcouncillor":
                return new ElectCouncillor();

            case "buybusinesscard":
            case "abbuybusinesscard":
                return new BuyBusinessCard();

            case "buildemporium":
            case "abbuildemporium":
                return new BuildEmporium();

            case "buildemporiumbyking":
            case "abbuildemporiumbyking":
                return new BuildEmporiumByKing();

            case "engageassistant":
            case "abengageassistant":
                return new EngageAssistant();

            case "changebusinesscards":
            case "abchangebusinesscard":
                return new ChangeBusinessCards();

            case "electcouncillorbyassistants":
            case "abelectcouncillorbyassistants":
                return new ElectCouncillor("ElectCouncillorByAssistants");

            case "doonemoremainaction":
            case "abdoonemoremainaction":
                return new DoOneMoreMainAction();

            case "pass":
            case "abpass":
                return new Pass();

            default:
                throw new GameActionNotFoundException("No gameaction found for string:" + s);
        }
    }
}
