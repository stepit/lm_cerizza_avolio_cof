package it.stepit.client.game.gameactions;

import it.stepit.client.Client;

class Pass extends GameAction {

    Pass() {
        super("PassQuickAction");
        this.setReady();
    }

    @Override
    public void acquireParams(Client client) {
        this.setReady();
    }

    @Override
    public String getActionDescription() {
        return "Pass your turn";
    }
}
