package it.stepit.client.ui;

import javax.annotation.Nullable;

public interface ConnectionInitializationListener {
    void onInit();

    void onError(@Nullable Exception e);

    void onSuccess();
}
