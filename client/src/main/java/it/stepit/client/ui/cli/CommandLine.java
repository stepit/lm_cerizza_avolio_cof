package it.stepit.client.ui.cli;

import it.stepit.cscommons.communication.ListableObject;
import it.stepit.cscommons.communication.metaobjects.MetaGame;
import it.stepit.cscommons.communication.metaobjects.MetaGameStatus;
import it.stepit.cscommons.communication.metaobjects.map.MetaGameMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

public class CommandLine {
    private static final Logger LOGGER = LoggerFactory.getLogger(CommandLine.class);
    private static PrintStream outStream = System.out; //NOSONAR

    private static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_BLUE = "\u001B[34m";
    public static final String ANSI_PURPLE = "\u001B[35m";
    public static final String ANSI_CYAN = "\u001B[36m";
    public static final String ANSI_WHITE = "\u001B[37m";

    private CommandLine() { }

    public static void println(String ansiColorCode, String message){
        CommandLine.println( ansiColorCode.concat(message).concat(ANSI_RESET) );
    }

    public static void println(String message){
        outStream.println(message);
    }

    public static void println(){
        outStream.println();
    }

    public static void print(String ansiColorCode, String message){
        CommandLine.print( ansiColorCode.concat(message).concat(ANSI_RESET) );
    }

    public static void print(String message){
        outStream.print(message);
    }

    /**
     * Waits for an user input line.
     *
     * @return  The user input as string if able to read it, an empty string otherwise.
     */
    public static String getInput() {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        try {
            return br.readLine();
        } catch (IOException e) {
            LOGGER.warn("Unable to read user imput from StdIn", e);
        }

        return "";
    }

    /**
     * Waits for an user numeric input between min and max (included) boundaries.
     * Keeps asking the player for a value in that range if user sends wrong values.
     *
     * @return  The first valid user int input
     */
    public static int getIntInput(int min, int max){
        int inputInt;
        try {
            inputInt = Integer.parseInt(CommandLine.getInput());
        } catch (NumberFormatException e){
            inputInt = min-1; //Just force the validation to fail
        }

        if(inputInt < min || inputInt > max){
            CommandLine.println("Choice must be between " + min + " and " + max);
            return CommandLine.getIntInput(min, max);
        }

        return inputInt;
    }

    /**
     * Waits for an user input made of multiple integers separed by a space.
     * Each integer value must be included between min and max.
     * User must provide an <i>elements</i> amount of elements.
     *
     * @param min inputs lower bound
     * @param max inputs upper bound
     * @param minElements the min amount of elements the user must select
     * @param maxElements the max amout of elements the user must select
     */
    private static List<Integer> getMultipleIntInputs(int min, int max, int minElements, int maxElements){
        ArrayList<Integer> result = new ArrayList<>();
        boolean validationPassed = true;

        String[] explodedInput = CommandLine.getInput().split(" ");
        for (String anExplodedInput : explodedInput) {
            Integer intInput;
            try {
                intInput = Integer.parseInt(anExplodedInput);
            } catch (NumberFormatException e) {
                intInput = null;
            }

            if (intInput != null && intInput >= min && intInput <= max && !result.contains(intInput))
                result.add(intInput);
            else {
                validationPassed = false;
                break;
            }
        }

        if(validationPassed && result.size() >= min && result.size() <= max){
            return result;
        } else {
            CommandLine.println("Insert from " + minElements + " to " + maxElements + " elements between " + min + " and " + max + " separated by a space");
            return CommandLine.getMultipleIntInputs(min, max, minElements, maxElements);
        }
    }

    /**
     * Prints a list of objects adding an index for each element and waits for a user selection.
     *
     * @param objects The list of objects to select from
     * @return The index selected by the user, -1 if the list is empty
     */
    public static int createSingleSelectionList(List objects){
        return CommandLine.createSingleSelectionList(objects, false);
    }

    /**
     * Prints a list of objects adding an index for each element and waits for a user selection.
     * Add an option for a cancel choice
     *
     * @param objects The list of objects to select from
     * @return The index selected by the user, -1 if the list is empty or if the users canceled
     */
    public static int createSingleSelectionList(List objects, boolean cancelOption){
        if(objects.isEmpty())
            return -1;

        CommandLine.printList(objects, cancelOption);
        return CommandLine.getIntInput( cancelOption ? -1 : 0, objects.size()-1 );
    }

    /**
     * Prints a list of objects adding an index for each element and waits for a user multiple selection.
     * The amount of selected items should be greater than minElements and lower thant maxElements
     *
     * @param objects The list of objects to multiple select from
     * @param minElements The min amount of elements to select
     * @param maxElements The max amount of elements to select
     * @return A list of selected integers
     */
    public static List<Integer> createMultipleSelectionList(List objects, int minElements, int maxElements){
        if(objects.isEmpty())
            return new ArrayList<>();

        CommandLine.printList(objects, false);
        return CommandLine.getMultipleIntInputs(0, objects.size()-1, minElements, maxElements);
    }

    private static void printList(List objects, boolean cancelOption){
        for (int i = 0; i < objects.size(); i++) {
            CommandLine.print( String.format("%02d - ", i) );

            if( objects.get(i) instanceof ListableObject)
               CommandLine.println( ((ListableObject) objects.get(i)).getTextDescriptor() );
            else
                CommandLine.println(objects.get(i).toString() );
        }

        if(cancelOption)
            CommandLine.println("-1 - Cancel");
    }

    public static void printMap(MetaGame game){
        MetaGameStatus gameStatus = game.getGameStatus();
        MetaGameMap map = gameStatus.getGameMap();

        if(gameStatus.getGameMap().isUserEdit()) {
            throw new UnsupportedOperationException("Currently client doesn't support user edited maps! Sorry...");
        }else{
            CommandLine.println(map.getCliMap());
        }
    }

    /**
     * Cleans the output console.
     * Does differente actions based on the current operative system.
     */
    public static void clean(){
        String os = System.getProperty("os.name").toLowerCase();

        try {
            if( os.contains("win") ) {
                new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
            }else {
                CommandLine.print("\u001B[2J\u001B[H");
            }
        } catch (InterruptedException | IOException e) {
            LOGGER.warn("Unable to clean StdOut", e);
        }
    }
}