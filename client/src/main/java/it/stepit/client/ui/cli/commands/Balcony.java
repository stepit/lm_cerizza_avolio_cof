package it.stepit.client.ui.cli.commands;

import it.stepit.client.Client;
import it.stepit.client.ui.cli.CommandLine;
import it.stepit.cscommons.communication.metaobjects.MetaGameStatus;
import it.stepit.cscommons.communication.metaobjects.map.MetaRegion;

class Balcony implements Command{
    private String[] arguments;

    Balcony(String[] arguments){
        this.arguments = arguments;
    }

    @Override
    public void perform() {
        MetaGameStatus gameStatus = Client.instance.getGame().getGameStatus();

        if(arguments.length != 1){
            CommandLine.println("Usage /balcony <king/region>");
        }else{
            if("king".equals(arguments[0])){
                CommandLine.println(gameStatus.getKing().getBalcony().getTextDescriptor());
            }else{
                MetaRegion rRegion = gameStatus.getGameMap().getRegion(arguments[0]);
                if(rRegion != null){
                    CommandLine.println("Balcony of " + rRegion.getName() + "\t" + rRegion.getBalcony().getTextDescriptor());
                }else{
                    CommandLine.println("No region found with name " + arguments[0]);
                }
            }
        }
    }
}
