package it.stepit.client.ui.cli.commands;

import it.stepit.client.Client;
import it.stepit.client.ui.cli.CommandLine;
import it.stepit.cscommons.communication.metaobjects.MetaBonusCard;
import it.stepit.cscommons.communication.metaobjects.MetaCouncillor;
import it.stepit.cscommons.communication.metaobjects.MetaGameStatus;

class Board implements Command{

    @Override
    public void perform() {
        MetaGameStatus gameStatus = Client.instance.getGame().getGameStatus();

        CommandLine.println("- Free assistants: " + gameStatus.getFreeAssistants() + "\t- King bonus card: " + gameStatus.getKing().getBonusCards());
        CommandLine.println();

        CommandLine.print("- Free councillors: ");
        for(MetaCouncillor c : gameStatus.getFreeCouncillors()){
            CommandLine.print( c.getTextDescriptor() + " ");
        }
        CommandLine.println();
        CommandLine.println();

        CommandLine.println("- Bonus cards: ");
        for(MetaBonusCard bc : gameStatus.getBonusCards()){
            CommandLine.println("\t\t" + bc.getTextDescriptor());
        }
    }
}
