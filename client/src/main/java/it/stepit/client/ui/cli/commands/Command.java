package it.stepit.client.ui.cli.commands;

@FunctionalInterface
public interface Command {
    void perform();
}
