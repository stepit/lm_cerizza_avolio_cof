package it.stepit.client.ui.cli.commands;

import it.stepit.client.exceptions.CommandNotFoundException;

public class CommandsFactory {

    private CommandsFactory(){
        //Private constructor to hide default one.
    }

    public static Command getCommand(String s, String[] commandArgs) throws CommandNotFoundException {
        switch(s.toLowerCase()){
            case "balcony":
                return new Balcony(commandArgs);

            case "board":
                return new Board();

            case "help":
                return new Help();

            case "king":
                return new King();

            case "map":
                return new Map();

            case "me":
                return new Me();

            case "nobilitypath":
                return new NobilityPath();

            case "players":
                return new Players();

            case "reward":
                return new Reward(commandArgs);

            case "takeaction":
                return new TakeAction(commandArgs);

            case "towns":
                return new Towns();

            case "market":
                return new Market(commandArgs);

            default:
                throw new CommandNotFoundException("No command found for string " + s);
        }
    }
}
