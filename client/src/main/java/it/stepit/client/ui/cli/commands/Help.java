package it.stepit.client.ui.cli.commands;

import it.stepit.client.ui.cli.CommandLine;

class Help implements Command {
    @Override
    public void perform() {
        CommandLine.println();
        CommandLine.println("/players\t\tPlayers list and stats");
        CommandLine.println("/nobilitypath\t\tShow nobility path");
        CommandLine.println("/map\t\t\tShow the map");
        CommandLine.println("/balcony <region/king>\tShow region or king balcony");
        CommandLine.println("/king\t\t\tShow king position");
        CommandLine.println("/reward <region/town>\tShow the reward associated with region or town");
        CommandLine.println("/towns\t\t\tShow town list and stats");
        CommandLine.println("/board\t\t\tShow board stats");
        CommandLine.println("/me\t\t\tShow informations about your status");
        CommandLine.println("/takeaction <action>\tTake a game action");
        CommandLine.println("/market <sell/buy/pass>\tAvailable during market mode");
    }
}
