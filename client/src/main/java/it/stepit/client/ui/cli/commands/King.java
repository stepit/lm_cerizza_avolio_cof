package it.stepit.client.ui.cli.commands;

import it.stepit.client.Client;
import it.stepit.client.ui.cli.CommandLine;

class King implements Command {
    @Override
    public void perform() {
        CommandLine.println("Current king town is: " + Client.instance.getGame().getGameStatus().getKing().getCurrentTown().getName()+
                "\t\tBalcony: " + Client.instance.getGame().getGameStatus().getKing().getBalcony().getTextDescriptor());
    }
}
