package it.stepit.client.ui.cli.commands;

import it.stepit.client.Client;
import it.stepit.client.ui.cli.CommandLine;
import it.stepit.cscommons.communication.metaobjects.map.MetaRegion;

class Map implements Command {
    @Override
    public void perform() {
        CommandLine.printMap(Client.instance.getGame());
        for(MetaRegion region: Client.instance.getGame().getGameStatus().getGameMap().getRegions()){
            CommandLine.println("Balcony of "+ region.getName() + " " + region.getBalcony().getTextDescriptor()+"\n");
        }
    }

}
