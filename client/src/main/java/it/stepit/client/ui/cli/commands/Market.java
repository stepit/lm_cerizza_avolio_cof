package it.stepit.client.ui.cli.commands;

import com.google.common.collect.ImmutableList;
import it.stepit.client.Client;
import it.stepit.client.ui.cli.CommandLine;
import it.stepit.cscommons.communication.metaobjects.MetaBusinessCard;
import it.stepit.cscommons.communication.metaobjects.MetaMarketOffer;
import it.stepit.cscommons.communication.metaobjects.MetaPoliticCard;
import it.stepit.cscommons.exceptions.ClientServerCommunicationException;
import it.stepit.cscommons.utils.RichBoolean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class Market implements Command {
    private static final Logger LOGGER = LoggerFactory.getLogger(Market.class);
    private String[] arguments;

    Market(String[] arguments){
        this.arguments = arguments;
    }

    @Override
    public void perform() {
        if (arguments.length != 1) {
            CommandLine.println("Usage /market <sell/buy/pass>");
        } else {
            if ( "sell".equals(arguments[0])) {
                this.sell();

            } else if ( "buy".equals(arguments[0])) {
                this.buy();
            } else if( "pass".equals(arguments[0])){
                this.pass();
            } else {
                CommandLine.println("Usage /market <sell/buy/pass>");
            }
        }
    }

    private void sell(){
        MetaMarketOffer newMarketOffer;
        String[] sellOptions = {"Politic card", "Business card", "Assistants"};

        CommandLine.println("What do you want to sell?");
        switch (CommandLine.createSingleSelectionList(Arrays.asList(sellOptions), true)) {
            case 0: //Politic Card
                newMarketOffer = this.sellPoliticCard();
                break;

            case 1: //Business Card
                newMarketOffer = this.sellBusinessCard();
                break;

            case 2: //Assistants
                newMarketOffer = this.sellAssistants();
                break;

            default:
                return;
        }

        if(newMarketOffer == null)
            return;

        try {
            RichBoolean offerPublishResult = Client.instance.getOutInterface().sendMarketOffer(newMarketOffer);
            if (offerPublishResult.getBooleanValue())
                CommandLine.println(CommandLine.ANSI_GREEN, "Offer published succesfully!");
            else
                CommandLine.println(CommandLine.ANSI_RED, "Unable to publish market offer: " + offerPublishResult.getMessage());

        } catch (ClientServerCommunicationException e) {
            LOGGER.error("Unable to deliver market offer", e);
        }
    }

    private void buy(){
        CommandLine.println("Pick an offer to buy:");
        MetaMarketOffer selectedOffer = null;

        try {
            List<MetaMarketOffer> marketOffers = new ArrayList<>(Client.instance.getOutInterface().getMarketOffers());

            if (marketOffers.isEmpty()) {
                CommandLine.println(CommandLine.ANSI_RED, "No available market offers");
            } else {
                int selectionResult = CommandLine.createSingleSelectionList(marketOffers, true);
                if(selectionResult >= 0)
                    selectedOffer = marketOffers.get(selectionResult);
            }
        } catch (ClientServerCommunicationException e) {
            LOGGER.error("Unable to retrieve market offers list from server", e);
        }

        if (selectedOffer != null) {
            try {
                RichBoolean buyResult = Client.instance.getOutInterface().buyMarketOffer(selectedOffer);
                if( !buyResult.getBooleanValue())
                    CommandLine.println(CommandLine.ANSI_RED, "Unable to buy object: " + buyResult.getMessage());

            } catch (ClientServerCommunicationException e) {
                LOGGER.error("Unable to deliver buy request", e);
            }
        }
    }

    private void pass(){
        try {
            RichBoolean res = Client.instance.getOutInterface().passMarketBuyPhase();
            if(!res.getBooleanValue())
                CommandLine.println(CommandLine.ANSI_RED, "Unable to pass: " + res.getMessage());

        } catch (ClientServerCommunicationException e) {
            LOGGER.error("Unable to deliver pass request", e);
        }

    }

    private MetaMarketOffer sellPoliticCard(){
        List<MetaPoliticCard> playerPoliticCard = ImmutableList.copyOf(Client.instance.getPlayer().getPoliticsCards());

        if (!playerPoliticCard.isEmpty()) {
            CommandLine.println("Select one of your politic card:");
            MetaPoliticCard selectedPoliticCard = playerPoliticCard.get(CommandLine.createSingleSelectionList(playerPoliticCard));
            CommandLine.println("Choose a price:");
            int price = CommandLine.getIntInput(0, 999);

            return new MetaMarketOffer(Client.instance.getPlayer(), selectedPoliticCard, price);
        } else {
            CommandLine.println(CommandLine.ANSI_RED, "You don't have any politic card");
        }

        return null;
    }

    private MetaMarketOffer sellBusinessCard(){
        List<MetaBusinessCard> playerBusinessCard = ImmutableList.copyOf(Client.instance.getPlayer().getBusinessCards());

        if (!playerBusinessCard.isEmpty()) {
            CommandLine.println("Select one of your business card:");
            MetaBusinessCard selectedBusinessCard = playerBusinessCard.get(CommandLine.createSingleSelectionList(playerBusinessCard));
            CommandLine.println("Choose a price:");
            int price = CommandLine.getIntInput(0, 999);

            return new MetaMarketOffer(Client.instance.getPlayer(), selectedBusinessCard, price);
        } else {
            CommandLine.println(CommandLine.ANSI_RED, "You don't have any business card");
        }

        return null;
    }

    private MetaMarketOffer sellAssistants(){
        int playerAssistants = Client.instance.getPlayer().getAssistants();
        if (playerAssistants > 0) {
            CommandLine.println("Select how many of your assistants you want to sell:");
            int assistantsOnSale = CommandLine.getIntInput(1, playerAssistants);
            CommandLine.println("Choose a price:");
            int price = CommandLine.getIntInput(0, 999);

            return new MetaMarketOffer(Client.instance.getPlayer(), assistantsOnSale, price);
        } else {
            CommandLine.println(CommandLine.ANSI_RED, "You don't have any assistant");
        }

        return null;
    }
}
