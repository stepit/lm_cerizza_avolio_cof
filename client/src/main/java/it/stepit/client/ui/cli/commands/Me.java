package it.stepit.client.ui.cli.commands;

import it.stepit.client.Client;
import it.stepit.client.ui.cli.CommandLine;
import it.stepit.cscommons.communication.metaobjects.MetaBusinessCard;
import it.stepit.cscommons.communication.metaobjects.MetaPlayer;
import it.stepit.cscommons.communication.metaobjects.MetaPoliticCard;

class Me implements Command {
    @Override
    public void perform() {
        MetaPlayer player = Client.instance.getPlayer();
        CommandLine.println(player.getTextDescriptor());

        CommandLine.println();
        CommandLine.println("- Business Cards:");

        for(MetaBusinessCard businessCard : player.getBusinessCards()){
            CommandLine.println("\t\t\t" + businessCard.getTextDescriptor());
        }

        CommandLine.println();
        CommandLine.println("- Politics Cards:");

        for(MetaPoliticCard politicCard : player.getPoliticsCards()){
            CommandLine.println("\t\t\t" + politicCard.getTextDescriptor());
        }
    }
}
