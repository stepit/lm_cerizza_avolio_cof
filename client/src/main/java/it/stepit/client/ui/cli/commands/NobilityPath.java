package it.stepit.client.ui.cli.commands;

import com.google.common.collect.ImmutableList;
import it.stepit.client.Client;
import it.stepit.client.ui.cli.CommandLine;
import it.stepit.cscommons.communication.metaobjects.MetaNobilityPathStep;
import it.stepit.cscommons.communication.metaobjects.MetaPlayer;

import java.util.List;

class NobilityPath implements Command{
    @Override
    public void perform() {
        List<MetaNobilityPathStep> nobilityPath = Client.instance.getGame().getGameStatus().getNobilityPath();
        for (int i = 0; i < nobilityPath.size(); i++) {
            MetaNobilityPathStep pathStep = nobilityPath.get(i);
            CommandLine.print(i + " -\tReward: ");
            if(pathStep.hasReward())
                CommandLine.print(pathStep.getReward().getTextDescriptor());

            CommandLine.println();
            ImmutableList<MetaPlayer> playersInThisStep = Client.instance.getGame().getPlayersInNobilityPathStep(i);
            if(!playersInThisStep.isEmpty()){
                CommandLine.print("\tPlayers: ");
                for(MetaPlayer p : playersInThisStep){
                    CommandLine.print(p.getUsername() + " ");
                }

                CommandLine.println();
            }
        }
    }
}
