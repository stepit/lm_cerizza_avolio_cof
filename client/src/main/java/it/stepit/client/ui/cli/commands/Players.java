package it.stepit.client.ui.cli.commands;

import it.stepit.client.Client;
import it.stepit.client.ui.cli.CommandLine;
import it.stepit.cscommons.communication.metaobjects.MetaPlayer;

class Players implements Command {
    @Override
    public void perform() {
        for(MetaPlayer player : Client.instance.getGame().getPlayers()){
            CommandLine.println(player.getTextDescriptor());
        }
    }
}
