package it.stepit.client.ui.cli.commands;

import it.stepit.client.Client;
import it.stepit.client.ui.cli.CommandLine;
import it.stepit.cscommons.communication.metaobjects.map.MetaGameMap;
import it.stepit.cscommons.communication.metaobjects.map.MetaRegion;
import it.stepit.cscommons.communication.metaobjects.map.MetaTown;

class Reward implements Command {
    private String[] arguments;

    Reward(String[] arguments){
        this.arguments = arguments;
    }

    @Override
    public void perform() {
        if(this.arguments.length != 1){
            CommandLine.println("Usage /reward <region/town>");
        }else{
            MetaGameMap gameMap = Client.instance.getGame().getGameStatus().getGameMap();
            MetaRegion rRegion = gameMap.getRegion(arguments[0]);
            MetaTown rTown = gameMap.getTown(arguments[0]);
            if(rRegion != null){
                CommandLine.print("Reward of region " + arguments[0] + ":\t");
                if(rRegion.hasReward()){
                    CommandLine.println("\t\t" + rRegion.getReward().getTextDescriptor());
                }else{
                    CommandLine.println("\t\tReward was alredy taken");
                }
            }else if(rTown != null){
                CommandLine.print("Reward of town " + arguments[0] + ":\t");
                if(rTown.hasReward())
                    CommandLine.println(rTown.getRewardToken().getTextDescriptor());
                else
                    CommandLine.println("No reward");
            }else{
                CommandLine.println("No town or region found with name " + arguments[0]);
            }
        }
    }
}
