package it.stepit.client.ui.cli.commands;

import it.stepit.client.CliClient;
import it.stepit.client.Client;
import it.stepit.client.exceptions.ClientException;
import it.stepit.client.exceptions.GameActionNotFoundException;
import it.stepit.client.game.gameactions.GameAction;
import it.stepit.client.game.gameactions.GameActionFactory;
import it.stepit.client.ui.cli.CommandLine;
import it.stepit.cscommons.exceptions.ClientServerCommunicationException;
import it.stepit.cscommons.utils.RichBoolean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class TakeAction implements Command{
    private static final Logger LOGGER = LoggerFactory.getLogger(CliClient.class);
    private String[] arguments;

    TakeAction(String[] arguments){
        this.arguments = arguments;
    }

    @Override
    public void perform() {
        if (arguments.length != 1) {
            this.printUsageHints();
        } else {
            try {
                GameAction selectedGameAction = GameActionFactory.getGameAction(arguments[0]);
                selectedGameAction.acquireParams(Client.instance);
                this.deliverActionToServer(selectedGameAction);

            } catch (GameActionNotFoundException e) {
                this.printUsageHints();
            }
        }
    }

    private void deliverActionToServer(GameAction selectedGameAction){
        if(selectedGameAction.isReady()) {
            RichBoolean gameActionResult = new RichBoolean(false);

            try {
                gameActionResult = Client.instance.getOutInterface().takeGameAction(selectedGameAction.buildMetaGameAction());
            } catch (ClientServerCommunicationException | ClientException e) {
                LOGGER.error("Unable to deliver game action", e);
            }

            if ( !gameActionResult.getBooleanValue() )
                CommandLine.println(CommandLine.ANSI_RED, "Unable to take game action: " + gameActionResult.getMessage());
        }
    }

    private void printUsageHints(){
        CommandLine.println("Usage /takeaction <actionName>");
        CommandLine.println("Available actions: \tElectCouncillor");
        CommandLine.println( "\t\t\tBuyBusinessCard" );
        CommandLine.println( "\t\t\tBuildEmporium" );
        CommandLine.println( "\t\t\tBuildEmporiumByKing" );
        CommandLine.println( "\t\t\tEngageAssistant" );
        CommandLine.println( "\t\t\tChangeBusinessCards" );
        CommandLine.println( "\t\t\tElectCouncillorByAssistants" );
        CommandLine.println( "\t\t\tDoOneMoreMainAction" );
        CommandLine.println();
    }
}
