package it.stepit.client.ui.cli.commands;

import it.stepit.client.Client;
import it.stepit.client.ui.cli.CommandLine;
import it.stepit.cscommons.communication.metaobjects.map.MetaGameMap;
import it.stepit.cscommons.communication.metaobjects.map.MetaTown;

class Towns implements Command{
    @Override
    public void perform() {
        MetaGameMap gameMap = Client.instance.getGame().getGameStatus().getGameMap();
        for(MetaTown town : gameMap.getTowns()){
            CommandLine.println("- " + town.getTextDescriptor());
        }
    }
}
