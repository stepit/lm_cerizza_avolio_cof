package it.stepit.client.ui.gui;

import it.stepit.client.Client;
import it.stepit.client.GuiClient;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Gui extends Application{

    @Override
    public void start(Stage primaryStage) throws Exception {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/javafx/startPane.fxml"));
        primaryStage.setTitle("Welcome to Council of Four!");
        primaryStage.setResizable(false);
        primaryStage.setScene(new Scene( loader.load() ));
        primaryStage.show();

        ((GuiClient) Client.instance).setStartPanelControl( loader.getController() );
    }

    @Override
    public void stop() throws Exception {
        super.stop();
        ((GuiClient) Client.instance).onStageClosed();
    }
}
