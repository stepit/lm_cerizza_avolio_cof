package it.stepit.client.ui.gui.controllers;

import it.stepit.client.Client;
import it.stepit.client.exceptions.ClientException;
import it.stepit.client.exceptions.GameActionNotFoundException;
import it.stepit.client.game.gameactions.GameAction;
import it.stepit.client.game.gameactions.GameActionFactory;
import it.stepit.cscommons.communication.metaobjects.*;
import it.stepit.cscommons.communication.metaobjects.map.MetaRegion;
import it.stepit.cscommons.communication.metaobjects.map.MetaTown;
import it.stepit.cscommons.exceptions.ClientServerCommunicationException;
import it.stepit.cscommons.exceptions.XmlLoadingException;
import it.stepit.cscommons.utils.ColorUtils;
import it.stepit.cscommons.utils.RichBoolean;
import it.stepit.cscommons.utils.XmlReader;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.effect.ColorAdjust;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;

import java.awt.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;

public class GamePaneController {
    private static final Logger LOGGER = LoggerFactory.getLogger(GamePaneController.class);
    private static final String IMAGES_LOCATION = "/javafx/images/";
    private static final String CONFIGURATIONS_LOCATION = "/javafx/game_configurations/";

    /* Objects Sizes */
    private static final int BALCONYIMAGE_WIDTH = 115;
    private static final int BALCONYIMAGE_HEIGHT = 37;
    private static final int COUNCILLORIMAGE_WIDTH = 30;
    private static final int COUNCILLORIMAGE_HEIGHT = 58;

    private String gameConfigurationName;
    private Document xmlElementsDocument;
    private SelectedElementsHelper selectedElementsHelper;
    private GameAction playerAction;

    @FXML
    private ImageView mapImageview;
    @FXML
    private VBox messagesContainer;
    @FXML
    private AnchorPane mapElementsContainer;
    @FXML
    private VBox playersListContainer;
    @FXML
    private AnchorPane ytPoliticCards;
    @FXML
    private AnchorPane ytBusinessCards;
    @FXML
    private ScrollPane messagesScrollPane;
    @FXML
    private HBox freeCouncillorsContainer;
    @FXML
    private Label actionDescriptionContainer;
    @FXML
    private Button applyGameAction;
    @FXML
    private Button cancelGameAction;
    @FXML
    private Button abElectCouncillor;
    @FXML
    private Button abBuyBusinessCard;
    @FXML
    private Button abBuildEmporium;
    @FXML
    private Button abBuildEmporiumByKing;
    @FXML
    private Button abEngageAssistant;
    @FXML
    private Button abChangeBusinessCard;
    @FXML
    private Button abElectCouncillorByAssistants;
    @FXML
    private Button abDoOneMoreMainAction;
    @FXML
    private Button abPass;
    @FXML
    private AnchorPane marketSellPopup;
    @FXML
    private AnchorPane blackCourtain;
    @FXML
    private Button mtSell;
    @FXML
    private Button mtPass;
    @FXML
    private Button mpopupCancel;
    @FXML
    private Button mpopupSell;
    @FXML
    private ToggleGroup marketSellType;
    @FXML
    private RadioButton sellBusinessCard;
    @FXML
    private RadioButton sellAssistants;
    @FXML
    private RadioButton sellPoliticCard;
    @FXML
    private AnchorPane sellAssistantsAmount;
    @FXML
    private TextField mpopupPrice;
    @FXML
    private TextField mpopupAssistantsAmount;
    @FXML
    private Button mtLoadOffers;
    @FXML
    private VBox marketOffersContainer;
    @FXML
    private AnchorPane leaderBoardPopup;
    @FXML
    private AnchorPane leaderBoardContainer;

    @FXML
    void initialize() {
        this.selectedElementsHelper = new SelectedElementsHelper();

        this.gameConfigurationName = Client.instance.getGame().getGameStatus().getConfigurationName().toLowerCase();

        LOGGER.debug("Initialize. Configuration Name: " + this.gameConfigurationName);
        try {
            this.xmlElementsDocument = XmlReader.getXmlDocumentFromFile( getClass().getResourceAsStream("/javafx/game_configurations/" + this.gameConfigurationName + "/elements.xml") );

            this.drawMap();

            this.drawTownsAndRewardTokens();

            this.drawEmporiumsIndicators();

            this.drawRegionBonusCards();

            this.drawBonusCards();

            this.drawPlayerTab();

            this.drawPlayersList();

            this.printMessages();

            this.marketSellType.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
                @Override
                public void changed(ObservableValue<? extends Toggle> observable, Toggle oldValue, Toggle newValue) {
                    if(newValue.equals(sellAssistants))
                        sellAssistantsAmount.setVisible(true);
                    else
                        sellAssistantsAmount.setVisible(false);
                }
            });

        } catch (XmlLoadingException e) {
            LOGGER.error("Unable to load map elements xml file", e);
        }
    }

    /**********************
     * CLICK INTERACTIONS *
     **********************/
    @FXML
    void onActionButtonClicked(MouseEvent event){
        String buttonId = ((Control) event.getSource()).getId();
        try {
            this.playerAction = GameActionFactory.getGameAction(buttonId);

            this.setActionButtonsDisabled(true);
            this.setActionsApplyCancelDisabled(false);
            this.actionDescriptionContainer.setText(this.playerAction.getActionDescription());

        } catch (GameActionNotFoundException e) {
            LOGGER.error("Unable to instantiate game action", e);
        }
    }

    @FXML
    void onActionApplyCancel(MouseEvent event){
        if(event.getSource().equals(this.cancelGameAction)){
            this.setActionsApplyCancelDisabled(true);
            this.setActionButtonsDisabled(false);
            this.playerAction = null;
        }else{
            this.playerAction.acquireParams(Client.instance);
            this.deliverActionToServer(this.playerAction);
        }
    }

    private void deliverActionToServer(GameAction selectedGameAction){

        Task<RichBoolean> deliverActionToServerTask = new Task<RichBoolean>() {
            @Override
            protected RichBoolean call() throws Exception {
                try {
                    return Client.instance.getOutInterface().takeGameAction(selectedGameAction.buildMetaGameAction());
                } catch (ClientServerCommunicationException e) {
                    LOGGER.error("Unable to deliver game action", e);
                    return new RichBoolean(false, "Unable to communicate with server");
                } catch (ClientException e) {
                    return new RichBoolean(false, "Please make sure your selection contains only requested elements");
                }
            }
        };

        deliverActionToServerTask.setOnSucceeded(t -> {
            RichBoolean gameActionResult = deliverActionToServerTask.getValue();
            if ( gameActionResult != null && !gameActionResult.getBooleanValue() ){
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Unable to take game action");
                alert.setHeaderText(null);
                alert.setContentText("Server response:\n\n" + gameActionResult.getMessage());
                alert.show();
            }

            this.setActionsApplyCancelDisabled(true);
            this.setActionButtonsDisabled(false);
        });

        new Thread(deliverActionToServerTask).start();
    }

    private void onElementClicked(Object obj, javafx.scene.Node node, String selectedStyleClass){
        if(this.selectedElementsHelper.makeObjectSelected(obj)){
            node.getStyleClass().add(selectedStyleClass);
        }else{
            node.getStyleClass().remove(selectedStyleClass);
            this.selectedElementsHelper.makeSelected(obj);
        }
    }

    private void setActionButtonsDisabled(boolean disabled){
        this.abElectCouncillor.setDisable(disabled);
        this.abBuyBusinessCard.setDisable(disabled);
        this.abBuildEmporium.setDisable(disabled);
        this.abBuildEmporiumByKing.setDisable(disabled);
        this.abEngageAssistant.setDisable(disabled);
        this.abChangeBusinessCard.setDisable(disabled);
        this.abElectCouncillorByAssistants.setDisable(disabled);
        this.abDoOneMoreMainAction.setDisable(disabled);
        this.abPass.setDisable(disabled);
    }

    private void setActionsApplyCancelDisabled(boolean disabled){
        this.applyGameAction.setDisable(disabled);
        this.cancelGameAction.setDisable(disabled);

        if(disabled)
            this.actionDescriptionContainer.setText(null);
    }

    /****************
     * DRAW METHODS *
     ****************/
    public void addMessage(String m, int index){
        this.addMessage(m, index%2 == 0);
    }

    private void addMessage(String m, boolean even){
        DateFormat dateFormat = new SimpleDateFormat("HH:mm");
        Label newLabel = new Label( "[" + dateFormat.format(new Date()) + "] " + m);

        if( even )
            newLabel.getStyleClass().addAll("message_row", "message_row_even");
        else
            newLabel.getStyleClass().addAll("message_row", "message_row_odd");

        Platform.runLater(() -> this.messagesContainer.getChildren().add(newLabel) );
        Platform.runLater(() -> this.messagesScrollPane.setVvalue(1.0) );

    }

    public <T> Collection<T> getSelectedElements(Class<T> objClass){
        return this.selectedElementsHelper.getSelectedObjects(objClass);
    }

    private void drawMap(){
        if( this.mapImageview.getImage() == null )
            this.mapImageview.setImage( new Image( getClass().getResourceAsStream( CONFIGURATIONS_LOCATION + this.gameConfigurationName + "/map.png")));

        this.drawBalconies();

        this.drawBusinessCards();

        this.drawFreeCouncillors();
    }

    private void drawPlayersList(){
        List<MetaPlayer> players = Client.instance.getGame().getPlayers();

        for(int i = 0; i < players.size(); i++){
            MetaPlayer p = players.get(i);

            VBox playerRow = new VBox();
            if( i%2 == 0)
                playerRow.getStyleClass().addAll("playersList_row", "playersList_row_even");
            else
                playerRow.getStyleClass().addAll("playersList_row", "playersList_row_odd");

            Label playerName = new Label(p.getUsername());
            playerName.getStyleClass().add("player_username");
            playerRow.getChildren().add(playerName);

            HBox playerStatus1 = new HBox();
            playerStatus1.getStyleClass().add("hbox");
            HBox playerStatus2 = new HBox();
            playerStatus2.getStyleClass().add("hbox");

            ImageView coinsIcon = new ImageView( IMAGES_LOCATION + "coin.png" );
            coinsIcon.setPreserveRatio(true);
            Label coins = new Label(Integer.toString(p.getCoins()));
            coins.getStyleClass().add("value_label");

            ImageView assistantsIcon = new ImageView( IMAGES_LOCATION + "assistant.png");
            assistantsIcon.setPreserveRatio(true);
            Label assistants = new Label(Integer.toString(p.getAssistants()));
            assistants.getStyleClass().add("value_label");

            ImageView victoryPointsIcon = new ImageView( IMAGES_LOCATION + "victory_point.png" );
            victoryPointsIcon.setPreserveRatio(true);
            Label victoryPoints = new Label(Integer.toString(p.getVictoryPoints()));
            victoryPoints.getStyleClass().add("value_label");

            ImageView emporiumsIcon = new ImageView( IMAGES_LOCATION + "emporium.png");
            emporiumsIcon.setPreserveRatio(true);
            Label emporiums = new Label( Integer.toString(p.getEmporiumsToWin()) );
            emporiums.getStyleClass().add("value_label");

            ImageView politicCardIcon = new ImageView( IMAGES_LOCATION + "politic_card.png");
            politicCardIcon.setPreserveRatio(true);
            Label politicCard = new Label( Integer.toString(p.getPoliticsCards().size()) );
            politicCard.getStyleClass().add("value_label");

            ImageView nobilityPathIcon = new ImageView(IMAGES_LOCATION + "nobility.png");
            Label nobilityPath = new Label( Integer.toString(p.getNobilityPathPosition()) );
            nobilityPath.getStyleClass().add("value_label");

            playerStatus1.getChildren().addAll(coinsIcon, coins, assistantsIcon, assistants, victoryPointsIcon, victoryPoints);
            playerStatus2.getChildren().addAll(emporiumsIcon, emporiums, politicCardIcon, politicCard, nobilityPathIcon, nobilityPath);
            playerRow.getChildren().addAll(playerStatus1, playerStatus2);

            Platform.runLater(() -> this.playersListContainer.getChildren().add(playerRow));
        }
    }

    private void drawFreeCouncillors(){
        Set<MetaCouncillor> freeCouncillors = Client.instance.getGame().getGameStatus().getFreeCouncillors();

        for(MetaCouncillor fc : freeCouncillors){
            ImageView councillorImageView = new ImageView( IMAGES_LOCATION + "councillor_" + ColorUtils.getHexStringFromColor( fc.getColor() ) + ".png" );
            councillorImageView.setFitWidth(35);
            councillorImageView.setPreserveRatio(true);
            councillorImageView.getStyleClass().add("free_councillor");

            councillorImageView.setOnMouseClicked(event -> this.onElementClicked(fc, councillorImageView, "selected_free_councillor") );

            Platform.runLater(() -> this.freeCouncillorsContainer.getChildren().add(councillorImageView));
        }
    }

    /****************************************
     * TOWNS BONUS CARDS & KING BONUS CARDS *
     ****************************************/
    private void drawBonusCards(){
        Set<MetaBonusCard> bonusCards = Client.instance.getGame().getGameStatus().getBonusCards();

        for(MetaBonusCard bonusCard: bonusCards){
            Point cardPosition = XmlReader.getElementCoords(this.xmlElementsDocument, new String[]{"bonusCards", ColorUtils.getColorNameFromColor(bonusCard.getColor()).toUpperCase()} );
            if(cardPosition != null){
                ImageView cardImageView = new ImageView( CONFIGURATIONS_LOCATION + this.gameConfigurationName + "/bonus_cards/" + ColorUtils.getHexStringFromColor(bonusCard.getColor()) + ".png" );
                cardImageView.setFitWidth(62);
                cardImageView.setPreserveRatio(true);
                cardImageView.setRotate(45);
                cardImageView.setLayoutX(cardPosition.getX());
                cardImageView.setLayoutY(cardPosition.getY());
                cardImageView.getStyleClass().add("board_bonus_card");

                Platform.runLater(() -> this.mapElementsContainer.getChildren().add(cardImageView));
            }
        }

        //TODO Print king bonus card
    }

    /*************
     * BALCONIES *
     *************/
    private void drawBalconies(){
        Point kingBalconyPosition = XmlReader.getElementCoords(this.xmlElementsDocument, new String[]{"balconies", "king"});
        this.drawBalcony( Client.instance.getGame().getGameStatus().getKing().getBalcony(), kingBalconyPosition );

        for(MetaRegion r : Client.instance.getGame().getGameStatus().getGameMap().getRegions()){
            Point regionBalconyPosition = XmlReader.getElementCoords(this.xmlElementsDocument, new String[]{"balconies", r.getName()} );
            this.drawBalcony(r.getBalcony(), regionBalconyPosition);
        }
    }

    private void drawBalcony(MetaBalcony balcony, Point balconyPosition){
        Image balconyImage = new Image(getClass().getResourceAsStream(IMAGES_LOCATION + "council_balcony.png"));
        AnchorPane balconyContainer = new AnchorPane();

        HBox councillorsContainer = new HBox();
        councillorsContainer.setPrefSize(BALCONYIMAGE_WIDTH, BALCONYIMAGE_HEIGHT + 25);

        for(MetaCouncillor mc : balcony.getCouncillors()){
            ImageView councillorImageView = new ImageView(IMAGES_LOCATION + "councillor_" + ColorUtils.getHexStringFromColor(mc.getColor()).toUpperCase() + ".png");
            councillorImageView.setFitWidth(COUNCILLORIMAGE_WIDTH);
            councillorImageView.setFitHeight(COUNCILLORIMAGE_HEIGHT);
            councillorsContainer.getChildren().add(councillorImageView);
        }

        ImageView balconyImageView = new ImageView(balconyImage);
        balconyImageView.setFitHeight(BALCONYIMAGE_HEIGHT);
        balconyImageView.setFitWidth(BALCONYIMAGE_WIDTH);
        balconyImageView.setLayoutY(20);

        balconyContainer.getChildren().addAll(councillorsContainer, balconyImageView);
        balconyContainer.setLayoutX(balconyPosition.getX());
        balconyContainer.setLayoutY(balconyPosition.getY());
        balconyContainer.getStyleClass().add("balcony_container");

        balconyContainer.setOnMouseClicked(event -> this.onElementClicked(balcony, balconyContainer, "selected_balcony_container"));

        Platform.runLater(() -> this.mapElementsContainer.getChildren().add(balconyContainer));
    }

    /******************
     * BUSINESS CARDS *
     ******************/
    private void drawBusinessCards(){
        for( MetaRegion r : Client.instance.getGame().getGameStatus().getGameMap().getRegions() ){
            Point counterPosition = XmlReader.getElementCoords(this.xmlElementsDocument, new String[] { "businessCards", r.getName(), "counter" } );
            Circle newCircle = new Circle();
            newCircle.setLayoutX(counterPosition.getX());
            newCircle.setLayoutY(counterPosition.getY());
            newCircle.setFill( javafx.scene.paint.Color.rgb(0, 0, 0, 0.6) );
            newCircle.setRadius(20.0);
            newCircle.getStyleClass().add("region_business_card_circle");

            Label newLabel = new Label( String.valueOf( r.getHiddenBusinessCardsDeckSize() ) );
            newLabel.setTextAlignment(TextAlignment.CENTER );
            newLabel.setTextFill(Color.WHITE);
            newLabel.setFont( Font.font(18) );
            newLabel.setLayoutX(counterPosition.getX()-10);
            newLabel.setLayoutY(counterPosition.getY()-14);
            newLabel.getStyleClass().add("region_business_card_label");

            Platform.runLater(() -> mapElementsContainer.getChildren().addAll( newCircle, newLabel) );

            List<MetaBusinessCard> regionBuyableBusinessCards = r.getBuyableBusinessCards();
            for(int i = 0; i < regionBuyableBusinessCards.size(); i++){
                MetaBusinessCard businessCard = regionBuyableBusinessCards.get(i);

                Point cardPosition = XmlReader.getElementCoords(this.xmlElementsDocument, new String[] { "businessCards", r.getName(), "buyable" + (i+1) } );
                ImageView cardImageView = new ImageView( CONFIGURATIONS_LOCATION + this.gameConfigurationName + "/business_cards/" + businessCard.getGraphicResourceId() + ".png" );
                cardImageView.setFitWidth(51);
                cardImageView.setPreserveRatio(true);
                cardImageView.setLayoutX(cardPosition.getX());
                cardImageView.setLayoutY(cardPosition.getY());
                cardImageView.getStyleClass().add("region_business_card");

                cardImageView.setOnMouseClicked(event -> this.onElementClicked(businessCard, cardImageView, "selected_region_business_card"));

                Platform.runLater(() -> mapElementsContainer.getChildren().add( cardImageView ) );
            }
        }
    }

    /***************
     * BONUS CARDS *
     ***************/
    private void drawRegionBonusCards(){
        for( MetaRegion r : Client.instance.getGame().getGameStatus().getGameMap().getRegions() ){
            Point bonusCardPosition = XmlReader.getElementCoords(this.xmlElementsDocument, new String[]{"regionBonusCards", r.getName()});
            ImageView bonusCardImage = new ImageView(CONFIGURATIONS_LOCATION + this.gameConfigurationName + "/bonusTile_" + r.getName() + ".png");
            bonusCardImage.setLayoutX(bonusCardPosition.getX());
            bonusCardImage.setLayoutY(bonusCardPosition.getY());
            bonusCardImage.setFitWidth(55);
            bonusCardImage.setFitHeight(30);
            bonusCardImage.getStyleClass().add("region_arms");

            bonusCardImage.setOnMouseClicked(event -> this.onElementClicked(r, bonusCardImage, "selected_region_arms") );

            Platform.runLater(() -> this.mapElementsContainer.getChildren().add(bonusCardImage));
        }
    }

    /*********
     * TOWNS *
     *********/
    private void drawTownsAndRewardTokens(){
        for(MetaTown t : Client.instance.getGame().getGameStatus().getGameMap().getTowns()){
            Point townIndicatorPosition = XmlReader.getElementCoords(this.xmlElementsDocument, new String[]{"towns", t.getName()} );
            Circle townIndicator = new Circle(50, Color.TRANSPARENT);
            townIndicator.setLayoutX(townIndicatorPosition.getX());
            townIndicator.setLayoutY(townIndicatorPosition.getY());
            townIndicator.getStyleClass().add("town_indicator");

            townIndicator.setOnMouseClicked(event -> this.onElementClicked(t, townIndicator, "selected_town_indicator"));

            Platform.runLater(() -> this.mapElementsContainer.getChildren().add(townIndicator));

            if(t.hasReward() && t.getRewardToken().getGraphicalResourceId() != null){
                ImageView rewardTokenImageView = new ImageView( CONFIGURATIONS_LOCATION + this.gameConfigurationName + "/reward_tokens/" + t.getRewardToken().getGraphicalResourceId() + ".png" );
                rewardTokenImageView.setFitHeight(46);
                rewardTokenImageView.setFitWidth(46);
                rewardTokenImageView.setLayoutX( townIndicatorPosition.getX()-45 );
                rewardTokenImageView.setLayoutY( townIndicatorPosition.getY()-63 );

                Platform.runLater(() -> this.mapElementsContainer.getChildren().add(rewardTokenImageView));
            }
        }
    }

    private void drawEmporiumsIndicators(){
        for(MetaTown t : Client.instance.getGame().getGameStatus().getGameMap().getTowns()){
            Point townLabelPosition = XmlReader.getElementCoords(this.xmlElementsDocument, new String[]{"towns", t.getName()} );
            Label newLabel = new Label(t.countEmporiums() + " emporiums");
            newLabel.getStyleClass().add("emporiums_label");
            newLabel.setLayoutX( townLabelPosition.getX() - 38);
            newLabel.setLayoutY( townLabelPosition.getY() + 30);

            Tooltip labelToolTip = new Tooltip( t.getEmporiumsList() );
            newLabel.setTooltip( labelToolTip );

            Platform.runLater(() -> this.mapElementsContainer.getChildren().add(newLabel));
        }
    }

    /***********
     * YOU TAB *
     ***********/
    private void drawPlayerTab(){
        this.drawPlayerPoliticCards();
        this.drawPlayerBusinessCards();
    }

    private void drawPlayerPoliticCards(){
        Set<MetaPoliticCard> playerPoliticCards = Client.instance.getPlayer().getPoliticsCards();

        GridPane politicCardsContainer = new GridPane();
        int cols = 5;
        int rows = (int) Math.ceil( (double) playerPoliticCards.size() / (double) cols );

        for (int i = 0; i < cols; i++) {
            ColumnConstraints colConst = new ColumnConstraints();
            politicCardsContainer.getColumnConstraints().add(colConst);
        }
        for (int i = 0; i < rows; i++) {
            RowConstraints rowConst = new RowConstraints();
            politicCardsContainer.getRowConstraints().add(rowConst);
        }

        int currentCol = 0;
        int currentRow = 0;
        for(MetaPoliticCard pc : playerPoliticCards){
            String imageUrl = IMAGES_LOCATION + "politiccard_" + ColorUtils.getHexStringFromColor(pc.getColor()) + ".png";
            if(pc.isJolly())
                imageUrl = IMAGES_LOCATION + "politiccard_jolly.png";

            ImageView politicCardImage = new ImageView( imageUrl );
            politicCardImage.setFitWidth(45);
            politicCardImage.setPreserveRatio(true);
            politicCardImage.getStyleClass().add("player_politic_card");

            politicCardImage.setOnMouseClicked(event -> this.onElementClicked(pc, politicCardImage, "player_politic_card_selected") );

            politicCardsContainer.add(politicCardImage, currentCol, currentRow);

            currentCol++;
            if(currentCol == cols){
                currentCol = 0;
                currentRow++;
            }
        }

        politicCardsContainer.setPadding(new Insets(10, 10, 10, 10));
        politicCardsContainer.setHgap(15);
        politicCardsContainer.setVgap(15);

        Platform.runLater(() -> this.ytPoliticCards.getChildren().add(politicCardsContainer));
    }

    private void drawPlayerBusinessCards(){
        Set<MetaBusinessCard> playerBusinessCard = Client.instance.getPlayer().getBusinessCards();

        GridPane businessCardContainer = new GridPane();
        int cols = 5;
        int rows = (int) Math.ceil( (double) playerBusinessCard.size() / (double) cols );

        for (int i = 0; i < cols; i++) {
            ColumnConstraints colConst = new ColumnConstraints();
            businessCardContainer.getColumnConstraints().add(colConst);
        }
        for (int i = 0; i < rows; i++) {
            RowConstraints rowConst = new RowConstraints();
            businessCardContainer.getRowConstraints().add(rowConst);
        }

        int currentCol = 0;
        int currentRow = 0;
        for(MetaBusinessCard bc : playerBusinessCard){
            ImageView businessCardImage = new ImageView( CONFIGURATIONS_LOCATION + this.gameConfigurationName + "/business_cards/" + bc.getGraphicResourceId() + ".png" );
            businessCardImage.setFitWidth(45.0);
            businessCardImage.setPreserveRatio(true);
            businessCardImage.getStyleClass().add("player_business_card");

            if(bc.isUsed()){
                ColorAdjust monochrome = new ColorAdjust();
                monochrome.setSaturation(-0.8);
                businessCardImage.setEffect(monochrome);
            }

            businessCardImage.setOnMouseClicked(event -> this.onElementClicked(bc, businessCardImage, "player_business_card_selected") );

            businessCardContainer.add(businessCardImage, currentCol, currentRow);

            currentCol++;
            if(currentCol == cols){
                currentCol = 0;
                currentRow++;
            }
        }

        businessCardContainer.setPadding(new Insets(10, 10, 10, 10));
        businessCardContainer.setHgap(15);
        businessCardContainer.setVgap(15);

        Platform.runLater(() -> this.ytBusinessCards.getChildren().add(businessCardContainer));
    }

    private void printMessages(){
        List<String> gameMessages = Client.instance.getGameMessages();
        for(int i = gameMessages.size()-1; i >= 0; i--){
            this.addMessage(gameMessages.get(i), i%2 == 0);
        }
    }

    public void onServerDisconnection(){
        Platform.runLater(() -> {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Server disconnection");
            alert.setHeaderText(null);
            alert.setContentText("Server unexpectedly closed connection");
            alert.showAndWait();
            ((Stage) this.messagesContainer.getScene().getWindow()).close();
        });
    }

    public void onGameAborted(){
        Platform.runLater(() -> {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Game Aborted");
            alert.setHeaderText(null);
            alert.setContentText("Can't continue with no players :(");
            alert.showAndWait();
            ((Stage) this.messagesContainer.getScene().getWindow()).close();
        });
    }

    public void onGameEnded(){
        Map<String, Integer> finalLeaderBoard = Client.instance.getGame().getGameStatus().getFinalLeaderboard();

        GridPane resultsContainer = new GridPane();
        resultsContainer.setVgap(10);
        resultsContainer.setHgap(10);
        int cols = 2;
        int rows = finalLeaderBoard.size();

        for (int i = 0; i < cols; i++) {
            ColumnConstraints colConst = new ColumnConstraints();
            resultsContainer.getColumnConstraints().add(colConst);
        }
        for (int i = 0; i < rows; i++) {
            RowConstraints rowConst = new RowConstraints();
            resultsContainer.getRowConstraints().add(rowConst);
        }

        int row = 0;
        for (Map.Entry<String, Integer> entry : finalLeaderBoard.entrySet())
        {
            Label playerUsername = new Label(entry.getKey());
            playerUsername.getStyleClass().add("player");

            Label playerPoints = new Label(entry.getValue().toString());
            playerPoints.getStyleClass().add("points");

            resultsContainer.add(playerUsername, 0, row);
            resultsContainer.add(playerPoints, 1, row);

            row++;
        }

        Platform.runLater(() -> {
            this.setActionsApplyCancelDisabled(true);
            this.setActionButtonsDisabled(true);
            this.blackCourtain.setVisible(true);
            this.leaderBoardPopup.setVisible(true);
            this.leaderBoardContainer.getChildren().add(resultsContainer);
        });
    }

    /******************
     * REDRAW METHODS *
     ******************/
    public void redrawNeeded(MetaGame oldGame, MetaGame newGame){
        if(MetaGamesComparator.playersChanged(oldGame, newGame))
            this.redrawPlayersList();

        if(MetaGamesComparator.freeCouncillorsChanged(oldGame, newGame))
            this.redrawFreeCouncillors();

        if(MetaGamesComparator.balconiesChanged(oldGame, newGame))
            this.redrawBalconies();

        if(MetaGamesComparator.regionBusinessCardsChanged(oldGame, newGame))
            this.redrawRegionsBusinessCards();

        if(MetaGamesComparator.emporiumsChanged(oldGame, newGame))
            this.redrawEmporiumsIndicator();

        if(MetaGamesComparator.playerBusinessCardsChanged( Client.instance.getPlayer(), oldGame, newGame ) )
            this.redrawPlayerBusinessCards();

        if(MetaGamesComparator.playerPoliticCardsChanged( Client.instance.getPlayer(), oldGame, newGame ) )
            this.redrawPlayerPoliticCards();

        if(MetaGamesComparator.bonusCardsChanged(oldGame, newGame))
            this.redrawBoardBonusCards();
    }

    private void redrawRegionsBusinessCards(){
        LOGGER.debug("Redrawing region business cards");
        List<Node> balconyContainers = this.mapElementsContainer.getChildren().filtered(node -> (node.getStyleClass().contains("region_business_card") || node.getStyleClass().contains("region_business_card_circle") || node.getStyleClass().contains("region_business_card_label")));
        Platform.runLater(() -> this.mapElementsContainer.getChildren().removeAll(balconyContainers) );
        this.selectedElementsHelper.unselecteAll(MetaBusinessCard.class);
        this.drawBusinessCards();
    }

    private void redrawFreeCouncillors(){
        LOGGER.debug("Redrawing free councillors");
        Platform.runLater(() -> this.freeCouncillorsContainer.getChildren().clear() );
        this.selectedElementsHelper.unselecteAll(MetaCouncillor.class);
        this.drawFreeCouncillors();
    }

    private void redrawBalconies(){
        LOGGER.debug("Redrawing balconies");
        List<Node> balconyContainers = this.mapElementsContainer.getChildren().filtered(node -> node.getStyleClass().contains("balcony_container"));
        Platform.runLater(() -> this.mapElementsContainer.getChildren().removeAll(balconyContainers) );
        this.selectedElementsHelper.unselecteAll(MetaBalcony.class);
        this.drawBalconies();
    }

    private void redrawPlayersList(){
        LOGGER.debug("Redrawing players list");
        Platform.runLater(() ->  this.playersListContainer.getChildren().clear() );
        this.drawPlayersList();
    }

    private void redrawEmporiumsIndicator(){
        LOGGER.debug("Redrawing emporiums indicators");
        List<Node> emporiumLabels = this.mapElementsContainer.getChildren().filtered(node -> node.getStyleClass().contains("emporiums_label"));
        Platform.runLater(() -> this.mapElementsContainer.getChildren().removeAll(emporiumLabels));
        this.drawEmporiumsIndicators();
    }

    private void redrawPlayerPoliticCards(){
        LOGGER.debug("Redrawing player's politic cards");
        Platform.runLater(() -> this.ytPoliticCards.getChildren().clear() );
        this.selectedElementsHelper.unselecteAll(MetaPoliticCard.class);
        this.drawPlayerPoliticCards();
    }

    private void redrawPlayerBusinessCards(){
        LOGGER.debug("Redrawing player's business cards");
        Platform.runLater(() -> this.ytBusinessCards.getChildren().clear() );
        this.selectedElementsHelper.unselecteAll(MetaBusinessCard.class);
        this.drawPlayerBusinessCards();
    }

    private void redrawBoardBonusCards(){
        LOGGER.debug("Redrawing board bonus cards");
        List<Node> bonusCardImages = this.mapElementsContainer.getChildren().filtered(node -> node.getStyleClass().contains("board_bonus_card"));
        Platform.runLater(() -> this.mapElementsContainer.getChildren().removeAll(bonusCardImages));
        this.drawBonusCards();
    }

    /******************
     * MARKET METHODS *
     ******************/

    @FXML
    void onMarketButtonsClicked(MouseEvent event){
        if(event.getSource().equals(this.mtSell)){
            this.blackCourtain.setVisible(true);
            this.marketSellPopup.setVisible(true);

        }else if(event.getSource().equals(this.mpopupCancel)){
            this.blackCourtain.setVisible(false);
            this.marketSellPopup.setVisible(false);

        }else if(event.getSource().equals(this.mpopupSell)){
            this.marketSell();

        }else if(event.getSource().equals(this.mtLoadOffers)){
            this.loadMarketOffers();

        }else if(event.getSource().equals(this.mtPass)){
            this.marketPass();

        }
    }

    private void marketPass(){
        Task<RichBoolean> marketPassTask = new Task<RichBoolean>() {
            @Override
            protected RichBoolean call() throws Exception {
                return Client.instance.getOutInterface().passMarketBuyPhase();
            }
        };

        marketPassTask.setOnSucceeded(t -> {
            RichBoolean passResult = marketPassTask.getValue();
            if(!passResult.getBooleanValue()){
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error");
                alert.setHeaderText(null);
                alert.setContentText("Unable to pass market buy phase\n\nServer response: " + passResult.getMessage());
                alert.show();
            }

        });

        new Thread(marketPassTask).start();
    }

    private void marketSell(){
        Integer sellPrice;

        try {
            sellPrice = Integer.parseInt(this.mpopupPrice.getText());
        } catch (NumberFormatException e){
            sellPrice = null;
        }

        if(sellPrice == null || sellPrice < 0){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Wrong params");
            alert.setHeaderText(null);
            alert.setContentText("Please insert a valid and positive numeric value for price field");
            alert.show();

        }else{
            MetaMarketOffer marketOffer = null;
            if( this.marketSellType.getSelectedToggle().equals(this.sellBusinessCard) ){
                marketOffer = this.sellBusinessCard(sellPrice);
            }else if( this.marketSellType.getSelectedToggle().equals(this.sellPoliticCard) ){
                marketOffer = this.sellPoliticCard(sellPrice);
            }else if( this.marketSellType.getSelectedToggle().equals(this.sellAssistants) ){
                marketOffer = this.sellAssistants(sellPrice);
            }

            if(marketOffer != null){
                try {
                    RichBoolean offerPublishResult = Client.instance.getOutInterface().sendMarketOffer(marketOffer);

                    Alert alert;
                    if (offerPublishResult.getBooleanValue()) {
                        alert = new Alert(Alert.AlertType.INFORMATION);
                        alert.setTitle("Success");
                        alert.setContentText("Offer published successfully");
                        this.mtLoadOffers.fire();
                    } else {
                        alert = new Alert(Alert.AlertType.ERROR);
                        alert.setTitle("Error");
                        alert.setContentText("Unable to publish offer.\n\n" + offerPublishResult.getMessage());
                    }

                    this.blackCourtain.setVisible(false);
                    this.marketSellPopup.setVisible(false);
                    alert.setHeaderText(null);
                    alert.show();

                } catch (ClientServerCommunicationException e) {
                    LOGGER.error("Unable to deliver market offer", e);
                }
            }else{
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Wrong params");
                alert.setHeaderText(null);
                alert.setContentText("Please make sure your selection contains only one element you want to sell and amounts are valid and positive");
                alert.show();
            }
        }
    }

    private void loadMarketOffers(){
        Platform.runLater(() -> this.marketOffersContainer.getChildren().clear());

        Task<Collection<MetaMarketOffer>> loadMarketOffersTask = new Task<Collection<MetaMarketOffer>>() {
            @Override
            protected Collection<MetaMarketOffer> call() throws Exception {
                LOGGER.debug("Loading market offers");
                return Client.instance.getOutInterface().getMarketOffers();
            }
        };

        loadMarketOffersTask.setOnSucceeded(t -> {
            LOGGER.debug("Market offers loaded");
            this.printMarketOffers(loadMarketOffersTask.getValue());
        });

        new Thread(loadMarketOffersTask).start();
    }

    private void printMarketOffers(Collection<MetaMarketOffer> marketOffers){
        LOGGER.debug("Printing market offers");
        int count = 0;
        for(MetaMarketOffer marketOffer : marketOffers){
            VBox offerContainer = new VBox();

            String rowStyle = "market_offer_container_even";
            if(count%2 != 0)
                rowStyle = "market_offer_container_odd";

            offerContainer.getStyleClass().addAll("market_offer_container", rowStyle);

            Label sellerName = new Label(marketOffer.getSeller().getUsername());
            sellerName.getStyleClass().add("seller_username");

            HBox offerObjects = new HBox();
            ImageView costImage = new ImageView( IMAGES_LOCATION + "coin.png" );
            costImage.setFitHeight(46.0);
            costImage.setPreserveRatio(true);

            Label xLabel = new Label("X");
            xLabel.getStyleClass().add("x_icon");

            Label costLabel = new Label( Integer.toString( marketOffer.getCost() ) );
            costLabel.getStyleClass().add("quantity");

            offerObjects.getChildren().addAll( costImage, xLabel, costLabel );

            if( marketOffer.getBusinessCard() != null ){
                ImageView businessCardImage = new ImageView( CONFIGURATIONS_LOCATION + this.gameConfigurationName + "/business_cards/" + marketOffer.getBusinessCard().getGraphicResourceId() + ".png" );
                businessCardImage.setFitHeight(46.0);
                businessCardImage.setPreserveRatio(true);
                offerObjects.getChildren().add(businessCardImage);
            }else if(marketOffer.getPoliticCard() != null){
                ImageView politicCardImage;
                if(marketOffer.getPoliticCard().isJolly()) {
                    politicCardImage = new ImageView(IMAGES_LOCATION + "politiccard_jolly.png");
                }else{
                    politicCardImage = new ImageView(IMAGES_LOCATION + "politiccard_" + ColorUtils.getHexStringFromColor(marketOffer.getPoliticCard().getColor()) + ".png");
                }

                politicCardImage.setFitHeight(46.0);
                politicCardImage.setPreserveRatio(true);
                offerObjects.getChildren().add(politicCardImage);
            }else{
                ImageView assistantsImageView = new ImageView( IMAGES_LOCATION + "assistant.png");
                assistantsImageView.setFitHeight(46.0);
                assistantsImageView.setPreserveRatio(true);
                Label assistantsAmountLabel = new Label( Integer.toString( marketOffer.getAssistants() ) );
                assistantsAmountLabel.getStyleClass().add("quantity");
                Label xLabel2 = new Label("X");
                xLabel2.getStyleClass().add("x_icon");

                offerObjects.getChildren().addAll( assistantsImageView, xLabel2, assistantsAmountLabel );
            }

            offerContainer.setOnMouseClicked(event -> this.buyMarketOffer(marketOffer));

            offerContainer.getChildren().addAll(sellerName, offerObjects);
            Platform.runLater(() -> this.marketOffersContainer.getChildren().add(offerContainer));
            count++;
        }
    }

    private void buyMarketOffer(MetaMarketOffer marketOffer){
        Task<RichBoolean> buyMarketOfferTask = new Task<RichBoolean>() {
            @Override
            protected RichBoolean call() throws Exception {
               return Client.instance.getOutInterface().buyMarketOffer(marketOffer);
            }
        };

        buyMarketOfferTask.setOnSucceeded(t -> {
            RichBoolean buyResult = buyMarketOfferTask.getValue();

            Alert alert;
            if(!buyResult.getBooleanValue()){
                alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Unable to buy market offer");
                alert.setContentText("Server response:\n\n" + buyResult.getMessage());
            }else{
                alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Success");
                alert.setContentText("Offer bought successfully");
            }

            alert.setHeaderText(null);
            this.loadMarketOffers();
            alert.show();
        });

        new Thread(buyMarketOfferTask).start();
    }

    private MetaMarketOffer sellBusinessCard(int price){
        Collection<MetaBusinessCard> selectedBusinessCards = this.getSelectedElements(MetaBusinessCard.class);
        if(selectedBusinessCards.size() == 1)
            return new MetaMarketOffer(Client.instance.getPlayer(), selectedBusinessCards.iterator().next(), price);
        else
            return null;
    }

    private MetaMarketOffer sellPoliticCard(int price){
        Collection<MetaPoliticCard> selectedPoliticCard = this.getSelectedElements(MetaPoliticCard.class);
        if(selectedPoliticCard.size() == 1)
            return new MetaMarketOffer(Client.instance.getPlayer(), selectedPoliticCard.iterator().next(), price);
        else
            return null;
    }

    private MetaMarketOffer sellAssistants(int price){
        Integer assistantsAmount;

        try {
            assistantsAmount = Integer.parseInt(this.mpopupAssistantsAmount.getText());
        } catch (NumberFormatException e){
            assistantsAmount = null;
        }

        if(assistantsAmount != null)
            return new MetaMarketOffer(Client.instance.getPlayer(), assistantsAmount, price);
        else
            return null;
    }
}