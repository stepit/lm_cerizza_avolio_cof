package it.stepit.client.ui.gui.controllers;

import it.stepit.cscommons.communication.metaobjects.*;
import it.stepit.cscommons.communication.metaobjects.map.MetaRegion;
import it.stepit.cscommons.communication.metaobjects.map.MetaTown;

import java.util.List;
import java.util.Set;

class MetaGamesComparator {

    private MetaGamesComparator(){
        //Private constructor to hide default one
    }

    /**
     * Checks if the status of at least one player is changed between two meta games.
     *
     * @return True if at least one player changed, false if not
     */
    static boolean playersChanged(MetaGame oldGame, MetaGame newGame){
        List<MetaPlayer> oldPlayers = oldGame.getPlayers();
        List<MetaPlayer> newPlayers = newGame.getPlayers();

        for(MetaPlayer op : oldPlayers){
            if(!newPlayers.contains(op))
                return true;

            MetaPlayer np = newPlayers.get( newPlayers.indexOf(op) );

            if(op.getPoliticsCards().size() != np.getPoliticsCards().size() ||
                    op.getCoins() != np.getCoins() ||
                    op.countPoliticsCards() != np.countPoliticsCards() ||
                    op.getEmporiumsToWin() != np.getEmporiumsToWin() ||
                    op.getAssistants() != np.getAssistants() ||
                    op.getVictoryPoints() != np.getVictoryPoints() ||
                    op.getNobilityPathPosition() != np.getVictoryPoints()) {

                return true;
            }
        }

        return false;
    }

    /**
     * Checks if the list of freeCouncillors is changed between two meta games.
     *
     * @return True if the list of free councillors is changed, false if not
     */
    static boolean freeCouncillorsChanged(MetaGame oldGame, MetaGame newGame){
        Set<MetaCouncillor> oldCouncillors = oldGame.getGameStatus().getFreeCouncillors();
        Set<MetaCouncillor> newCouncillors = newGame.getGameStatus().getFreeCouncillors();

        if(oldCouncillors.size() != newCouncillors.size())
            return true;

        for(MetaCouncillor c : oldCouncillors) {
            if( !newCouncillors.contains(c) )
                return true;
        }

        return false;
    }

    /**
     * Checks if the at least one balcony changed between two meta games
     *
     * @return True if at least one balcony changed, false otherwise
     */
    static boolean balconiesChanged(MetaGame oldGame, MetaGame newGame){

        for(MetaRegion oldRegion : oldGame.getGameStatus().getGameMap().getRegions()){
            MetaRegion newRegion = newGame.getGameStatus().getGameMap().getRegion(oldRegion.getName());

            if( !compareBalconies(oldRegion.getBalcony(), newRegion.getBalcony()) )
                return true;
        }

        MetaBalcony oldKingBalcony = oldGame.getGameStatus().getKing().getBalcony();
        MetaBalcony newKingBalcony = newGame.getGameStatus().getKing().getBalcony();

        return !compareBalconies(oldKingBalcony, newKingBalcony);
    }

    private static boolean compareBalconies(MetaBalcony b1, MetaBalcony b2){
        for(MetaCouncillor oldKingBalconyCouncillor : b1.getCouncillors()){
            if(!b2.getCouncillors().contains(oldKingBalconyCouncillor))
                return false;
        }

        return true;
    }

    /**
     * Checks if at least one business card in one region changed between two meta games
     *
     * @return True if at least one business card in one region changed, false otherwise
     */
    static boolean regionBusinessCardsChanged(MetaGame oldGame, MetaGame newGame){
        for( MetaRegion oldRegion : oldGame.getGameStatus().getGameMap().getRegions() ){
            MetaRegion newRegion = newGame.getGameStatus().getGameMap().getRegion(oldRegion.getName());

            if( oldRegion.getHiddenBusinessCardsDeckSize() != newRegion.getHiddenBusinessCardsDeckSize() ||
                        !oldRegion.getBuyableBusinessCards().equals(newRegion.getBuyableBusinessCards())){
                return true;
            }
        }

        return false;
    }

    /**
     * Checks if at least one town has new emporiums between two meta games
     *
     * @return True if at least one town has new emporiums, false otherwise
     */
    static boolean emporiumsChanged(MetaGame oldGame, MetaGame newGame){
        for(MetaTown oldTown : oldGame.getGameStatus().getGameMap().getTowns()){
            MetaTown newTown = newGame.getGameStatus().getGameMap().getTown(oldTown.getName());

            if(newTown.countEmporiums() != oldTown.countEmporiums())
                return true;
        }

        return false;
    }

    /**
     * Checks if the given player has different (more or less) business cards between two meta games.
     *
     * @return True if the player has different business cards, false otherwise
     */
    static boolean playerBusinessCardsChanged(MetaPlayer player, MetaGame oldGame, MetaGame newGame){
        MetaPlayer oldPlayer = oldGame.getPlayer(player.getUsername());
        MetaPlayer newPlayer = newGame.getPlayer(player.getUsername());

        for(MetaBusinessCard oldBc : oldPlayer.getBusinessCards()){
            if( !newPlayer.getBusinessCards().contains(oldBc) )
                return true;
        }

        for(MetaBusinessCard newBc : newPlayer.getBusinessCards()){
            if( !oldPlayer.getBusinessCards().contains(newBc) )
                return true;
        }

        return false;
    }

    /**
     * Checks if the given player has different (more or less) politic cards between two meta games.
     *
     * @return True if the player has different politic cards, false otherwise
     */
    static boolean playerPoliticCardsChanged(MetaPlayer player, MetaGame oldGame, MetaGame newGame){
        MetaPlayer oldPlayer = oldGame.getPlayer(player.getUsername());
        MetaPlayer newPlayer = newGame.getPlayer(player.getUsername());

        for(MetaPoliticCard oldPc : oldPlayer.getPoliticsCards()){
            if( !newPlayer.getPoliticsCards().contains(oldPc) )
                return true;
        }

        for(MetaPoliticCard newPc : newPlayer.getPoliticsCards()){
            if( !oldPlayer.getPoliticsCards().contains(newPc) )
                return true;
        }

        return false;
    }

    static boolean bonusCardsChanged(MetaGame oldGame, MetaGame newGame){
        return oldGame.getGameStatus().getBonusCards().size() != newGame.getGameStatus().getBonusCards().size() ||
                    oldGame.getGameStatus().getKing().getBonusCards().size() != newGame.getGameStatus().getKing().getBonusCards().size();
    }
}
