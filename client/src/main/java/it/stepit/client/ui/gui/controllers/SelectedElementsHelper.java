package it.stepit.client.ui.gui.controllers;


import java.util.*;

class SelectedElementsHelper {
    private final HashMap<Class, HashSet<Object>> selectedElements;

    SelectedElementsHelper() {
        this.selectedElements = new HashMap<>();
    }

    /**
     * Adds given object into selected elements
     *
     * @param obj The object to add
     * @return true if element is added, false if element was already present
     */
    boolean makeObjectSelected(Object obj){
        this.selectedElements.putIfAbsent(obj.getClass(), new HashSet<>());

        HashSet<Object> selectedElementsPerClass = this.selectedElements.get(obj.getClass());
        if(selectedElementsPerClass.contains(obj))
            return false;
        else{
            selectedElementsPerClass.add(obj);
            return true;
        }
    }

    void makeSelected(Object obj){
        HashSet<Object> selectedElementsPerClass = this.selectedElements.get(obj.getClass());

        if(selectedElementsPerClass != null)
            selectedElementsPerClass.remove(obj);
    }

    boolean isSelected(Object obj){
        HashSet<Object> selectedElementsPerClass = this.selectedElements.get(obj.getClass());

        return selectedElementsPerClass != null && selectedElementsPerClass.contains(obj);
    }

    <T> Collection<T> getSelectedObjects(Class<T> objClass){
        if( this.selectedElements.containsKey(objClass) )
            return (HashSet<T>) this.selectedElements.get(objClass);
        else
            return Collections.<T>emptySet();
    }

    void unselecteAll(Class objClass){
        Set selectedObjs = this.selectedElements.get( objClass );
        if(selectedObjs != null)
            selectedObjs.clear();
    }
}
