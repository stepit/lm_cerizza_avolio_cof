package it.stepit.client.ui.gui.controllers;

import it.stepit.client.Client;
import it.stepit.client.GuiClient;
import it.stepit.client.communication.rmi.RmiInitialization;
import it.stepit.client.communication.socket.SocketInitialization;
import it.stepit.cscommons.communication.metaobjects.MetaGame;
import it.stepit.cscommons.exceptions.ClientServerCommunicationException;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class StartPaneController {
    private static final Logger LOGGER = LoggerFactory.getLogger(StartPaneController.class);

    @FXML
    private AnchorPane loadingPane;
    @FXML
    private AnchorPane enterGamePane;
    @FXML
    private AnchorPane loginPane;
    @FXML
    private AnchorPane waitingPane;
    @FXML
    private AnchorPane createGamePane;
    @FXML
    private TextField loginUsername;
    @FXML
    private ToggleGroup serverConnectionMethod;
    @FXML
    private RadioButton connectionMethodRmi;
    @FXML
    private TextField gameNameText;
    @FXML
    private ComboBox<String> availableGameConfiurationsCombobox;
    @FXML
    private Label waitingLabel;

    /* Variables not related to interface/fbxml */
    private String userInputUsername;

    /* Getters and Setters */
    public String getUserInputUsername() {
        return userInputUsername;
    }

    @FXML
    void onLoginSubmit(MouseEvent event) {
        this.userInputUsername= loginUsername.getText();
        this.loadingPane.setVisible(true);
        Task<Void> initializeConnectionTask = new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                try{
                    if(serverConnectionMethod.getSelectedToggle().equals(connectionMethodRmi)){
                        new RmiInitialization().start();
                    }else{
                        new SocketInitialization().start();
                    }
                }catch(ClientServerCommunicationException e){
                    LOGGER.debug("Fatal exception during connectio initialization", e);
                    onLoginResult(false, "Unable to connect to the server");
                }


                return null;
            }
        };

        new Thread(initializeConnectionTask).start();
    }

    @FXML
     void onSubmitCreateGame(MouseEvent event){
        this.loadingPane.setVisible(true);
        String selectedConfigutation= new String();
        String selectedConfiguration=  this.availableGameConfiurationsCombobox.getValue();
        if(selectedConfiguration==null){
            Platform.runLater(() -> {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error");
                alert.setHeaderText(null);
                alert.setContentText("Please select a game configuration");
                alert.show();
                this.loadingPane.setVisible(false);
            });
        } else {
            Task<Void> createGameTask = new Task<Void>() {
                @Override
                protected Void call() throws Exception {
                    try {
                        Client.instance.setGame(Client.instance.getOutInterface().createGame(selectedConfiguration, availableGameConfiurationsCombobox.getValue()));
                    } catch (ClientServerCommunicationException e) {
                        LOGGER.error("Exception while creating a new game", e);

                        Platform.runLater(() -> {
                            Alert alert = new Alert(Alert.AlertType.ERROR);
                            alert.setTitle("Error");
                            alert.setHeaderText(null);
                            alert.setContentText("Unable to create the game");
                            alert.show();
                        });
                    }

                    return null;
                }
            };

            createGameTask.setOnSucceeded(t -> {
                Platform.runLater(() -> {
                    Platform.runLater(() -> {
                        this.loadingPane.setVisible(false);
                        this.waitingPane.setVisible(true);
                    });
                });
            });

            new Thread(createGameTask).start();

        }
    }

    @FXML
    void onGotoCreateGameClicked(MouseEvent event) {
        this.loadingPane.setVisible(true);

        Task<List<String>> getAvailableGameConfigurationsTask = new Task<List<String>>() {
            @Override
            protected List<String> call() throws Exception {
                return new ArrayList<>( Client.instance.getOutInterface().getAvailableGameConfigurations() );
            }
        };

        getAvailableGameConfigurationsTask.setOnSucceeded(t -> {
            Platform.runLater(() -> {
                this.loadingPane.setVisible(false);
                ObservableList<String> gameConfigurations = FXCollections.observableList(getAvailableGameConfigurationsTask.getValue());
                this.availableGameConfiurationsCombobox.setItems(gameConfigurations);
                this.enterGamePane.setVisible(false);
                this.createGamePane.setVisible(true);
            });
        });

        new Thread(getAvailableGameConfigurationsTask).start();
    }

    @FXML
    void onJoinGameClicked(MouseEvent event){
        Platform.runLater(()->{
            this.loadingPane.setVisible(true);

            try {
                MetaGame receivedGame = Client.instance.getOutInterface().joinGame();
                if(receivedGame != null) {
                    Client.instance.setGame( receivedGame );
                    this.waitingPane.setVisible(true);
                }else{
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Unable to join a game");
                    alert.setHeaderText(null);
                    alert.setContentText("No games to join. Please, create a new game");
                    alert.show();
                }

                this.loadingPane.setVisible(false);
            } catch (ClientServerCommunicationException e) {
                LOGGER.error("Exception while joining a new game", e);
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error");
                alert.setHeaderText(null);
                alert.setContentText("Unable to join the game");
                alert.show();
            }
        });

    }

    public void onLoginResult(boolean success, String message){
        Platform.runLater(() -> {
            loadingPane.setVisible(false);

            if(success){
                loginPane.setVisible(false);
                loadingPane.setVisible(false);
                enterGamePane.setVisible(true);
            }else{
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Unable to Log-In");
                alert.setHeaderText(null);
                alert.setContentText(message);
                alert.show();
            }
        });
    }

    public void onGameRejoin(){
        Platform.runLater(() -> {
            loadingPane.setVisible(false);
            waitingPane.setVisible(true);
            waitingLabel.setText("Joining your previous game...");
        });
    }

    public void onUnstartedGameUpdate(int numOfPlayers, String message) {
        Platform.runLater(() ->  this.waitingLabel.setText(message + "\n" + numOfPlayers + " player(s) in the room") );
    }

    public void switchToGamePane() {
        Platform.runLater(() -> {
            try {
                Stage primaryStage = (Stage) this.loadingPane.getScene().getWindow();
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/javafx/gamePane.fxml"));
                primaryStage.setTitle("Game - Council of Four");
                primaryStage.setResizable(false);
                primaryStage.setScene(new Scene(loader.load()));
                primaryStage.show();

                ((GuiClient) Client.instance).setGamePaneController( loader.getController() );
            } catch (IOException e) {
                LOGGER.error("Unable to switch scene", e);
            }
        });
    }

    public void onServerDisconnection(){
        Platform.runLater(() -> {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Server disconnection");
            alert.setHeaderText(null);
            alert.setContentText("Server unexpectedly closed connection");
            alert.showAndWait();
            ((Stage) this.loadingPane.getScene().getWindow()).close();
        });
    }
}

