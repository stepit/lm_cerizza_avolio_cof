package it.stepit.cscommons.communication;

import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;
import java.util.UUID;

/**
 * This abstract class provides an unique UUID for each object instance.
 * On this UUID is based clients <-> server communication.
 *
 * hashCode and equals objects are overrided and not re-overidded by sub-class: this allows server and client
 * to equal two objects only based on the UUID.
 */
public abstract class IDableObject implements Serializable {
    private UUID id;

    protected IDableObject(){
        this.id = UUID.randomUUID();
    }

    public UUID getId(){
        return this.id;
    }

    protected void setId(UUID id){
        this.id = id;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(61, 113)
                .append(this.getId())
                .toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null)
            return false;

        if (!(obj instanceof IDableObject))
            return false;

        IDableObject idableoject = (IDableObject) obj;
        return this.getId().equals(idableoject.getId()) || super.equals(obj);
    }
}
