package it.stepit.cscommons.communication;

/**
 * This interface provide an alternative for toStirng() method in order to allow objects
 * textual representation without any violation of the toString() method contract.
 */

@FunctionalInterface
public interface ListableObject {
    String getTextDescriptor();
}
