package it.stepit.cscommons.communication.metaobjects;

import com.google.common.collect.ImmutableList;
import it.stepit.cscommons.communication.IDableObject;
import it.stepit.cscommons.communication.ListableObject;
import it.stepit.cscommons.utils.ColorUtils;

import java.io.Serializable;
import java.util.List;
import java.util.UUID;

public class MetaBalcony extends IDableObject implements Serializable, ListableObject {
    private final ImmutableList<MetaCouncillor> councillors;

    public MetaBalcony(List<MetaCouncillor> councillors, UUID modelObjectId) {
        this.councillors = ImmutableList.copyOf(councillors);
        this.setId(modelObjectId);
    }

    public ImmutableList<MetaCouncillor> getCouncillors(){
        return councillors;
    }

    @Override
    public String getTextDescriptor() {
        String result = "";
        for(MetaCouncillor c : this.councillors){
            result = result.concat(ColorUtils.getColorNameFromColor(c.getColor()) + " ");
        }
        return result;
    }
}
