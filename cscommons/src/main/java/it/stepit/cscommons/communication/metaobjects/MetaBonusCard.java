package it.stepit.cscommons.communication.metaobjects;

import it.stepit.cscommons.communication.IDableObject;
import it.stepit.cscommons.communication.ListableObject;
import it.stepit.cscommons.utils.ColorUtils;

import java.awt.*;
import java.io.Serializable;
import java.util.UUID;

public class MetaBonusCard extends IDableObject implements Serializable, ListableObject {
    private final Color color;
    private final MetaReward reward;

    public MetaBonusCard(Color color, MetaReward reward, UUID id) {
        this.setId(id);

        this.color = color;
        this.reward = reward;
    }

    public MetaReward getReward() {
        return this.reward;
    }

    public Color getColor(){
        return this.color;
    }

    @Override
    public String getTextDescriptor() {
        String colorStr = "Color: " + ColorUtils.getColorNameFromColor(this.color);
        String rewardStr = "Reward: " + this.reward.getTextDescriptor();

        return String.format( "%-18s %s", colorStr, rewardStr );
    }
}
