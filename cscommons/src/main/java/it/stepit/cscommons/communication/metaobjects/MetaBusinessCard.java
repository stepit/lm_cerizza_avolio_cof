package it.stepit.cscommons.communication.metaobjects;

import it.stepit.cscommons.communication.IDableObject;
import it.stepit.cscommons.communication.ListableObject;

import java.io.Serializable;
import java.util.UUID;

public class MetaBusinessCard extends IDableObject implements Serializable, ListableObject {
    private final String[] availableTowns;
    private final MetaReward reward;
    private final boolean used;
    private final String graphicResourceId;

    public MetaBusinessCard(String[] availableTowns, MetaReward reward, boolean used, String graphicResourceId, UUID modelId) {
        this.availableTowns = availableTowns;
        this.reward = reward;
        this.used = used;
        this.graphicResourceId = graphicResourceId;
        this.setId(modelId);
    }

    public boolean isUsed() {
        return used;
    }

    public String getGraphicResourceId() {
        return graphicResourceId;
    }

    @Override
    public String getTextDescriptor() {
        String townsStr = "Towns: ";
        for(String avt : this.availableTowns){
            townsStr = townsStr.concat(avt + " ");
        }

        String usedStr = "Used: " + this.used;
        String rewardStr = "Reward: " + this.reward.getTextDescriptor();

        return String.format( "%-17s %-17s %s", townsStr, usedStr, rewardStr );
    }
}
