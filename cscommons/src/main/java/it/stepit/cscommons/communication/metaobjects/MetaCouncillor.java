package it.stepit.cscommons.communication.metaobjects;

import it.stepit.cscommons.communication.IDableObject;
import it.stepit.cscommons.communication.ListableObject;
import it.stepit.cscommons.utils.ColorUtils;

import java.io.Serializable;
import java.util.UUID;
import java.awt.*;

public class MetaCouncillor extends IDableObject implements Serializable, ListableObject {
    private final Color color;

    public MetaCouncillor(Color color, UUID modelObjectId) {
        this.color = color;
        this.setId(modelObjectId);
    }

    public Color getColor() {
        return color;
    }

    @Override
    public String getTextDescriptor() {
        return ColorUtils.getColorNameFromColor(this.color);
    }
}
