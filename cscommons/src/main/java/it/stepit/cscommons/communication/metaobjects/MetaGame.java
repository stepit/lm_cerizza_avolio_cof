package it.stepit.cscommons.communication.metaobjects;

import com.google.common.collect.ImmutableList;
import it.stepit.cscommons.game.GameState;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * This is a lightweight copy of server Game class used for communications.
 */
public class MetaGame implements Serializable{
    private GameState gameState;
    private MetaGameStatus gameStatus;
    private HashMap<String, MetaPlayer> players;

    public MetaGame(GameState gameState, MetaGameStatus gameStatus, Map<String, MetaPlayer> players) {
        this.gameState = gameState;
        this.gameStatus = gameStatus;
        this.players = new HashMap<>(players);
    }

    public GameState getGameState() {
        return this.gameState;
    }

    public int getPlayersCount(){
        return players.size();
    }

    public MetaGameStatus getGameStatus(){
        return this.gameStatus;
    }

    public MetaPlayer getPlayer(String username){
        return this.players.get(username);
    }

    public ImmutableList<MetaPlayer> getPlayers(){
        return ImmutableList.copyOf(players.values());
    }

    public ImmutableList<MetaPlayer> getPlayersInNobilityPathStep(int nobilitPathPosition){
        ImmutableList.Builder<MetaPlayer> resBuilder = ImmutableList.builder();
        for(MetaPlayer p : this.players.values()){
            if(p.getNobilityPathPosition() == nobilitPathPosition)
                resBuilder.add(p);
        }

        return resBuilder.build();
    }

    public boolean isEndend(){
        return this.gameState == GameState.STATE_ENDED;
    }

    public boolean isAborted(){
        return this.gameState == GameState.STATE_ABORTED;
    }
}
