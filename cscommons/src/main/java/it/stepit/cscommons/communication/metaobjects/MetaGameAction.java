package it.stepit.cscommons.communication.metaobjects;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class MetaGameAction implements Serializable {
    private final String className;
    private final ArrayList<Object> gameActionParams;

    public MetaGameAction(String className, List<Object> gameActionParams) {
        this.className = className;
        this.gameActionParams = new ArrayList<>(gameActionParams);
    }

    public String getClassName() {
        return className;
    }

    public List<Object> getGameActionParams() {
        return gameActionParams;
    }
}
