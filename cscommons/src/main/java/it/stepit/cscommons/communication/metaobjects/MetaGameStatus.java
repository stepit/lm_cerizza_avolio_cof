package it.stepit.cscommons.communication.metaobjects;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.ImmutableSortedMap;
import it.stepit.cscommons.communication.metaobjects.map.MetaGameMap;
import java.io.Serializable;
import java.util.List;
import java.util.Set;
import java.util.SortedMap;

/**
 * This is a lightweight copy of server game model GameStatus class used for communications.
 */
public class MetaGameStatus implements Serializable {
    private final MetaPlayer currentPlayer;
    private final String configurationName;
    private final int freeAssistants;
    private final ImmutableSet<MetaBonusCard> bonusCards;
    private final MetaKing king;
    private final MetaGameMap gameMap;
    private final ImmutableSet<MetaCouncillor> freeCouncillors;
    private final ImmutableList<MetaNobilityPathStep> nobilityPath;
    private final ImmutableSortedMap<String, Integer> finalLeaderboard;

    public MetaGameStatus(String configurationName, MetaPlayer currentPlayer, int freeAssistants, Set<MetaBonusCard> bonusCards, MetaKing king, MetaGameMap gameMap, List<MetaNobilityPathStep> nobilityPath, Set<MetaCouncillor> freeCouncillors, SortedMap<String, Integer> finalLeaderboard) {
        this.configurationName = configurationName;
        this.currentPlayer = currentPlayer;
        this.freeAssistants = freeAssistants;
        this.king = king;
        this.gameMap = gameMap;
        this.nobilityPath = ImmutableList.copyOf(nobilityPath);
        this.bonusCards = ImmutableSet.copyOf(bonusCards);
        this.freeCouncillors = ImmutableSet.copyOf(freeCouncillors);

        if(finalLeaderboard != null)
            this.finalLeaderboard = ImmutableSortedMap.copyOf(finalLeaderboard);
        else
            this.finalLeaderboard = null;
    }

    public MetaGameMap getGameMap(){
        return this.gameMap;
    }

    public MetaPlayer getCurrentPlayer(){
        return currentPlayer;
    }

    public MetaKing getKing() {
        return king;
    }

    public ImmutableList<MetaNobilityPathStep> getNobilityPath() {
        return nobilityPath;
    }

    public int getFreeAssistants() {
        return freeAssistants;
    }

    public ImmutableSet<MetaBonusCard> getBonusCards() {
        return bonusCards;
    }

    public ImmutableSet<MetaCouncillor> getFreeCouncillors() {
        return freeCouncillors;
    }

    public ImmutableMap<String, Integer> getFinalLeaderboard() {
        return finalLeaderboard;
    }

    public String getConfigurationName() {
        return configurationName;
    }
}
