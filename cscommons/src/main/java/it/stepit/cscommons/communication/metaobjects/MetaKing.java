package it.stepit.cscommons.communication.metaobjects;

import com.google.common.collect.ImmutableList;
import it.stepit.cscommons.communication.metaobjects.map.MetaTown;

import java.io.Serializable;
import java.util.List;

public class MetaKing implements Serializable{
    private final MetaBalcony balcony;
    private final MetaTown currentTown;
    private final ImmutableList<MetaBonusCard> bonusCards;

    public MetaKing(MetaBalcony balcony, MetaTown currentTown, List<MetaBonusCard> bonusCards) {
        this.balcony = balcony;
        this.currentTown = currentTown;
        this.bonusCards = ImmutableList.copyOf(bonusCards);
    }

    public MetaTown getCurrentTown() {
        return currentTown;
    }

    public MetaBalcony getBalcony() {
        return balcony;
    }

    public int countBonusCards() {
        return this.bonusCards.size();
    }

    public ImmutableList<MetaBonusCard> getBonusCards(){
        return this.bonusCards;
    }
}
