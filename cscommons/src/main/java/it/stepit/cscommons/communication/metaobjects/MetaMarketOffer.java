package it.stepit.cscommons.communication.metaobjects;

import it.stepit.cscommons.communication.IDableObject;
import it.stepit.cscommons.communication.ListableObject;

import java.io.Serializable;
import java.util.UUID;

public class MetaMarketOffer extends IDableObject implements Serializable, ListableObject {
    private final MetaPlayer seller;
    private final MetaBusinessCard businessCard;
    private final MetaPoliticCard politicCard;
    private final int assistants;
    private final int cost;

    public MetaMarketOffer(MetaPlayer seller, MetaBusinessCard businessCardOnSale, int cost) {
        this.cost = cost;
        this.businessCard = businessCardOnSale;
        this.seller = seller;
        this.assistants = 0;
        this.politicCard = null;
    }

    public MetaMarketOffer(MetaPlayer seller, MetaPoliticCard politicCardOnSale, int cost) {
        this.cost = cost;
        this.businessCard = null;
        this.seller = seller;
        this.assistants = 0;
        this.politicCard = politicCardOnSale;

    }
    public MetaMarketOffer(MetaPlayer seller, int assistantOnSale , int cost) {
        this.cost = cost;
        this.assistants = assistantOnSale;
        this.seller=seller;
        this.businessCard = null;
        this.politicCard = null;
    }

    public MetaMarketOffer(MetaPlayer seller, MetaBusinessCard businessCard, MetaPoliticCard politicCard, int assistants, int cost, UUID modelUUID) {
        this.seller = seller;
        this.businessCard = businessCard;
        this.politicCard = politicCard;
        this.assistants = assistants;
        this.cost = cost;
        this.setId(modelUUID);
    }

    public MetaPlayer getSeller() {
        return seller;
    }

    public MetaBusinessCard getBusinessCard() {
        return businessCard;
    }

    public MetaPoliticCard getPoliticCard() {
        return politicCard;
    }

    public int getAssistants() {
        return assistants;
    }

    public int getCost() {
        return cost;
    }

    @Override
    public String getTextDescriptor() {
        String sellerUsername = "Seller: " + this.seller.getUsername();
        String costStr = "Cost: " + this.cost;

        String object;
        if(this.politicCard != null){
            object = "Politic Card: " + this.politicCard.getTextDescriptor();
        }else if(this.businessCard != null){
            object = "Business Card: " + this.politicCard.getTextDescriptor();
        }else{
            object = "Assistant: " + this.assistants;
        }

        return String.format("%-17s %-17s %s", sellerUsername, costStr, object);
    }
}
