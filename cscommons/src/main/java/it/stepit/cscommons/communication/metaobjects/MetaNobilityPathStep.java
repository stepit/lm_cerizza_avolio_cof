package it.stepit.cscommons.communication.metaobjects;

import java.io.Serializable;

public class MetaNobilityPathStep implements Serializable {
    private final MetaReward reward;

    public MetaNobilityPathStep(MetaReward reward) {
        this.reward = reward;
    }

    public MetaReward getReward() {
        return reward;
    }

    public boolean hasReward(){
        return this.reward != null;
    }
}
