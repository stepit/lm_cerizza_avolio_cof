package it.stepit.cscommons.communication.metaobjects;

import com.google.common.collect.ImmutableSet;
import it.stepit.cscommons.communication.IDableObject;
import it.stepit.cscommons.communication.ListableObject;

import java.io.Serializable;
import java.util.Collection;
import java.util.UUID;

public class MetaPlayer extends IDableObject implements Serializable, ListableObject {
    private final String username;

    private final Collection<MetaBonusCard> bonusCards;
    private final Collection<MetaBusinessCard> businessCards;
    private final Collection<MetaPoliticCard> politicsCards;
    private final int coins;
    private final int assistants;
    private final int nobilityPathPosition;
    private final int victoryPoints;
    private final int emporiumsToWin;

    public MetaPlayer(String username, Collection<MetaBonusCard> bonusCards, Collection<MetaBusinessCard> businessCards, Collection<MetaPoliticCard> politicsCards, int emporiumsToWin, int victoryPoints, int nobilityPathPosition, int assistants, int coins, UUID id) {
        this.bonusCards = ImmutableSet.copyOf(bonusCards);
        this.politicsCards = ImmutableSet.copyOf(politicsCards);
        this.businessCards = ImmutableSet.copyOf(businessCards);
        this.emporiumsToWin = emporiumsToWin;
        this.victoryPoints = victoryPoints;
        this.nobilityPathPosition = nobilityPathPosition;
        this.assistants = assistants;
        this.coins = coins;
        this.username = username;
        this.setId(id);
    }

    public String getUsername() {
        return username;
    }

    public int getCoins() {
        return coins;
    }

    public int getAssistants() {
        return assistants;
    }

    public int getNobilityPathPosition() {
        return nobilityPathPosition;
    }

    public int getVictoryPoints() {
        return victoryPoints;
    }

    public int countPoliticsCards() { return politicsCards.size(); }

    public int getEmporiumsToWin() {
        return emporiumsToWin;
    }

    public ImmutableSet<MetaBonusCard> getBonusCards() {
        return ImmutableSet.copyOf(bonusCards);
    }

    public ImmutableSet<MetaBusinessCard> getBusinessCards() {
        return ImmutableSet.copyOf(businessCards);
    }

    public ImmutableSet<MetaPoliticCard> getPoliticsCards() {
        return ImmutableSet.copyOf(politicsCards);
    }

    @Override
    public String getTextDescriptor() {
        String result = "";

        result = result.concat("- " + this.getUsername());
        result = result.concat("\t\t\tCoins: " + this.getCoins() + "\tAssistants: " + this.getAssistants() + "\n");
        result = result.concat("\t\t\tVictory points: " + this.getVictoryPoints() + "\tEmporiums to build: " + this.getEmporiumsToWin() + "\n");
        result = result.concat("\t\t\tPolitics Cards: " + this.countPoliticsCards());

        return result;
    }
}
