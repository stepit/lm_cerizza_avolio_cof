package it.stepit.cscommons.communication.metaobjects;

import it.stepit.cscommons.communication.IDableObject;
import it.stepit.cscommons.communication.ListableObject;
import it.stepit.cscommons.utils.ColorUtils;

import java.awt.*;
import java.io.Serializable;
import java.util.UUID;

public class MetaPoliticCard extends IDableObject implements Serializable, ListableObject {
    private final boolean isJolly;
    private final Color color;

    public MetaPoliticCard(boolean isJolly, Color color, UUID id) {
        this.setId(id);

        this.isJolly = isJolly;
        this.color = color;
    }

    public Color getColor() {
        return color;
    }

    public boolean isJolly() {
        return isJolly;
    }

    @Override
    public String getTextDescriptor() {
        String colorStr = "Color: " + ColorUtils.getColorNameFromColor(this.color);
        String isJollyStr = "Jolly: " + this.isJolly;

        return String.format("%-18s %s", colorStr, isJollyStr);
    }
}
