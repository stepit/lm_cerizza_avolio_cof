package it.stepit.cscommons.communication.metaobjects;

import it.stepit.cscommons.communication.ListableObject;

import java.io.Serializable;

public class MetaReward implements Serializable, ListableObject {
    private final int assistants;
    private final int coins;
    private final int nobiltyPathSteps;
    private final int victoryPoints;
    private final int takeBusinessCards;
    private final int doMoreMainActions;
    private final int useRewardTokens;
    private final int useBusinessCards;
    private final int takePoliticsCards;
    private final String graphicalResourceId;

    public MetaReward(int assistants, int coins, int nobiltyPathSteps, int victoryPoints, int takeBusinessCards, int doMoreMainActions, int useRewardTokens, int useBusinessCards, int takePoliticsCards, String graphicalResourceId) {
        this.assistants = assistants;
        this.coins = coins;
        this.nobiltyPathSteps = nobiltyPathSteps;
        this.victoryPoints = victoryPoints;
        this.takeBusinessCards = takeBusinessCards;
        this.doMoreMainActions = doMoreMainActions;
        this.useRewardTokens = useRewardTokens;
        this.useBusinessCards = useBusinessCards;
        this.takePoliticsCards = takePoliticsCards;
        this.graphicalResourceId = graphicalResourceId;
    }

    public String getGraphicalResourceId() {
        return graphicalResourceId;
    }

    @Override
    public String getTextDescriptor() {
        String result = "";
        if(this.assistants != 0){
            result = result.concat("Assistants:" + this.assistants + " ");
        }

        if(this.coins != 0){
            result = result.concat("Coins:" + this.coins + " ");
        }

        if(this.victoryPoints != 0){
            result = result.concat("VictoryPoints:" + this.victoryPoints + " ");
        }

        if(this.nobiltyPathSteps != 0){
            result = result.concat("NobilityPathSteps:" + this.nobiltyPathSteps + " ");
        }

        if(this.doMoreMainActions != 0){
            result = result.concat("DoMoreMainActions:" + this.doMoreMainActions + " ");
        }

        if(this.takeBusinessCards != 0){
            result = result.concat("TakeBusinessCards:" + this.takeBusinessCards + " ");
        }

        if(this.useRewardTokens != 0){
            result = result.concat("UseRewardToken:" + this.useRewardTokens +  " ");
        }

        if(this.useBusinessCards != 0){
            result = result.concat("UseBusinessCards:" + this.useBusinessCards +  " ");
        }

        if(this.takePoliticsCards != 0){
            result = result.concat("TakePoliticCards:" + this.takePoliticsCards + " ");
        }

        return result;
    }
}
