package it.stepit.cscommons.communication.metaobjects.map;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;

import java.io.Serializable;
import java.util.Map;

public class MetaGameMap implements Serializable{
    private final ImmutableMap<String, MetaRegion> regions;
    private final ImmutableMap<String, MetaTown> towns;
    private final String cliMap;
    private final boolean isUserEditMap;

    public MetaGameMap(Map<String, MetaRegion> regions, Map<String, MetaTown> towns, String cliMap, boolean isUserEditMap) {
        this.regions = ImmutableMap.copyOf(regions);
        this.towns = ImmutableMap.copyOf(towns);
        this.cliMap = cliMap;
        this.isUserEditMap = isUserEditMap;
    }

    public String getCliMap() {
        return cliMap;
    }

    public boolean isUserEdit(){
        return this.isUserEditMap;
    }

    public MetaRegion getRegion(String regionName){
        return regions.get(regionName);
    }

    public MetaTown getTown(String townName){
        return towns.get(townName);
    }

    public ImmutableSet<MetaTown> getTowns(){
        return ImmutableSet.copyOf( this.towns.values() );
    }

    public ImmutableList<MetaRegion> getRegions(){
        return ImmutableList.copyOf( this.regions.values() );
    }
}
