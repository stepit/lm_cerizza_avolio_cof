package it.stepit.cscommons.communication.metaobjects.map;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import it.stepit.cscommons.communication.IDableObject;
import it.stepit.cscommons.communication.ListableObject;
import it.stepit.cscommons.communication.metaobjects.*;

import java.io.Serializable;
import java.util.*;

public class MetaRegion extends IDableObject implements Serializable, ListableObject {
    private final String name;
    private final MetaBonusCard bonusCard;
    private final ImmutableList<MetaBusinessCard> businessCards;
    private final MetaBalcony balcony;
    private final ImmutableMap<String, MetaTown> towns;

    public MetaRegion(String name, MetaBonusCard bonusCard, List<MetaBusinessCard> businessCards, MetaBalcony balcony, Map<String, MetaTown> towns, UUID id) {
        this.setId(id);
        this.name = name;
        this.bonusCard = bonusCard;
        this.businessCards = ImmutableList.copyOf(businessCards);
        this.balcony = balcony;
        this.towns = ImmutableMap.copyOf(towns);
    }

    public String getName(){
        return this.name;
    }

    public ImmutableList<MetaTown> getTowns(){
        return ImmutableList.copyOf( towns.values() );
    }

    public boolean hasReward(){
        return this.bonusCard != null;
    }

    public MetaReward getReward() {
        return bonusCard.getReward();
    }

    public MetaBalcony getBalcony() {
        return balcony;
    }

    public List<MetaBusinessCard> getBuyableBusinessCards(){
        return this.businessCards.subList(0, 2);
    }

    /**
     * Returns the amount of "hidden" business cards deck.
     * In other words, returns the size of all businessCards-2
     *
     * @return The amount of "business cards deck - buyable business cards size
     */
    public int getHiddenBusinessCardsDeckSize(){
        int val = this.businessCards.size() - 2;
        if(val < 0)
            val = 0;

        return val;
    }

    @Override
    public String getTextDescriptor() {
        return this.getName();
    }
}
