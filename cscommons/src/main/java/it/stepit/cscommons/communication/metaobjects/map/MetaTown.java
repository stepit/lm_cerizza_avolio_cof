package it.stepit.cscommons.communication.metaobjects.map;

import com.google.common.collect.ImmutableSet;
import it.stepit.cscommons.communication.IDableObject;
import it.stepit.cscommons.communication.ListableObject;
import it.stepit.cscommons.communication.metaobjects.MetaPlayer;
import it.stepit.cscommons.communication.metaobjects.MetaReward;
import it.stepit.cscommons.utils.ColorUtils;

import java.awt.*;
import java.io.Serializable;
import java.util.Set;
import java.util.UUID;

public class MetaTown extends IDableObject implements Serializable, ListableObject{
    private final String name;
    private final Color color;
    private final MetaReward rewardToken;
    private final ImmutableSet<MetaPlayer> emporiums;

    public MetaTown(String name, Color color, MetaReward rewardToken, Set<MetaPlayer> emporiums, UUID id) {
        this.setId(id);
        this.name = name;
        this.color = color;
        this.rewardToken = rewardToken;
        this.emporiums = ImmutableSet.copyOf(emporiums);
    }

    public String getName(){
        return this.name;
    }

    public MetaReward getRewardToken() {
        return rewardToken;
    }

    public boolean hasReward(){
        return this.rewardToken != null;
    }

    public int countEmporiums(){
        return this.emporiums.size();
    }

    public Set<MetaPlayer> getEmporiums(){
        return this.emporiums;
    }

    public String getEmporiumsList(){
        String res = "";
        for(MetaPlayer p : this.emporiums){
            res = res.concat(p.getUsername() + "\n");
        }

        return res;
    }

    @Override
    public String getTextDescriptor() {
        String colorString = "Color: " + ColorUtils.getColorNameFromColor(this.color);
        String emporiumsString = "Emporiums: ";

        for(MetaPlayer player : this.emporiums){
            emporiumsString = emporiumsString.concat(player.getUsername() + " ");
        }

        return String.format( "%-17s %-17s %s", this.getName(), colorString, emporiumsString );
    }
}
