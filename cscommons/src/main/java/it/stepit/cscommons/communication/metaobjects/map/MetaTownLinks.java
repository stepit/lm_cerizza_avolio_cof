package it.stepit.cscommons.communication.metaobjects.map;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;

import java.io.Serializable;
import java.util.Map;
import java.util.UUID;

public class MetaTownLinks implements Serializable{
    private final ImmutableMap<UUID, ImmutableSet<MetaTown>> connectionMap;

    public MetaTownLinks(Map<UUID, ImmutableSet<MetaTown>> connectionMap) {
        this.connectionMap = ImmutableMap.copyOf(connectionMap);
    }

    public ImmutableSet<MetaTown> getConnectedTowns(MetaTown town){
        return this.connectionMap.get(town.getId());
    }
}
