package it.stepit.cscommons.communication.rmi;

import it.stepit.cscommons.game.GameUpdate;
import it.stepit.cscommons.game.RewardParamsRequest;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * This interface provides remote methods definitions for Client object.
 * It defines methods that the server can call on the client.
 */
public interface RmiClientInterface extends Remote {
    /**
     * Pings the client.
     *
     * @return  Always true
     * @throws RemoteException
     */
    boolean heartbeat() throws RemoteException;

    /**
     * Sends a GameUpdate object to the client.
     *
     * @param game  The GameUpdate object containing updated Game and GameStatus objects
     * @throws RemoteException
     */
    void updateGame(GameUpdate game) throws RemoteException;

    /**
     * Send a RewardParamsRequest to the client.
     */
    void requestRewardParams(RewardParamsRequest paramsRequest) throws RemoteException;
}
