package it.stepit.cscommons.communication.rmi;

import it.stepit.cscommons.utils.RichBoolean;
import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * This interface provedis methods for exposed rmi object RmiHandshake used
 * to init rmi communication and client-server stubs exchange.
 */
public interface RmiHandshakeInterface extends Remote{
    /**
     * Called to check connection.
     *
     * @return Always true
     * @throws RemoteException
     */
    boolean hello() throws RemoteException;

    /**
     * Called to authenticate the client.
     * Passes client stub to the server through rmiClientInterface
     *
     * @param username User's username
     * @param rmiClientInterface The stub of the client for client to server remote method invocations.
     * @return A RichBoolean containing the login result of the operation and a description message
     * @throws RemoteException
     */
    RichBoolean login(String username, RmiClientInterface rmiClientInterface) throws RemoteException;

    RmiServerInterface acquireServer(String username) throws RemoteException;
}
