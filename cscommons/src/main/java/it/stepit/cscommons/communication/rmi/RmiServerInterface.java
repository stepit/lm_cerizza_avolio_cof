package it.stepit.cscommons.communication.rmi;

import it.stepit.cscommons.communication.metaobjects.MetaGame;
import it.stepit.cscommons.communication.metaobjects.MetaGameAction;
import it.stepit.cscommons.communication.metaobjects.MetaMarketOffer;
import it.stepit.cscommons.utils.RichBoolean;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Set;

public interface RmiServerInterface extends Remote {
    boolean heartbeat() throws RemoteException;

    Set<String> getAvailableGameConfigurations() throws RemoteException;

    MetaGame createGame(String gameName, String configurationName) throws RemoteException;

    MetaGame joinGame() throws RemoteException;

    RichBoolean takeGameAction(MetaGameAction gameAction) throws RemoteException;

    RichBoolean sendMarketOffer(MetaMarketOffer offer) throws RemoteException;

    Set<MetaMarketOffer> getMarketOffers() throws RemoteException;

    RichBoolean buyMarketOffer(MetaMarketOffer marketOffer) throws RemoteException;

    RichBoolean passMarketBuyPhase() throws RemoteException;
}
