package it.stepit.cscommons.communication.socket;

import java.io.Serializable;

public class SocketObject implements Serializable {
    /****************************************************
     * USED FOR PARAMS WHEN TRANSPORTEDOBJECT IS A MAP *
     ****************************************************/
    public static final transient int PARAM_NEW_GAME_NAME = 0;
    public static final transient int PARAM_GAME_CONFIGURATION_NAME = 1;

    private final SocketObjectIdentifier objectIdentifier;
    private final Serializable transportedObject;

    public SocketObject(SocketObjectIdentifier objectIdentifier, Serializable transportedObject) {
        this.objectIdentifier = objectIdentifier;
        this.transportedObject = transportedObject;
    }

    public SocketObjectIdentifier getObjectIdentifier() {
        return objectIdentifier;
    }

    public Serializable getTransportedObject() {
        return transportedObject;
    }
}
