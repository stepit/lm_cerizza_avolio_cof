package it.stepit.cscommons.communication.socket;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.EnumMap;
import java.util.HashSet;

/**
 * This class works as synchronized buffer.
 * It makes possible objects exchange between SocketInbound and SocketOutbound
 */
public class SocketObjectsBuffer {
    private static final Logger LOGGER = LoggerFactory.getLogger(SocketObjectsBuffer.class);
    private HashSet<SocketObjectIdentifier> requestedObjects;
    private EnumMap<SocketObjectIdentifier, SocketObject> caughtObjects;
    private boolean clearRequests;

    public SocketObjectsBuffer(){
        this.requestedObjects = new HashSet<>();
        this.caughtObjects = new EnumMap<>(SocketObjectIdentifier.class);
    }

    /**
     * Register a request for a specified SocketObjectIdentifier.
     * Request has not timeout and execution is terminated only when a SocketObject with given identifier is cought
     * or when clearRequests is called on the class.
     *
     * @param objectIdentifier The Socket Object Identifier requested
     * @return A SocketObject with given objectIdentifier when caught, null if clearRequests() is called first.
     */
    public synchronized SocketObject waitForObject(SocketObjectIdentifier objectIdentifier) {
        this.requestedObjects.add(objectIdentifier);
        while( !this.caughtObjects.containsKey(objectIdentifier) && !this.clearRequests ){
            try {
                wait();
            } catch (InterruptedException e) { //NOSONAR
                LOGGER.error("Unexpected InterruptedExcetion", e);
                return null;
            }
        }

        if( this.clearRequests ){
            this.clearRequests = false;
            return null;
        }

        SocketObject r = caughtObjects.get(objectIdentifier);
        caughtObjects.remove(objectIdentifier);
        return r;
    }

    public synchronized void publishObject(SocketObject socketObject){
        if(this.requestedObjects.contains(socketObject.getObjectIdentifier())){
            this.caughtObjects.put(socketObject.getObjectIdentifier(), socketObject);
            this.requestedObjects.remove(socketObject.getObjectIdentifier());

            notifyAll();
        }
    }

    public synchronized void clearRequests(){
        this.clearRequests = true;
        notifyAll();
    }
}
