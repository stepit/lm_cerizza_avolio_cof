package it.stepit.cscommons.exceptions;

public class ClientServerCommunicationException extends Exception {

    public ClientServerCommunicationException() {
        super();
    }

    public ClientServerCommunicationException(String message) {
        super(message);
    }

    public ClientServerCommunicationException(String message, Throwable cause) {
        super(message, cause);
    }

    public ClientServerCommunicationException(Throwable cause) {
        super(cause);
    }
}
