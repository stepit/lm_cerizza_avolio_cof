package it.stepit.cscommons.exceptions;

public class XmlLoadingException extends Exception {
    public XmlLoadingException() {
        super();
    }

    public XmlLoadingException(String message) {
        super(message);
    }

    public XmlLoadingException(String message, Throwable cause) {
        super(message, cause);
    }

    public XmlLoadingException(Throwable cause) {
        super(cause);
    }
}
