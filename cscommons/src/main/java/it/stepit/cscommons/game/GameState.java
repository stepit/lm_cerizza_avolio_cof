package it.stepit.cscommons.game;

public enum GameState {
    STATE_UNSTARTED,
    STATE_JUST_STARTED,
    STATE_STARTED,
    STATE_ENDED,
    STATE_ABORTED;
}
