package it.stepit.cscommons.game;

import it.stepit.cscommons.communication.metaobjects.MetaGame;
import java.io.Serializable;

public class GameUpdate implements Serializable {
    private final String message;
    private final MetaGame game;

    public GameUpdate(String message, MetaGame game) {
        this.message = message;
        this.game = game;
    }

    public String getMessage() {
        return message;
    }

    public MetaGame getGame() {
        return game;
    }
}
