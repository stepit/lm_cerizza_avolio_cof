package it.stepit.cscommons.game;

import java.io.Serializable;

public class RewardParamsRequest implements Serializable {

    private final int businessCardsToTake;
    private final int rewardTokensToUse;
    private final int businessCardToUse;

    public RewardParamsRequest(int businessCardsToTake, int rewardTokensToUse, int businessCardToUse) {
        this.businessCardsToTake = businessCardsToTake;
        this.rewardTokensToUse = rewardTokensToUse;
        this.businessCardToUse = businessCardToUse;
    }


    public int getBusinessCardsToTake() {
        return businessCardsToTake;
    }

    public int getRewardTokensToUse() {
        return rewardTokensToUse;
    }

    public int getBusinessCardToUse() {
        return businessCardToUse;
    }
}
