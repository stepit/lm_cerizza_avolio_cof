package it.stepit.cscommons.utils;

import java.util.ArrayList;
import java.awt.*;
import java.util.Arrays;

/**
 * This class provides a series of utils related to colors.
 */
public class ColorUtils {
    private static ColorName[] colorsList = {
        new ColorName("Black", 0x00, 0x00, 0x00),
        new ColorName("White", 255, 255, 255),
        new ColorName("Red", 255,0 ,0),
        new ColorName("Orange", 247, 120, 5 ),
        new ColorName("Pink", 221, 102, 144),
        new ColorName("Cyan", 8, 138, 198),
        new ColorName("Purple", 73, 51, 69),
        new ColorName("Gold", 203, 166, 30),
        new ColorName("Bronze", 195, 110, 74),
        new ColorName("Iron", 50, 118, 123),
        new ColorName("Silver", 205, 208, 201),
        new ColorName("Lime", 0, 255, 0)};

    private ColorUtils(){
        //Private constructor to hide default one
    }

    /**
     * Get the closest color name from static color list
     *
     * @param r
     * @param g
     * @param b
     * @return
     */
    public static String getColorNameFromRgb(int r, int g, int b) {
        ArrayList<ColorName> colorList = new ArrayList<>(Arrays.asList(colorsList));
        ColorName closestMatch = null;
        int minMSE = Integer.MAX_VALUE;
        int mse;
        for (ColorName c : colorList) {
            mse = c.computeMSE(r, g, b);
            if (mse < minMSE) {
                minMSE = mse;
                closestMatch = c;
            }
        }

        if (closestMatch != null) {
            return closestMatch.getName();
        } else {
            return "No matched color name.";
        }
    }

    public static String getColorNameFromColor(Color color) {
        return getColorNameFromRgb(color.getRed(), color.getGreen(),
                color.getBlue());
    }

    /**
     * Returns an HEX string from color.
     *
     * @param color The color to get the HEX string from
     * @return An HEX string for color
     */
    public static String getHexStringFromColor(Color color){
        return "#" + Integer.toHexString(color.getRGB()).substring(2).toUpperCase();
    }

    /**
     * SubClass of ColorUtils. In order to lookup color name
     *
     * @author Xiaoxiao Li
     *
     */
    private static class ColorName {
        private final int r;
        private final int g;
        private final int b;
        private final String name;

        ColorName(String name, int r, int g, int b) {
            this.r = r;
            this.g = g;
            this.b = b;
            this.name = name;
        }

        int computeMSE(int pixR, int pixG, int pixB) {
            return ((pixR - r) * (pixR - r) + (pixG - g) * (pixG - g) + (pixB - b)
                    * (pixB - b)) / 3;
        }

        public int getR() {
            return r;
        }

        public int getG() {
            return g;
        }

        public int getB() {
            return b;
        }

        public String getName() {
            return name;
        }
    }
}
