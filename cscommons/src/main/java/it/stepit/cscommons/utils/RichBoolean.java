package it.stepit.cscommons.utils;

import java.io.Serializable;

public class RichBoolean implements Serializable {
    private final boolean aBoolean;
    private final String aMessage;

    public RichBoolean(boolean booleanValue, String message) {
        this.aBoolean = booleanValue;
        this.aMessage = message;
    }

    public RichBoolean(boolean booleanValue){
        this(booleanValue, null);
    }

    public boolean getBooleanValue() {
        return aBoolean;
    }

    @Override
    public String toString() {
        return "RichBoolean:logic " + aBoolean + ", message " + aMessage;
    }

    public String getMessage() {
        return aMessage;
    }
}
