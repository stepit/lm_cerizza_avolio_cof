package it.stepit.cscommons.utils;

import it.stepit.cscommons.exceptions.XmlLoadingException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.awt.*;
import java.io.IOException;
import java.io.InputStream;

public class XmlReader {

    private XmlReader(){
    }

    public static Document getXmlDocumentFromFile(InputStream is) throws XmlLoadingException {
        try {
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document doc = documentBuilder.parse(is);
            doc.getDocumentElement().normalize();

            return doc;
        } catch (ParserConfigurationException | SAXException | IOException | NullPointerException | IllegalArgumentException e) {
            throw new XmlLoadingException(e);
        }
    }

    /**
     * Searches for a given structure in xmlDocument in order to find (driven by elementHierarchy) array
     * a given element with only two children: x and y
     *
     * @param xmlDocument The xmlDocument to search in
     * @param elementHierarchy Elements hierarchy to search for
     * @return A point containing read x and y cordinates. <italic>null</italic> if elements are not found
     */
    public static Point getElementCoords(Document xmlDocument, String[] elementHierarchy){
        if(xmlDocument == null)
            throw new IllegalArgumentException("Configuration elements xml document not loaded");

        Node lastNode = xmlDocument.getElementsByTagName("elements").item(0);
        for(String s : elementHierarchy){
            NodeList lastNodeChildNodes = lastNode.getChildNodes();

            for(int i = 0; i < lastNodeChildNodes.getLength(); i++){
                if(lastNodeChildNodes.item(i).getNodeType() == Node.ELEMENT_NODE
                        && ((Element) lastNodeChildNodes.item(i)).getTagName().equals(s) ) {

                    lastNode = lastNodeChildNodes.item(i);
                    break;
                }
            }
        }

        if( ((Element) lastNode).getTagName().equals( elementHierarchy[ elementHierarchy.length-1 ] ) ) {
            int x = Integer.parseInt(((Element) lastNode).getElementsByTagName("x").item(0).getTextContent());
            int y = Integer.parseInt(((Element) lastNode).getElementsByTagName("y").item(0).getTextContent());
            return new Point(x, y);
        }else{
            return null;
        }
    }
}
