package it.stepit.cscommons.utils;

import org.junit.Test;
import java.awt.*;
import static org.junit.Assert.*;

public class ColorUtilsTest {

    @Test
    public void testHexResults(){
        assertEquals( "#FF0000", ColorUtils.getHexStringFromColor(new Color(255, 0, 0))  );
        assertEquals( "#0066FF", ColorUtils.getHexStringFromColor(new Color(0, 102, 255)) );
        assertEquals( "#993333", ColorUtils.getHexStringFromColor(new Color(153, 51, 51)) );
        assertEquals( "#006600", ColorUtils.getHexStringFromColor(new Color(0, 102, 0)) );
    }

    @Test
    public void testColorNames(){
        assertEquals( "Red", ColorUtils.getColorNameFromColor(new Color(255, 0, 0)) );
        assertEquals( "Black", ColorUtils.getColorNameFromColor(new Color(0, 0, 0)) );
        assertEquals( "Black", ColorUtils.getColorNameFromColor(new Color(9, 30, 33)) );
    }

    @Test(expected = NullPointerException.class)
    public void testNullColor(){
        ColorUtils.getColorNameFromColor(null);
    }
}