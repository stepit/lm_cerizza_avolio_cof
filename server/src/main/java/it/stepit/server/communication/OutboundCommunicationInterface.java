package it.stepit.server.communication;

import it.stepit.cscommons.exceptions.ClientServerCommunicationException;
import it.stepit.cscommons.game.GameUpdate;
import it.stepit.cscommons.game.RewardParamsRequest;
import it.stepit.server.controller.Observer;

/**
 * This public interface defines all methods to provide server to client communication
 */
public interface OutboundCommunicationInterface extends Observer<GameUpdate> {

    /**
     * Sends to client the request for active rewards params.
     * @param request Requested params
     */
    void requestRewardParams(RewardParamsRequest request) throws ClientServerCommunicationException;
}
