package it.stepit.server.communication.rmi;

import it.stepit.cscommons.communication.rmi.RmiClientInterface;
import it.stepit.cscommons.communication.rmi.RmiHandshakeInterface;
import it.stepit.cscommons.communication.rmi.RmiServerInterface;
import it.stepit.cscommons.utils.RichBoolean;
import it.stepit.server.controller.Server;
import it.stepit.server.model.Player;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Optional;

public class RmiHandshake extends UnicastRemoteObject implements RmiHandshakeInterface{
    private static final Logger LOGGER = LoggerFactory.getLogger(RmiHandshake.class);
    private transient RmiOutbound assignedOutInterface;

    public RmiHandshake() throws RemoteException {
        //Constructor required by RMI
    }

    @Override
    public boolean hello() throws RemoteException {
        LOGGER.debug("New RMI handshake");
        return true;
    }

    @Override
    public RichBoolean login(String username, RmiClientInterface rmiClientInterface) throws RemoteException {
        LOGGER.debug("New login request for " + username);
        this.assignedOutInterface = new RmiOutbound( rmiClientInterface, username.toLowerCase() );
        return Server.getInstance().playerLogin( username.toLowerCase(), this.assignedOutInterface);
    }

    @Override
    public RmiServerInterface acquireServer(String username) throws RemoteException {
        Optional<Player> player = Server.getInstance().getPlayer(username.toLowerCase() );

        if( player.isPresent() ){
            LOGGER.debug(username.toLowerCase() + " successfully acquired server");
            this.assignedOutInterface.startClientConnectionWatcher();
            return new RmiServer( player.get().getUsername() );
        }else{
            LOGGER.debug(username.toLowerCase() + " failed to acquire server.");
            return null;
        }
    }
}
