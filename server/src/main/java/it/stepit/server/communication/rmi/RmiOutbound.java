package it.stepit.server.communication.rmi;

import it.stepit.cscommons.communication.rmi.RmiClientInterface;
import it.stepit.cscommons.exceptions.ClientServerCommunicationException;
import it.stepit.cscommons.game.GameUpdate;
import it.stepit.cscommons.game.RewardParamsRequest;
import it.stepit.server.communication.OutboundCommunicationInterface;
import it.stepit.server.controller.Server;

import java.rmi.RemoteException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * This class provides an implementation for OutboundCommunicationInterface based on RMI
 */
class RmiOutbound implements OutboundCommunicationInterface {
    private static final int CLIENT_HEARTBEAT_SECONDS_REATE = 2;
    private final RmiClientInterface rmiClient;
    private final String playerUsername;

    private ScheduledExecutorService scheduledExecutorService;

    RmiOutbound(RmiClientInterface rmiClient, String playerUsername) {
        this.rmiClient = rmiClient;
        this.playerUsername = playerUsername;
    }

    void startClientConnectionWatcher(){
        this.scheduledExecutorService = Executors.newScheduledThreadPool(1);

        Runnable clientConnectionCheck = () -> {
            try {
                rmiClient.heartbeat();
            } catch (RemoteException e) {
                Server.getInstance().onPlayerDisconnect(playerUsername);
                scheduledExecutorService.shutdown();
            }
        };

        this.scheduledExecutorService.scheduleAtFixedRate(clientConnectionCheck, 0, CLIENT_HEARTBEAT_SECONDS_REATE, TimeUnit.SECONDS);
    }

    @Override
    public void onUpdate(GameUpdate gameUpdate) throws ClientServerCommunicationException {
        try {
            rmiClient.updateGame( gameUpdate  );
        } catch (RemoteException e) {
            throw new ClientServerCommunicationException(e);
        }
    }

    @Override
    public void requestRewardParams(RewardParamsRequest request) throws ClientServerCommunicationException {
        try {
            rmiClient.requestRewardParams( request  );
        } catch (RemoteException e) {
            throw new ClientServerCommunicationException(e);
        }
    }
}
