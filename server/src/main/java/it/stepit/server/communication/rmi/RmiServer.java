package it.stepit.server.communication.rmi;

import it.stepit.cscommons.communication.metaobjects.MetaGame;
import it.stepit.cscommons.communication.metaobjects.MetaGameAction;
import it.stepit.cscommons.communication.metaobjects.MetaMarketOffer;
import it.stepit.cscommons.communication.rmi.RmiServerInterface;
import it.stepit.cscommons.utils.RichBoolean;
import it.stepit.server.configuration.GameConfigurationsManager;
import it.stepit.server.controller.Game;
import it.stepit.server.controller.Server;
import it.stepit.server.model.Player;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Collections;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

/**
 * HERE IS WHERE WE RECEIVE MESSAGES FROM CLIENT VIA RMI!
 */
class RmiServer extends UnicastRemoteObject implements RmiServerInterface{
    private static final Logger LOGGER = LoggerFactory.getLogger(RmiServer.class);
    private final String playerUsername;

    RmiServer(String playerUsername) throws RemoteException {
        this.playerUsername = playerUsername;
    }

    @Override
    public boolean heartbeat() {
        return true;
    }

    @Override
    public Set<String> getAvailableGameConfigurations() {
        return new HashSet<>( GameConfigurationsManager.getInstance().getAvailableConfigurations() );
    }

    @Override
    public MetaGame createGame(String gameName, String configurationName) {
        Game newGame = Server.getInstance().createGame(gameName, configurationName, this.playerUsername);

        return newGame.buildMetaGame();
    }

    @Override
    public MetaGame joinGame() throws RemoteException {
        Optional<Game> game = Server.getInstance().joinGame(this.playerUsername);

        if(game.isPresent())
            return game.get().buildMetaGame();

        return null;
    }

    @Override
    public RichBoolean takeGameAction(MetaGameAction gameAction) {
        LOGGER.debug("New game action from client " + this.playerUsername);

        Optional<Player> p = Server.getInstance().getPlayer(this.playerUsername);
        if(p.isPresent()) {
            Optional<Game> playerGame = Server.getInstance().getPlayerGame(p.get());
            if(playerGame.isPresent()){
                return playerGame.get().onActionReceived(p.get(), gameAction);
            }
        }

        return new RichBoolean(false);
    }

    @Override
    public RichBoolean sendMarketOffer(MetaMarketOffer offer) throws RemoteException {
        LOGGER.debug("New market offer sell from client " + this.playerUsername);

        Optional<Player> p = Server.getInstance().getPlayer(this.playerUsername);
        if(p.isPresent() && offer.getSeller().getUsername().equalsIgnoreCase(this.playerUsername)) {
            Optional<Game> playerGame = Server.getInstance().getPlayerGame(p.get());
            if(playerGame.isPresent()){
                return playerGame.get().onMarketOfferReceived(offer);
            }
        }

        return new RichBoolean(false);
    }

    @Override
    public RichBoolean passMarketBuyPhase() throws RemoteException {
        Optional<Player> p = Server.getInstance().getPlayer(this.playerUsername);
        if(p.isPresent()) {
            Optional<Game> playerGame = Server.getInstance().getPlayerGame(p.get());
            if(playerGame.isPresent()){
                return playerGame.get().onMarketBuyPhasePass(p.get());
            }
        }

        return new RichBoolean(false);
    }

    @Override
    public Set<MetaMarketOffer> getMarketOffers() throws RemoteException {
        LOGGER.debug("New market offers list request from client " + this.playerUsername);

        Optional<Player> p = Server.getInstance().getPlayer(this.playerUsername);
        if(p.isPresent()) {
            Optional<Game> playerGame = Server.getInstance().getPlayerGame(p.get());
            if(playerGame.isPresent()){
                return playerGame.get().onMarketOffersListRequest();
            }
        }

        return Collections.emptySet();
    }

    @Override
    public RichBoolean buyMarketOffer(MetaMarketOffer marketOffer) throws RemoteException {
        LOGGER.debug("New market offer buy request from client " + this.playerUsername);

        Optional<Player> p = Server.getInstance().getPlayer(this.playerUsername);
        if(p.isPresent()) {
            Optional<Game> playerGame = Server.getInstance().getPlayerGame(p.get());
            if(playerGame.isPresent()){
                return playerGame.get().onMarketOfferBuyRequest(p.get(), marketOffer);
            }
        }

        return new RichBoolean(false);
    }
}