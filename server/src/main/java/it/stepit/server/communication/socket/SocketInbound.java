package it.stepit.server.communication.socket;

import it.stepit.cscommons.communication.metaobjects.MetaGameAction;
import it.stepit.cscommons.communication.metaobjects.MetaMarketOffer;
import it.stepit.cscommons.communication.socket.SocketObject;
import it.stepit.cscommons.communication.socket.SocketObjectsBuffer;
import it.stepit.cscommons.exceptions.ClientServerCommunicationException;
import it.stepit.cscommons.utils.RichBoolean;
import it.stepit.server.configuration.GameConfigurationsManager;
import it.stepit.server.controller.Game;
import it.stepit.server.controller.Server;
import it.stepit.server.model.Player;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;

public class SocketInbound extends Thread{
    private static final Logger LOGGER = LoggerFactory.getLogger(SocketInbound.class);
    private String playerUsername;
    private Player player;
    private final ObjectInputStream socketIn;
    private final ObjectOutputStream socketOut;
    private SocketOutbound socketOutbound;
    private SocketObjectsBuffer socketObjectsBuffer;

    public SocketInbound(Socket socket) throws IOException {
        this.socketIn = new ObjectInputStream(socket.getInputStream());
        this.socketOut = new ObjectOutputStream(socket.getOutputStream());
        this.socketObjectsBuffer = new SocketObjectsBuffer();
    }

    @Override
    public void run() {
        SocketObject socketObjectInput;

        try {
            while ( (socketObjectInput = (SocketObject) socketIn.readObject()) != null ) {
                LOGGER.debug("[SOCKET][" + (playerUsername != null ? playerUsername : "") + "] " + socketObjectInput.getObjectIdentifier());
                this.handleSocketInput(socketObjectInput);
            }
        } catch (IOException e) {
            LOGGER.debug("Socket disconnection. Player: " + playerUsername);
            Server.getInstance().onPlayerDisconnect( this.playerUsername );
        } catch (ClassNotFoundException e) {
            LOGGER.error("ClassNotFoundException while reading from SocketInputStream", e);
        } catch (ClientServerCommunicationException e) {
            LOGGER.error("Unable to send response", e);
        }
    }

    private void handleSocketInput(SocketObject input) throws ClientServerCommunicationException {
        MetaMarketOffer marketOffer;

        switch(input.getObjectIdentifier()){
            case LOGIN_REQUEST:
                this.handleLoginRequest( ((String) input.getTransportedObject()).toLowerCase() );
                break;

            case GAME_CONFIGURATIONS_REQUEST:
                this.socketOutbound.sendGameConfigurations( GameConfigurationsManager.getInstance().getAvailableConfigurations() );
                break;

            case CREATE_GAME_REQEST:
                Map<Integer, String> requestParams = (Map<Integer, String>) input.getTransportedObject();
                Game newGame = Server.getInstance().createGame(requestParams.get(SocketObject.PARAM_NEW_GAME_NAME), requestParams.get(SocketObject.PARAM_GAME_CONFIGURATION_NAME), this.playerUsername);
                this.socketOutbound.sendGame(newGame.buildMetaGame());
                break;

            case JOIN_GAME_REQUEST:
                this.handleJoinGameRequest();
                break;

            case GAME_ACTION:
                this.handleGameAction( (MetaGameAction) input.getTransportedObject() );
                break;

            case PUBLISH_MARKET_OFFER:
                this.handlePublishMarketOffer( (MetaMarketOffer) input.getTransportedObject() );
                break;

            case REQUEST_MARKET_OFFERS:
                LOGGER.debug("New market offers list request from client " + this.playerUsername);
                this.socketOutbound.sendMarketOffers( (HashSet<MetaMarketOffer>) this.getPlayerGame().onMarketOffersListRequest() );
                break;

            case BUY_MARKET_OFFER:
                LOGGER.debug("New market offers buy request from client " + this.playerUsername);
                marketOffer = (MetaMarketOffer) input.getTransportedObject();
                this.socketOutbound.sendMarketBuyResult( this.getPlayerGame().onMarketOfferBuyRequest(this.player, marketOffer) );
                break;

            case PASS_MARKET_BUY_PHASE:
                this.socketOutbound.sendMarketPassResult( this.getPlayerGame().onMarketBuyPhasePass( this.player ) );
                break;

            default:
                LOGGER.warn("Unhandled socket input: " + input.getObjectIdentifier() + " sent to SocketObjectsBuffer");
                this.socketObjectsBuffer.publishObject(input);
                break;
        }
    }

    private void handleLoginRequest(String loginUsername) throws ClientServerCommunicationException {
        LOGGER.debug("New login request for " + loginUsername);

        this.socketOutbound = new SocketOutbound(this.socketOut, this.socketObjectsBuffer);
        RichBoolean loginResult = Server.getInstance().playerLogin(loginUsername, socketOutbound);
        if(loginResult.getBooleanValue()) {
            this.playerUsername = loginUsername;
            this.player = Server.getInstance().getPlayer(this.playerUsername).get();
        }

        this.socketOutbound.sendLoginResponse(loginResult);
    }

    private void handleGameAction(MetaGameAction metaGameAction) throws ClientServerCommunicationException {
        LOGGER.debug("New game action from client " + this.playerUsername);
        RichBoolean actionApplicationResult = this.getPlayerGame().onActionReceived(this.player, metaGameAction);
        this.socketOutbound.sendGameActionResult(actionApplicationResult);
    }

    private void handlePublishMarketOffer(MetaMarketOffer metaMarketOffer) throws ClientServerCommunicationException {
        LOGGER.debug("New market offer from client " + this.playerUsername);
        RichBoolean publishOfferResult =  this.getPlayerGame().onMarketOfferReceived(metaMarketOffer);
        this.socketOutbound.sendPublishOfferResult(publishOfferResult);
    }

    private void handleJoinGameRequest() throws ClientServerCommunicationException {
        Optional<Game> game = Server.getInstance().joinGame(this.playerUsername);

        if (game.isPresent())
            this.socketOutbound.sendGame( game.get().buildMetaGame() );
        else
            this.socketOutbound.sendGame( null );
    }

    private Game getPlayerGame(){
        Optional<Game> playerGame = Server.getInstance().getPlayerGame(this.player);
        if(playerGame.isPresent())
            return playerGame.get();
        else
            throw new UnsupportedOperationException("Unable to retrieve player's game.");
    }
}
