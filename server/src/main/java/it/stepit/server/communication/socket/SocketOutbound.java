package it.stepit.server.communication.socket;

import it.stepit.cscommons.communication.metaobjects.MetaGame;
import it.stepit.cscommons.communication.metaobjects.MetaMarketOffer;
import it.stepit.cscommons.communication.socket.SocketObject;
import it.stepit.cscommons.communication.socket.SocketObjectIdentifier;
import it.stepit.cscommons.communication.socket.SocketObjectsBuffer;
import it.stepit.cscommons.exceptions.ClientServerCommunicationException;
import it.stepit.cscommons.game.GameUpdate;
import it.stepit.cscommons.game.RewardParamsRequest;
import it.stepit.cscommons.utils.RichBoolean;
import it.stepit.server.communication.OutboundCommunicationInterface;
import it.stepit.server.model.Player;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * This class provides an implementation for OutboundCommunicationInterface based on sockets
 */
class SocketOutbound implements OutboundCommunicationInterface {
    private final ObjectOutputStream socketOut;

    SocketOutbound(ObjectOutputStream socketOut, SocketObjectsBuffer socketObjectsBuffer) {
        this.socketOut = socketOut;
    }

    @Override
    public void onUpdate(GameUpdate gameUpdate) throws ClientServerCommunicationException {
        this.sendObject(SocketObjectIdentifier.GAME_UPDATE, gameUpdate);
    }

    @Override
    public void requestRewardParams(RewardParamsRequest request) throws ClientServerCommunicationException {
        this.sendObject( SocketObjectIdentifier.REWARD_PARAMS_REQUEST, request );
    }

    void sendLoginResponse(RichBoolean loginResult) throws ClientServerCommunicationException {
        this.sendObject(SocketObjectIdentifier.LOGIN_RESPONSE, loginResult);
    }

    void sendGameConfigurations(Set<String> configurations) throws ClientServerCommunicationException {
        this.sendObject(SocketObjectIdentifier.GAME_CONFIGURATIONS_RESPONSE, new HashSet<>(configurations));
    }

    void sendGame(MetaGame metaGame) throws ClientServerCommunicationException {
        this.sendObject(SocketObjectIdentifier.GAME_RESPONSE, metaGame);
    }

    void sendGameActionResult(RichBoolean result) throws ClientServerCommunicationException {
        this.sendObject(SocketObjectIdentifier.GAME_ACTION_RESULT, result);
    }

    void sendPublishOfferResult(RichBoolean result) throws ClientServerCommunicationException {
        this.sendObject(SocketObjectIdentifier.PUBLISH_MARKET_OFFER_RESULT, result);
    }

    void sendMarketOffers(HashSet<MetaMarketOffer> marketOffers) throws ClientServerCommunicationException {
        this.sendObject(SocketObjectIdentifier.REQUEST_MARKET_OFFERS_RESPONSE, marketOffers);
    }

    void sendMarketBuyResult(RichBoolean buyResult) throws ClientServerCommunicationException {
        this.sendObject(SocketObjectIdentifier.BUY_MARKET_OFFER_RESPONSE, buyResult);
    }

    void sendMarketPassResult(RichBoolean passResult) throws ClientServerCommunicationException{
        this.sendObject(SocketObjectIdentifier.PASS_MARKET_BUY_PHASE_RESULT, passResult );
    }

    private void sendObject(SocketObjectIdentifier objectIdentifier, Serializable objectToTransport) throws ClientServerCommunicationException {
        SocketObject socketObject = new SocketObject(objectIdentifier, objectToTransport);
        try {
            this.socketOut.writeObject(socketObject);
        } catch (IOException e) {
            throw new ClientServerCommunicationException("Unable to send socket object", e);
        }
    }
}
