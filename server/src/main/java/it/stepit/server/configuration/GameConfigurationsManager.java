package it.stepit.server.configuration;

import it.stepit.cscommons.exceptions.XmlLoadingException;
import it.stepit.cscommons.utils.XmlReader;
import it.stepit.server.exceptions.GameConfigurationParseException;
import it.stepit.server.model.*;
import it.stepit.server.model.map.GameMap;
import it.stepit.server.model.map.Region;
import it.stepit.server.model.map.Town;
import it.stepit.server.model.map.TownLinks;
import it.stepit.server.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.ParserConfigurationException;
import java.awt.*;
import java.util.*;
import java.util.List;

public class GameConfigurationsManager {
    private static final Logger LOGGER = LoggerFactory.getLogger(GameConfigurationsManager.class);
    private static GameConfigurationsManager instance = new GameConfigurationsManager();

    private String defaultConfigurationFile;
    private Map<String, Document> loadedConfigurations;

    private GameConfigurationsManager(){
        this.loadedConfigurations = new HashMap<>();
    }

    public static GameConfigurationsManager getInstance(){
        return instance;
    }

    /**
     * Should return all available game configurations in order to let the user to choos
     * which one to ouse.
     * @return  All available configurations
     */
    public Set<String> getAvailableConfigurations(){
        //TODO Load configurations from config file
        HashSet<String> result = new HashSet<>();
        result.add("easy");
        result.add("inbox");
        result.add("hard");
        return result;
    }

    /**
     * Loads, tests and caches the default configuration file.
     * When a configuration file is preloaded using this method it's saved as default configuration file
     * for future uses.
     *
     * @param configurationFile     The configuration file to be preloaded
     * @throws GameConfigurationParseException
     */
    public void preloadDefaultConfiguration(String configurationFile) throws GameConfigurationParseException {
        this.defaultConfigurationFile = configurationFile;
        this.getConfiguration(configurationFile, true);
    }

    public GameStatus getDefaultConfiguration(){
        return this.getConfiguration( this.defaultConfigurationFile );
    }

    public GameStatus getConfiguration(String configurationFile) {
        if( !configurationFile.endsWith(".xml") ){
            configurationFile = configurationFile.concat(".xml");
        }

        try {
            return this.getConfiguration(configurationFile, false);
        } catch (GameConfigurationParseException e) {
            LOGGER.error("Unable to parse " + configurationFile + " file. Returning default game configuration", e);
            return this.getDefaultConfiguration();
        }
    }

    /**
     * Tries to return a GameStatus associated to a configuration file.
     * If the xmlDocument is not chached attempts to load it.
     * If an exception is raised during xml document loading or parsing it returns
     * a GameConfiguration associated to the default configuration;
     * If an exception is raised during defualt configuration loading or parsing a GameConfigurationParseException is raised
     *
     * @param configurationFile     The name of the configuration file to be parsed
     * @param isDefaultConfig       If this is the default game configuration or not
     * @return                      A parsed GameStatus
     * @throws GameConfigurationParseException
     */
    private GameStatus getConfiguration(String configurationFile, boolean isDefaultConfig) throws GameConfigurationParseException {
        try {
            if( !this.loadedConfigurations.containsKey( configurationFile ) ){
                LOGGER.debug("Reading new XmlDocument: " + configurationFile );
                Document newXmlDocument = XmlReader.getXmlDocumentFromFile( this.getClass().getResourceAsStream("/game_configurations/" + configurationFile) );
                this.loadedConfigurations.put(configurationFile, newXmlDocument);
            }

            return this.parseConfiguration( this.loadedConfigurations.get( configurationFile ) );
        } catch (GameConfigurationParseException | XmlLoadingException e) {
            LOGGER.error("Exception during configuration {} loading: ", configurationFile, e);

            if( isDefaultConfig ){
                throw new GameConfigurationParseException();
            }else{
                return this.parseConfiguration( this.loadedConfigurations.get( this.defaultConfigurationFile ) );
            }
        }
    }

    /**
     * Parses an xmlDcoument and returns the correspondant GameStatus.
     * Throws numeros exceptions for bad values in the configuration file.
     * @param xmlDocument   The xmlDocument to be parsed
     * @return              The correspondant GameConfiguration
     */
    private GameStatus parseConfiguration(Document xmlDocument) throws GameConfigurationParseException {

        try {
            /*
                Parse integer and string values
             */
            String configurationName = xmlDocument.getElementsByTagName("configurationName").item(0).getTextContent();

            Boolean isMarketEnabled = Boolean.parseBoolean( xmlDocument.getElementsByTagName("isMarketEnabled").item(0).getTextContent() );

            Integer emporiumsPerPlayer = Integer.parseInt( xmlDocument.getElementsByTagName("emporiumsPerPlayer").item(0).getTextContent() );

            Integer assistantsCount = Integer.parseInt( xmlDocument.getElementsByTagName("assistantsCount").item(0).getTextContent() );

            Integer politicsCardCount = Integer.parseInt( xmlDocument.getElementsByTagName("politicsCardCount").item(0).getTextContent() );

            Integer jollyPoliticsCardCount = Integer.parseInt( xmlDocument.getElementsByTagName("jollyPoliticsCardCount").item(0).getTextContent() );

            Integer politicsCardsPerPlayer = Integer.parseInt( xmlDocument.getElementsByTagName("politicsCardPerPlayer").item(0).getTextContent() );

            Integer councillorsCount = Integer.parseInt( xmlDocument.getElementsByTagName("councillorsCount").item(0).getTextContent() );

            Color jollyColor = Color.decode( xmlDocument.getElementsByTagName("jollyColor").item(0).getTextContent() );

            /*
                Parse councillors and PoliticsCards
             */
            Deque<PoliticCard> politicsCardsDeck = new ArrayDeque<>(politicsCardCount);
            Deque<Councillor> councillorsDeck = new ArrayDeque<>(councillorsCount);

            for( int i = 0; i < jollyPoliticsCardCount; i++ ){
                politicsCardsDeck.add( new PoliticCard( true, jollyColor ) );
            }

            int colorsCount = xmlDocument.getElementsByTagName("councillorColor").getLength();
            int politicsCardsPerColor = (politicsCardCount - jollyPoliticsCardCount) / colorsCount;
            int councillorsPerColor = (councillorsCount) / colorsCount;
            for( int i = 0; i < colorsCount; i++){
                Color c = Color.decode( xmlDocument.getElementsByTagName("councillorColor").item(i).getTextContent() );

                if( i == colorsCount-1){
                    politicsCardsPerColor = politicsCardCount - politicsCardsDeck.size();
                    councillorsPerColor = councillorsCount - councillorsDeck.size();
                }

                for( int j = 0; j < politicsCardsPerColor; j++ ){
                    politicsCardsDeck.add( new PoliticCard( false, c) );
                }

                for( int j = 0; j < councillorsPerColor; j++ ){
                    councillorsDeck.add( new Councillor( c) );
                }
            }

            /*
                Parse regions and towns
             */
            NodeList regionNodes = xmlDocument.getElementsByTagName("region");
            HashMap<String, Region> regions = new HashMap<>( regionNodes.getLength() );
            HashMap<String, Town> towns = new HashMap<>();
            for(int i = 0; i < regionNodes.getLength(); i++){
                Node regionNode = regionNodes.item(i);
                String regionName = regionNode.getAttributes().getNamedItem("name").getTextContent();

                Node regionBonusCardNode = this.getFirstElementChild(regionNode);
                BonusCard regionBonusCard = new BonusCard( jollyColor,
                                                            parseReward( regionBonusCardNode ));

                Region newRegion = new Region(regionName, regionBonusCard);

                NodeList regionChildNodes = regionNode.getChildNodes();
                for(int j = 0; j < regionChildNodes.getLength(); j++){
                    Node townNode = regionChildNodes.item(j);
                    if(townNode.getNodeType() != Node.ELEMENT_NODE || !("town".equals( townNode.getNodeName() ))){
                        continue;
                    }

                    NamedNodeMap townNodeAttributes = townNode.getAttributes();
                    Town newTown = new Town(    townNodeAttributes.getNamedItem("name").getTextContent(),
                                                Color.decode(townNodeAttributes.getNamedItem("color").getTextContent()),
                                                newRegion);

                    newRegion.addTown(newTown);
                    towns.put( newTown.getName(), newTown );
                }

                regions.put(regionName, newRegion);
            }

            /*
                Parse king
             */
            NamedNodeMap kingElementAttributes = xmlDocument.getElementsByTagName("king").item(0).getAttributes();
            Town kingStartTown = towns.get( kingElementAttributes.getNamedItem("startTown").getTextContent() );
            int kingCostsPerRoad = Integer.parseInt( kingElementAttributes.getNamedItem("costPerRoad").getTextContent() );

            /*
                Parse townLinks
             */
            TownLinks townLinks = new TownLinks();
            NodeList townLinksNodes = xmlDocument.getElementsByTagName("townLink");
            for(int i = 0; i < townLinksNodes.getLength(); i++){
                Node townLinkNode = townLinksNodes.item(i);
                NamedNodeMap townLinkNodeAttributes = townLinkNode.getAttributes();
                townLinks.addConnection( towns.get( townLinkNodeAttributes.getNamedItem("from").getTextContent() ),
                                        towns.get ( townLinkNodeAttributes.getNamedItem("to").getTextContent()) );
            }

            /*
                Parse nobilityPath
             */
            Node nobilityPathNode = xmlDocument.getElementsByTagName("nobilityPath").item(0);
            NodeList nobilityPathNodeChild = nobilityPathNode.getChildNodes();
            NamedNodeMap nobilityPathNodeAttributes = nobilityPathNode.getAttributes();
            int nobilityPathLength = Integer.parseInt(nobilityPathNodeAttributes.getNamedItem("length").getTextContent());
            ArrayList<NobiltyPathStep> nobilityPath = new ArrayList<>(nobilityPathLength);
            for(int i = 0; i < nobilityPathLength; i++){
                nobilityPath.add(i, new NobiltyPathStep(null));
            }

            for(int i = 0; i < nobilityPathNodeChild.getLength(); i++){
                Node rewardNode = nobilityPathNodeChild.item(i);
                NamedNodeMap rewardNodeAttributes = rewardNode.getAttributes();
                if(rewardNode.getNodeType() != Node.ELEMENT_NODE || !("reward".equals( rewardNode.getNodeName() ))){
                    continue;
                }

                nobilityPath.get( Integer.parseInt( rewardNodeAttributes.getNamedItem("position").getTextContent()) )
                            .setReward( parseReward(rewardNode) );
            }

            /*
                Parse bonus cards
             */
            Deque<BonusCard> kingBonusCards = new ArrayDeque<>();
            Set<BonusCard> bonusCards = new HashSet<>();
            NodeList bonusCardNodes = xmlDocument.getElementsByTagName("bonusCard");
            for(int i = 0; i < bonusCardNodes.getLength(); i++){
                Node bonusCardNode = bonusCardNodes.item(i);
                NamedNodeMap bonusCardNodeAttributes = bonusCardNode.getAttributes();

                if(!("bonusCards".equals( bonusCardNode.getParentNode().getNodeName() ))){
                    continue;
                }

                if( bonusCardNodeAttributes.getNamedItem("isKingCard") != null &&
                        Boolean.parseBoolean( bonusCardNodeAttributes.getNamedItem("isKingCard").getTextContent() ) ){
                    kingBonusCards.add( new BonusCard(  jollyColor,
                                                        parseReward( this.getFirstElementChild(bonusCardNode) )) );
                }else{
                    bonusCards.add( new BonusCard(  Color.decode( bonusCardNodeAttributes.getNamedItem("townColor").getTextContent() ),
                                                    parseReward( this.getFirstElementChild(bonusCardNode))));
                }
            }

            Deque<Reward> rewardTokens = this.parseRewardTokens(xmlDocument.getElementsByTagName("rewardToken"));

            /*
                Parse businessCards
             */
            Deque<BusinessCard> businessCards = new ArrayDeque<>();
            NodeList businessCardNodes = xmlDocument.getElementsByTagName("businessCard");
            for(int i = 0; i < businessCardNodes.getLength(); i++){
                Node businessCardNode = businessCardNodes.item(i);
                NamedNodeMap businessCardNodeAttributes = businessCardNode.getAttributes();

                BusinessCard newBusinessCard = new BusinessCard(businessCardNodeAttributes.getNamedItem("town").getTextContent().split(" "),
                        parseReward( this.getFirstElementChild(businessCardNode) ));
                newBusinessCard.setGraphicalResourceId(businessCardNodeAttributes.getNamedItem("graphResourceId").getTextContent());

                businessCards.add( newBusinessCard );
            }
            Set<BusinessCard> allBusinessCards = new HashSet<>(businessCards);

            /*
                Calculate maxPlayers
             */
            int mp1 = (int) (( Math.sqrt((8*assistantsCount)+1D) -1 )/2);
            int mp2 = politicsCardCount/politicsCardsPerPlayer;
            int maxPlayers = mp1 < mp2 ? mp1 : mp2;

            String cliMap = xmlDocument.getElementsByTagName("cliMap").item(0).getTextContent();

            GameMap newGameMap = new GameMap(regions, towns, townLinks, cliMap);

            /*
                Assign RewardTokens to Towns
             */
            rewardTokens = Utils.shuffleDeque(rewardTokens);
            for(Map.Entry<String, Town> entry : towns.entrySet()){
                Town town = entry.getValue();

                if(town.equals(kingStartTown))
                    continue;

                if(!rewardTokens.isEmpty()){
                    town.setRewardToken( rewardTokens.pop() );
                }else{
                    break;
                }
            }

            /*
                Assign BusinessCards and Councillors to Regions
             */
            councillorsDeck = Utils.shuffleDeque(councillorsDeck);
            businessCards = Utils.shuffleDeque(businessCards);
            for (Map.Entry<String, Region> r : regions.entrySet()) {
                Region region = r.getValue();

                List<BusinessCard> regionBusinessCards = GameConfigurationsManager.getBusinessCardsByRegion(region,  new ArrayList<>( businessCards ) );
                region.addBusinessCards(regionBusinessCards);
                businessCards.removeAll(regionBusinessCards);

                for( int i = 0; i < 4; i++){
                    if( !councillorsDeck.isEmpty() ){
                        region.getBalcony().addCouncilor( councillorsDeck.pop() );
                    }else{
                        throw new ParserConfigurationException("The value for councillorsCount it's not valid for this configuration. Each region must have 4 councillors and 4 more councillors are for king's balcony");
                    }
                }
            }

            /*
                Inizialize King object
             */
            King king = new King(   kingBonusCards,
                                    kingStartTown,
                                    kingCostsPerRoad);

            for( int i = 0; i < 4; i++){
                if( !councillorsDeck.isEmpty() ){
                    king.getBalcony().addCouncilor( councillorsDeck.pop() );
                }else{
                    throw new GameConfigurationParseException("The value for councillorsCount it's not valid for this configuration. Each region must have 4 councillors and 4 more councillors are for king's balcony");
                }
            }

            LOGGER.debug("Configuration {} parsed", configurationName);
            return new GameStatus(  configurationName, isMarketEnabled, maxPlayers,
                                    emporiumsPerPlayer, politicsCardsPerPlayer, assistantsCount,
                                    councillorsDeck, politicsCardsDeck, allBusinessCards,
                                    bonusCards, nobilityPath, newGameMap, king);
        }catch(Exception e){
            throw new GameConfigurationParseException(e);
        }
    }

    private Deque<Reward> parseRewardTokens(NodeList nodes){
        Deque<Reward> result = new ArrayDeque<>();

        for(int i = 0; i < nodes.getLength(); i++){
            Reward parsedReward = parseReward(nodes.item(i));
            if(nodes.item(i).getAttributes().getNamedItem("graphResourceId") !=null){
                parsedReward.setGraphicalResourceId( nodes.item(i).getAttributes().getNamedItem("graphResourceId").getTextContent() );
            }

            result.add(parsedReward);
        }

        return result;
    }


    /**
     * Return the first ELEMENT (and not ATTRIBUTES) child of a given node;
     * @param parent    Tha parent node
     * @return          The first child element if exists, null otherwise
     */
    private Node getFirstElementChild(Node parent){
        NodeList childNodeList = parent.getChildNodes();
        for(int i = 0; i < childNodeList.getLength(); i++){
            Node childNode = childNodeList.item(i);
            if( childNode.getNodeType() == Node.ELEMENT_NODE ){
                return childNode;
            }
        }

        return null;
    }


    /**
     * Parses a xml reward node from xml and returns a RewardObject
     * @param rewardNode    The xml node to be parsed
     * @return              The correspondant Reward object
     */
    private static Reward parseReward(Node rewardNode){
        NamedNodeMap nodeAttributes = rewardNode.getAttributes();
        int assistants = 0;
        int coins = 0;
        int nobiltyPathSteps = 0;
        int victoryPoints = 0;
        int takeBusinessCards = 0;
        int doMoreMainActions = 0;
        int useRewardTokens = 0;
        int useBusinessCards = 0;
        int takePoliticsCards = 0;

        Node assistantsAttribute = nodeAttributes.getNamedItem("assistants");
        if(assistantsAttribute != null){
            assistants = Integer.parseInt(assistantsAttribute.getTextContent());
        }

        Node coinsAttribute = nodeAttributes.getNamedItem("coins");
        if(coinsAttribute != null){
            coins = Integer.parseInt(coinsAttribute.getTextContent());
        }

        Node nobilityPathStepsAttribute = nodeAttributes.getNamedItem("nobilityPathSteps");
        if(nobilityPathStepsAttribute != null){
            nobiltyPathSteps = Integer.parseInt(nobilityPathStepsAttribute.getTextContent());
        }

        Node victoryPointsAttribute = nodeAttributes.getNamedItem("victoryPoints");
        if(victoryPointsAttribute != null){
            victoryPoints = Integer.parseInt(victoryPointsAttribute.getTextContent());
        }

        Node takeBusinessCardsAttribute = nodeAttributes.getNamedItem("takeBusinessCards");
        if(takeBusinessCardsAttribute != null){
            takeBusinessCards = Integer.parseInt(takeBusinessCardsAttribute.getTextContent());
        }

        Node doMoreMainActionsAttribute = nodeAttributes.getNamedItem("doMoreMainActions");
        if(doMoreMainActionsAttribute != null){
            doMoreMainActions = Integer.parseInt(doMoreMainActionsAttribute.getTextContent());
        }

        Node useRewardTokensAttribute = nodeAttributes.getNamedItem("userRewardTokens");
        if(useRewardTokensAttribute != null){
            useRewardTokens = Integer.parseInt(useRewardTokensAttribute.getTextContent());
        }

        Node useBusinessCardsAttribute = nodeAttributes.getNamedItem("useBusinessCards");
        if(useBusinessCardsAttribute != null){
            useBusinessCards = Integer.parseInt(useBusinessCardsAttribute.getTextContent());
        }

        Node takePoliticsCardsAttribute = nodeAttributes.getNamedItem("takePoliticsCards");
        if(takePoliticsCardsAttribute != null){
            takePoliticsCards = Integer.parseInt(takePoliticsCardsAttribute.getTextContent());
        }

        return new Reward(  assistants, coins, nobiltyPathSteps,
                            victoryPoints, takeBusinessCards, doMoreMainActions,
                            useRewardTokens, useBusinessCards, takePoliticsCards);
    }

    private static List<BusinessCard> getBusinessCardsByRegion(Region region, List<BusinessCard> businessCards){
        List<BusinessCard> result = new ArrayList<>();
        for(BusinessCard b: businessCards) {
            for( Town townInRegion : region.getTowns() ){
                if( b.isApplicableToTown(townInRegion) && !result.contains(b)){
                    result.add(b);
                }
            }
        }

        return result;
    }
}
