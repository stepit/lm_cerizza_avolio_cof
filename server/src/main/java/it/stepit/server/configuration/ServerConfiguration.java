package it.stepit.server.configuration;

import java.net.URI;

public class ServerConfiguration {

    private final Integer socketPort;
    private final Integer rmiPort;
    private final String defaultGameConfiguration;
    private final URI rmiHostAddress;

    public ServerConfiguration(int socketPort, URI rmiHostAddress, int rmiPort, String defaultGameConfiguration){
        this.socketPort = socketPort;
        this.rmiPort = rmiPort;
        this.defaultGameConfiguration = defaultGameConfiguration;
        this.rmiHostAddress = rmiHostAddress;
    }

    public int getSocketPort() {
        return socketPort;
    }

    public int getRmiPort(){
        return this.rmiPort;
    }

    public String getDefaultGameConfiguration() {
        return defaultGameConfiguration;
    }

    public URI getRmiHostAddress(){
        return this.rmiHostAddress;
    }
}
