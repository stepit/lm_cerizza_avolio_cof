package it.stepit.server.controller;

import it.stepit.cscommons.communication.metaobjects.*;
import it.stepit.cscommons.game.GameState;
import it.stepit.cscommons.game.GameUpdate;
import it.stepit.cscommons.utils.RichBoolean;
import it.stepit.server.exceptions.GameModelException;
import it.stepit.server.model.BusinessCard;
import it.stepit.server.model.GameStatus;
import it.stepit.server.model.Player;
import it.stepit.server.model.PoliticCard;
import it.stepit.server.model.actions.GameAction;
import it.stepit.server.model.market.MarketOffer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.*;

/**
 * This class is where all the game logic is applied and it's also a primitve lobby.
 * Players are first added in playerBuffer and only when the game starts they are put into gameStatus players.
 * When the game starts (Status != STATUS_UNSTARTED) players cannot be removed from playersBuffer: this is need for player match restoring.
 *
 * This class receives works as controller in the MODEL-VIEW-CONTROLLER pattern: it receives calls from
 * clients and delivers them to GameStatus after parsing and validaating them.
 */
public class Game extends Observable<GameUpdate> implements Observer<String>{
    private static final Logger LOGGER = LoggerFactory.getLogger(Game.class);
    private static final int MIN_PLAYERS_TO_START = 2;
    private static final int GAME_START_DELAY = 5000;
    private static final String GAME_ACTIONS_PACKAGE = "it.stepit.server.model.actions";
    private Timer gameStarterTimer;
    private GameState gameState;
    private GameStatus gameStatus;
    private HashMap<String, Player> playersBuffer;
    private int connectedPlayers;

    public Game(GameStatus gameStatus) {
        this.gameStatus = gameStatus;
        this.playersBuffer = new HashMap<>();
        this.gameState = GameState.STATE_UNSTARTED;
    }

    void removePlayer(Player p){
        if( this.isStarted() )
            throw new UnsupportedOperationException("Can't remove a player while game is started");

        this.playersBuffer.remove(p.getUsername());
        if(this.playersBuffer.size() < MIN_PLAYERS_TO_START && this.gameStarterTimer != null){
                this.gameStarterTimer.cancel();
        }

        this.notifyObservers( new GameUpdate(p.getUsername() + " left the game", this.buildMetaGame() ));
    }

    private void startGame(){
        this.gameState = GameState.STATE_JUST_STARTED;
        try {
            this.gameStatus.addPlayers(this.playersBuffer.values());
        } catch (GameModelException e) {
            LOGGER.error("Fatal exception while adding players to gameStatus", e);
        }

        this.gameStatus.onGameStarted();
        this.gameStatus.registerObserver(this);
        this.connectedPlayers = this.playersBuffer.size();
        Server.getInstance().onGameStarted(this);
        this.notifyObservers( new GameUpdate("Game started!", this.buildMetaGame()) );
        this.gameState = GameState.STATE_STARTED;
    }

    /**
     * Adds a player into the game.
     * Note that the player is added into the player buffer and not in the gameStatus.
     * Player are put into the GameStatus when the game starts.
     *
     * @param p The player object to add
     * @return True if the player was correctly added, false if not
     */
    public boolean addPlayer(Player p){
        if( this.isStarted() || this.isAborted() || this.isEndend() ){
            throw new UnsupportedOperationException("Can't add a player while game is started/aborted/endend");
        }

        if(playersBuffer.size() < gameStatus.getMaxPlayers()) {
            playersBuffer.put(p.getUsername(), p);

            if( this.playersBuffer.size() == MIN_PLAYERS_TO_START){
                this.startGameStartTimer();
            }

            this.notifyObservers( new GameUpdate(p.getUsername() + " joined the game", this.buildMetaGame()) );
            return true;
        }

        return false;
    }

    /**
     * Event fired by Server class when a user disconnects.
     *
     * @param player The player who disconnected
     */
    public void onPlayerDisconnect(Player player){
        this.connectedPlayers--;

        if( this.connectedPlayers < MIN_PLAYERS_TO_START){
            LOGGER.debug("A game just passed into STATE_ABORTED state due to lack of players");
            this.gameState = GameState.STATE_ABORTED;
            this.gameStatus.onGameAborted();
        }

        this.notifyObservers(new GameUpdate( player.getUsername() + " left the game", this.buildMetaGame() ));
    }

    /**
     * Event fired by Server class when a user reconnects.
     * This method just notifies all clients.
     * @param player The player who reconnects;
     */
    public void onPlayerReceonnects(Player player){
        this.notifyObservers(new GameUpdate( player.getUsername() + " rejoined the game", this.buildMetaGame() ));
    }

    public boolean isStarted(){
        return this.gameState == GameState.STATE_JUST_STARTED ||
                this.gameState == GameState.STATE_STARTED;
    }

    public boolean isAborted(){
        return this.gameState == GameState.STATE_ABORTED;
    }

    public boolean isEndend() {
        return this.gameState == GameState.STATE_ENDED;
    }

    public boolean containsPlayer(Player p){
        return playersBuffer.containsKey( p.getUsername() );
    }

    /**
     * Checks if the game has no more players.
     * If the game is not started yet (STATE_UNSTARTED) then we can return if the player buffer is empty.
     * If the game is any other state then we can return if connectedPlayers == 0
     *
     * @return  If there are still players in the game
     */
    public boolean isEmpty(){
        if(this.gameState == GameState.STATE_UNSTARTED) {
            return this.playersBuffer.isEmpty();
        }else{
            return this.connectedPlayers == 0;
        }
    }

    /**
     * Game receives updates from GameStatus.
     * Implemented with observable-observers pattern
     *
     * @param message A message String
     */
    @Override
    public void onUpdate(String message) {
        if( this.gameState == GameState.STATE_STARTED ) {
            if(this.gameStatus.isEndend()){
                this.gameState = GameState.STATE_ENDED;
            }

            this.notifyObservers(new GameUpdate(message, this.buildMetaGame()));
        }
    }

    private void startGameStartTimer(){
        this.gameStarterTimer = new Timer();

        this.gameStarterTimer.schedule(new java.util.TimerTask() {
            @Override
            public void run() {
                startGame();
            }}, GAME_START_DELAY );
    }

    /**
     * Event fired when inbound interface receives a game action.
     * Handles and apply the action
     *
     * @param metaGameAction The received action
     */
    public RichBoolean onActionReceived(Player p, MetaGameAction metaGameAction){
        try {
            Class<?> clazz = Class.forName(GAME_ACTIONS_PACKAGE +  "." + metaGameAction.getClassName());
            Constructor<?> constructor = clazz.getConstructor(List.class);
            GameAction gameAction = (GameAction) constructor.newInstance( this.castMetaObjects( metaGameAction.getGameActionParams()) );

            RichBoolean actionCheck = gameAction.check(p, this.gameStatus);
            if(actionCheck.getBooleanValue()){
                gameAction.apply(p, this.gameStatus);
                return new RichBoolean(true);
            }else{
                return actionCheck;
            }

        } catch (ClassNotFoundException | NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException | IndexOutOfBoundsException | ClassCastException | GameModelException e) {
            LOGGER.error("Unable to handle GameAction", e);
        }

        return new RichBoolean(false);
    }

    public RichBoolean onMarketOfferReceived(MetaMarketOffer metaMarketOffer){
        MarketOffer newMarketOffer = null;
        Player seller = this.playersBuffer.get( metaMarketOffer.getSeller().getUsername() );

        if(metaMarketOffer.getAssistants() != 0){
            newMarketOffer = new MarketOffer(seller, metaMarketOffer.getAssistants(), metaMarketOffer.getCost());

        }else if(metaMarketOffer.getBusinessCard() != null){
            newMarketOffer = new MarketOffer(seller, this.castMetaObject(metaMarketOffer.getBusinessCard(), BusinessCard.class), metaMarketOffer.getCost());

        }else if(metaMarketOffer.getPoliticCard() != null){
            newMarketOffer = new MarketOffer(seller, this.castMetaObject(metaMarketOffer.getPoliticCard(), PoliticCard.class), metaMarketOffer.getCost());
        }

        if(seller != null && newMarketOffer != null)
            return this.gameStatus.publishMarketOffer(newMarketOffer);
        else
            return new RichBoolean(false, "Internal server error");
    }

    public Set<MetaMarketOffer> onMarketOffersListRequest(){
        Set<MetaMarketOffer> marketOffers = new HashSet<>();

        for(MarketOffer mo : this.gameStatus.getMarketOffers()){
            marketOffers.add( mo.buildMetaMarketOffer() );
        }

        return marketOffers;
    }

    public RichBoolean onMarketOfferBuyRequest(Player buyer, MetaMarketOffer metaMarketOffer){
        return this.gameStatus.buyMarketOffer(buyer, metaMarketOffer.getId());
    }

    public RichBoolean onMarketBuyPhasePass(Player player){
        return this.gameStatus.passMarketBuyPhase(player);
    }

    /**
     * Casts a list of MetaObjects into correspective model objects.
     * Result list comes with the same order of input list.
     * If an element cannot be casted (missing correspective) it's just ignored.
     *
     * @return A list of model objects equivalent to metaobjects
     */
    public List<Object> castMetaObjects(List<Object> metaObjects){
        MetaObjectsConverter objectsConverter = new MetaObjectsConverter(this.gameStatus);
        return objectsConverter.castMetaObjects(metaObjects);
    }

    /**
     * Casts a MetaObject into a model object.
     * Uses generics implementation to directly return object type.
     * Can rise exceptions if casting or conversion is not successful.
     *
     * @param metaObject The metaObject to convert
     * @param convertTo The class to convert the metaobject to
     * @return Converted object if conversion if possible
     */
    public <T> T castMetaObject(Object metaObject, Class<T> convertTo){
        MetaObjectsConverter objectsConverter = new MetaObjectsConverter(this.gameStatus);
        return objectsConverter.castMetaObject(metaObject, convertTo);
    }

    public MetaGame buildMetaGame(){
        MetaGameStatus metaGameStatus = this.gameStatus.buildMetaGameStatus();
        HashMap<String, MetaPlayer> metaPlayers = new HashMap<>();
        for(Player p : this.playersBuffer.values()){
            metaPlayers.put( p.getUsername(), p.buildMetaPlayer() );
        }

        return new MetaGame(this.gameState, metaGameStatus, metaPlayers);
    }
}
