package it.stepit.server.controller;

import it.stepit.cscommons.communication.metaobjects.*;
import it.stepit.cscommons.communication.metaobjects.map.MetaRegion;
import it.stepit.cscommons.communication.metaobjects.map.MetaTown;
import it.stepit.server.model.*;
import it.stepit.server.model.map.Region;
import it.stepit.server.model.map.Town;

import java.util.*;

/**
 * This class provides methods to convert MetaObjects into Game Model methods.
 * Conversion is basically based on id attribute of IDableObject
 */
public class MetaObjectsConverter {
    private final GameStatus gameStatus;

    public MetaObjectsConverter(GameStatus gameStatus) {
        this.gameStatus = gameStatus;
    }

    /**
     * Casts a MetaObject into a model object.
     * Uses generics implementation to directly return object type.
     * Can rise exceptions if casting or conversion is not successful.
     *
     * @param metaObject The metaObject to convert
     * @param convertTo The class to convert the metaobject to
     * @return Converted object if conversion if possible
     */
    public <T> T castMetaObject(Object metaObject, Class<T> convertTo){
        List conversionList = this.castMetaObjects(Collections.singletonList(metaObject));
        return convertTo.cast(conversionList.get(0));
    }

    /**
     * Casts a list of MetaObjects into correspective model objects.
     * Result list comes with the same order of input list.
     * If an element cannot be casted (missing correspective) it's just ignored.
     *
     * @return A list of model objects equivalent to metaobjects
     */
    public List<Object> castMetaObjects(List<Object> metaObjects){
        List<Object> result = new ArrayList<>();

        for(Object obj : metaObjects){
            if(obj == null)
                continue;

            Class objClazz = obj.getClass();
            Optional<?> castedObject = null;

            if(MetaBalcony.class.equals(objClazz)){
                castedObject = this.castBalcony(obj);

            }else if(MetaCouncillor.class.equals(objClazz)){
                castedObject = this.castCouncilor(obj);

            }else if(MetaTown.class.equals(objClazz)){
                castedObject = this.castTown(obj);

            }else if(MetaRegion.class.equals(objClazz)){
                castedObject = this.castRegion(obj);

            }else if(MetaBusinessCard.class.equals(objClazz)) {
                castedObject = this.castBusinessCard(obj);

            }else if(MetaPoliticCard.class.equals(objClazz)){
                castedObject = this.castPoliticCard(obj);

            }else if(MetaKing.class.equals(objClazz)){
                castedObject = Optional.of( this.gameStatus.getKing() );

            }else if( obj instanceof Collection<?>){
                castedObject = this.castCollection(obj);

            }

            if(castedObject != null && castedObject.isPresent())
                result.add(castedObject.get());
        }

        return result;
    }

    private Optional<Balcony> castBalcony(Object obj){
        if( this.gameStatus.getKingBalcony().equals(obj) ){
            return Optional.of(this.gameStatus.getKingBalcony());
        }else{
            for( Region r : this.gameStatus.getGameMap().getRegions().values() ){
                if( r.getBalcony().equals( obj ) ){
                    return Optional.of( r.getBalcony() );
                }
            }
        }

        return Optional.empty();
    }

    private Optional<Councillor> castCouncilor(Object obj){
        for(Councillor councillor : this.gameStatus.getFreeCouncillors()){
            if(councillor.equals(obj)){
                return Optional.of(councillor);
            }
        }

        return Optional.empty();
    }

    private Optional<Town> castTown(Object obj){
        Town castedTown = this.gameStatus.getGameMap().getTowns().get( ((MetaTown) obj).getName());
        if(castedTown != null)
            return Optional.of(castedTown);
        else
            return Optional.empty();
    }

    private Optional<Region> castRegion(Object obj){
        Region castedRegion = this.gameStatus.getGameMap().getRegions().get( ((MetaRegion) obj).getName() );
        if(castedRegion != null)
            return Optional.of(castedRegion);
        else
            return Optional.empty();
    }

    private Optional<BusinessCard> castBusinessCard(Object obj){
        for(BusinessCard bc : this.gameStatus.getAllBusinessCards()){
            if(bc.equals(obj)){
                return Optional.of(bc);
            }
        }

        return Optional.empty();
    }

    private Optional<PoliticCard> castPoliticCard(Object obj){
        for(PoliticCard pc : this.gameStatus.getAllPoliticCards()){
            if(pc.equals(obj)){
                return Optional.of(pc);
            }
        }

        return Optional.empty();
    }

    private Optional<Collection<?>> castCollection(Object obj){
        Collection<?> receivedCollection = (Collection<?>) obj;

        if(receivedCollection.isEmpty())
            return Optional.empty();

        Object[] collectionElements = receivedCollection.toArray();
        if(MetaPoliticCard.class.equals(collectionElements[0].getClass())){
            Collection<PoliticCard> castedCollection = new ArrayList<>();

            for(Object politicCardToCast : collectionElements){
                Optional<PoliticCard> castedPoliticCard = this.castPoliticCard(politicCardToCast);
                if(castedPoliticCard.isPresent())
                    castedCollection.add(castedPoliticCard.get());
            }

            return Optional.of(castedCollection);

        }else if(MetaTown.class.equals(collectionElements[0].getClass())){
            Collection<Town> castedCollection = new ArrayList<>();

            for(Object townToCast : collectionElements){
                Optional<Town> castedTown = this.castTown(townToCast);
                if(castedTown.isPresent())
                    castedCollection.add(castedTown.get());
            }

            return Optional.of(castedCollection);
        }

        return Optional.empty();
    }
}
