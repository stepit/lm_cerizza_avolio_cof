package it.stepit.server.controller;

import it.stepit.cscommons.exceptions.ClientServerCommunicationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedList;
import java.util.List;

public abstract class Observable<T> {
    private static final Logger LOGGER = LoggerFactory.getLogger(Observable.class);
    private List< Observer<T> > observers = new LinkedList<>();

    public void registerObserver(Observer<T> observer){
        if( observer == null ){
            return;
        }

        if( this.observers.contains( observer ) ){
            return;
        }

        this.observers.add( observer );
    }

    public void unregisterObserver(Observer<T> observer){
        this.observers.remove( observer );
    }

    public void notifyObservers(T data){
        for( Observer<T> o : observers){
            try {
                o.onUpdate(data);
            } catch (ClientServerCommunicationException e) {
                LOGGER.warn("Unable to notify client", e);
            }
        }
    }
}
