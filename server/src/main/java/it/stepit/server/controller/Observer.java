package it.stepit.server.controller;

import it.stepit.cscommons.exceptions.ClientServerCommunicationException;

@FunctionalInterface
public interface Observer<T> {

    void onUpdate( T data ) throws ClientServerCommunicationException;
}
