package it.stepit.server.controller;

import it.stepit.cscommons.communication.rmi.RmiHandshakeInterface;
import it.stepit.cscommons.exceptions.XmlLoadingException;
import it.stepit.cscommons.utils.RichBoolean;
import it.stepit.cscommons.utils.XmlReader;
import it.stepit.server.communication.OutboundCommunicationInterface;
import it.stepit.server.communication.rmi.RmiHandshake;
import it.stepit.server.communication.socket.SocketInbound;
import it.stepit.server.configuration.GameConfigurationsManager;
import it.stepit.server.configuration.ServerConfiguration;
import it.stepit.server.exceptions.GameConfigurationParseException;
import it.stepit.server.model.Player;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URI;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Optional;

public class Server {
    private static final Logger LOGGER = LoggerFactory.getLogger(Server.class);
    private static Server instance;
    private ServerConfiguration serverConfiguration;
    private HashSet<Game> startedGames;
    private HashSet<Game> unstartedGames;
    private HashMap<String, Player> players;

    private Server() {
        LOGGER.info("Starting COF Server.");

        try {
            this.serverConfiguration = this.loadServerConfiguration();
            LOGGER.info("ServerConfig found and loaded");
        } catch (Exception e){
            LOGGER.error("Exception during serverConfig loading and parsing: ", e);
            return;
        }

        GameConfigurationsManager gameConfigurationsManager = GameConfigurationsManager.getInstance();

        String availableConfigurations = "Available configurations:";
        for(String configuration : gameConfigurationsManager.getAvailableConfigurations()){
            availableConfigurations = availableConfigurations.concat(" " + configuration);
        }
        LOGGER.info(availableConfigurations);

        try {
            gameConfigurationsManager.preloadDefaultConfiguration(this.serverConfiguration.getDefaultGameConfiguration());
            LOGGER.info("Default game configuration found and loaded");
        } catch (Exception e){
            LOGGER.error("Fatal exception during default game configuration loading", e);
            return;
        }

        this.startedGames = new HashSet<>();
        this.unstartedGames = new HashSet<>();
        this.players = new HashMap<>();
    }

    private void startListening(){
        try {
            LOGGER.info("Locating RMI registry on port: " + this.serverConfiguration.getRmiPort());
            java.rmi.registry.LocateRegistry.createRegistry( this.serverConfiguration.getRmiPort() );
        } catch (RemoteException e) {
            LOGGER.error("Unable to locate RMI registry", e);
            return;
        }

        try {
            LOGGER.info("Binding RmiHandshake remote object");

            RmiHandshakeInterface rmihi = new RmiHandshake();
            Naming.rebind("handshake", rmihi);
        } catch (MalformedURLException | RemoteException e) {
            LOGGER.error("Unable to bind RmiHandshake object", e);
            return;
        }


        LOGGER.info("Listening for socket connections on port: " + this.serverConfiguration.getSocketPort());
        ServerSocket socketListener = null;
        try {
            socketListener = new ServerSocket( this.serverConfiguration.getSocketPort() ); //NOSONAR
        } catch (IOException e) {
            LOGGER.error("Unable to start new server socket", e);
            return;
        }

        while( true ){ //NOSONAR
            Socket newSocketConnection = null;
            try {
                newSocketConnection = socketListener.accept();
                LOGGER.debug("New SOCKET connection: {}:{}", newSocketConnection.getRemoteSocketAddress(), newSocketConnection.getLocalPort());
                new SocketInbound( newSocketConnection ).start();
            } catch (IOException e) {
                LOGGER.error("Exception while listening for socket connections", e);
            }
        }
    }

    /**
     * Tries to authenticate a player. Currently, it only checks that no other connected players
     * are using the same username. It creates a new Player object or returns a player object alredy in a match.
     *
     * @param username              Player's username
     * @param outboundInterface     Player's outbound interface for communication
     * @return                      A true richboolean if logged in successfully, false + motivation if not
     */
    public synchronized RichBoolean playerLogin(String username, OutboundCommunicationInterface outboundInterface){
        username = username.toLowerCase();

        if(username.isEmpty()){
            return new RichBoolean(false, "Please enter a valid username");
        }

        Optional<Player> player = this.getPlayer(username);
        RichBoolean result;

        if( player.isPresent() ){
            if( !player.get().isConnected() ){
                /* If the player was in a still active game, log him into it */
                player.get().setOutboundInterface(outboundInterface);
                Optional<Game> playerGame = this.getPlayerGame(player.get());
                if(playerGame.isPresent()){
                    playerGame.get().onPlayerReceonnects(player.get());
                    playerGame.get().registerObserver(outboundInterface);
                    result = new RichBoolean(true, "RJN");
                }else{
                    result = new RichBoolean(true);
                }
            }else{
                result = new RichBoolean(false, "Username currently in use");
            }
        }else{
            Player newPlayer = new Player(username, outboundInterface);
            this.players.put( username, newPlayer );

            result = new RichBoolean(true);
        }

        if( result.getBooleanValue() ){
            LOGGER.info(username + " logged-in");
        }

        return result;
    }

    /**
     * Event fired when a player disconnects.
     * Makes all the required actions such as deleting it from logged players
     * or update its game status.
     *
     * @param playerUsername    The username of the disconnected player
     */
    public synchronized void onPlayerDisconnect(String playerUsername){
        Optional<Player> player = this.getPlayer(playerUsername);
        if( player.isPresent() ) {

            if( this.leaveGame(player.get()) ){
                this.players.remove( player.get().getUsername() );
            }

            player.get().setOutboundInterface(null);
            LOGGER.info(playerUsername + " logged-out");
        }
    }

    /**
     * Attempts to remove a player from a game.
     * Checks if the game is empty and removes it from games list.
     *
     * @param player    The player to remove
     * @return          True if the player can be removed from players list, false otherwise
     */
    private synchronized boolean leaveGame(Player player){
        Optional<Game> playerGame = getPlayerGame(player);

        if( playerGame.isPresent() ) {
            playerGame.get().unregisterObserver(player.getOutboundInterface());

            if ( !playerGame.get().isStarted() && !playerGame.get().isAborted() ) {
                playerGame.get().removePlayer(player);
            }else{
                playerGame.get().onPlayerDisconnect(player);
            }

            if( playerGame.get().isEmpty() ){
                this.removeGame(playerGame.get());
                return true;
            }else{
                return !playerGame.get().isStarted();
            }
        }

        return true;
    }

    /**
     * Checks if a <strong>connected</strong> player can leave its current game.
     * In other words, it checks if current player game is FINISHED, ENDED, ABORTED, UNSTARTED
     *
     * @return True if player can leave its game, false otherwise
     */
    public synchronized boolean canLeaveGame(Player player){
        Optional<Game> playerGame = this.getPlayerGame(player);

        if(playerGame.isPresent())
            return playerGame.get().isAborted() || playerGame.get().isEndend();
        else
            return true;
    }

    /**
     * Event fired when a game starts.
     * Called by Game class, it moves the game from unstarted games to started ones
     */
    public synchronized void onGameStarted(Game game){
        if( this.unstartedGames.contains(game) ){
            this.unstartedGames.remove(game);
            this.startedGames.add(game);

            LOGGER.debug("A game was started (started games: " + this.startedGames.size() + ", unstarted games: " + this.unstartedGames.size() + ")");
        }
    }

    public Optional<Player> getPlayer(String username){
        if( this.players.get(username) != null )
            return Optional.of(this.players.get(username));

        return Optional.empty();
    }

    /**
     * Checks if a player is currently in a game.
     * It checks both started and unstarted games.
     *
     * @param player    The player to search for
     * @return          An optional containing the game if found
     */
    public synchronized Optional<Game> getPlayerGame(Player player){
        for(Game g : startedGames){
            if( g.containsPlayer(player) ){
                return Optional.of(g);
            }
        }


        for(Game g : unstartedGames){
            if( g.containsPlayer(player) ){
                return Optional.of(g);
            }
        }

        return Optional.empty();
    }

    private synchronized void removeGame(Game game){
        this.startedGames.remove(game);
        this.unstartedGames.remove(game);

        LOGGER.debug("A game was removed (started games: " + this.startedGames.size() + ", unstarted games: " + this.unstartedGames.size() + ")");
    }

    public synchronized Game createGame(String gameName, String configuration, String playerUsername) {
        Optional<Player> gameCreator = this.getPlayer( playerUsername );

        if( gameCreator.isPresent() ) {
            Game newGame = new Game(GameConfigurationsManager.getInstance().getConfiguration(configuration));
            newGame.registerObserver( gameCreator.get().getOutboundInterface() );
            newGame.addPlayer( gameCreator.get() );
            unstartedGames.add(newGame);

            LOGGER.debug(playerUsername + " created a new game " + gameName + " (started games: " + this.startedGames.size() + ", unstarted games: " + this.unstartedGames.size() + ")");
            return newGame;
        }

        return null;
    }

    public synchronized Optional<Game> joinGame(String playerUsername){
        Optional<Player> player = this.getPlayer( playerUsername );

        if( player.isPresent() ) {
            for (Game game : unstartedGames) {
                if( game.addPlayer(player.get()) ){
                    game.registerObserver( player.get().getOutboundInterface() );

                    LOGGER.info( playerUsername + " joined a game");
                    return Optional.of(game);
                }
            }
        }

        return Optional.empty();
    }

    private ServerConfiguration loadServerConfiguration() throws GameConfigurationParseException {
        Document xmlDocument = null;
        try {
            xmlDocument = XmlReader.getXmlDocumentFromFile( this.getClass().getResourceAsStream("/serverConfig.xml") );
        } catch (XmlLoadingException e) {
            throw new GameConfigurationParseException(e);
        }

        return new ServerConfiguration( Integer.parseInt(xmlDocument.getElementsByTagName("socketPort").item(0).getTextContent()),
                URI.create( xmlDocument.getElementsByTagName("RMIhostAddress").item(0).getTextContent() ),
                Integer.parseInt( xmlDocument.getElementsByTagName("RMIport").item(0).getTextContent() ),
                xmlDocument.getElementsByTagName("defaultGameConfiguration").item(0).getTextContent() );
    }

    private static Server create(){
        if(instance == null){
            instance = new Server();
        }else{
            throw new UnsupportedOperationException("Server alredy started.");
        }

        return instance;
    }

    public static Server getInstance(){
        return instance;
    }

    public static void main(String[] args){
        Server.create().startListening();
    }
}
