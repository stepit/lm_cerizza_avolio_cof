package it.stepit.server.exceptions;

public class GameConfigurationParseException extends Exception {

    public GameConfigurationParseException() {
        super();
    }

    public GameConfigurationParseException(String message) {
        super(message);
    }

    public GameConfigurationParseException(Throwable cause) {
        super(cause);
    }
}
