package it.stepit.server.exceptions;


public class GameControllerException extends RuntimeException {
    public GameControllerException() {
        super();
    }

    public GameControllerException(String message) {
        super(message);
    }

    public GameControllerException(String message, Throwable cause) {
        super(message, cause);
    }

    public GameControllerException(Throwable cause) {
        super(cause);
    }
}
