package it.stepit.server.exceptions;


public class GameModelException extends RuntimeException {
    public GameModelException() {
        super();
    }

    public GameModelException(String message) {
        super(message);
    }

    public GameModelException(String message, Throwable cause) {
        super(message, cause);
    }

    public GameModelException(Throwable cause) {
        super(cause);
    }
}
