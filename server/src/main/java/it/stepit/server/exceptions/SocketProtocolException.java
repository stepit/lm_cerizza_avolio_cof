package it.stepit.server.exceptions;

public class SocketProtocolException extends RuntimeException {
    public SocketProtocolException() {
        super();
    }

    public SocketProtocolException(String message) {
        super(message);
    }

    public SocketProtocolException(String message, Throwable cause) {
        super(message, cause);
    }

    public SocketProtocolException(Throwable cause) {
        super(cause);
    }
}
