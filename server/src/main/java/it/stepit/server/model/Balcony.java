package it.stepit.server.model;

import com.google.common.collect.ImmutableList;
import it.stepit.cscommons.communication.IDableObject;
import it.stepit.cscommons.communication.metaobjects.MetaBalcony;
import it.stepit.cscommons.communication.metaobjects.MetaCouncillor;
import it.stepit.server.exceptions.GameModelException;

import java.awt.*;
import java.util.*;

public class Balcony extends IDableObject{
    private static final int COUNCILLORS_PER_BALCONY = 4;

    private ArrayDeque<Councillor> councillors;

    public Balcony(Collection<Councillor> councillors){
        if(councillors.size() != COUNCILLORS_PER_BALCONY)
            throw new IllegalArgumentException("Balcony must contain " + COUNCILLORS_PER_BALCONY + " councillors. councillors param has" + councillors.size());

        this.councillors= new ArrayDeque<>(councillors);
    }

    public Balcony(){
        this.councillors= new ArrayDeque<>();
    }

    public ImmutableList<Councillor> getCouncillors(){
        return ImmutableList.copyOf( this.councillors );
    }

    public void addCouncilor( Councillor councillor ) {
        if( councillors.size() == COUNCILLORS_PER_BALCONY )
            throw new GameModelException("Can't add more than " + COUNCILLORS_PER_BALCONY + " councillors in a balcony");

        this.councillors.add( councillor );
    }

    /**
     * Shifts a councillor in the balcony.
     * Adds newCouncillor in first position, removes and returns last councillor.
     *
     * @param newCouncillor The new councillor to add into the balcony
     * @return The last councillor removed from the balcony, null if balcony is not full
     */
    public Councillor shiftCouncillors(Councillor newCouncillor){
        this.checkCorrectlyInitialized();

        councillors.addFirst(newCouncillor);
        return councillors.pollLast();
    }

    /**
     * Determines if a balcony is corruptible given a collection of PoliticsCards
     *
     * @param selectedCards     Selected card to corrupt the balcony
     * @return                  True if corruptible, false if not
     */
    public boolean isCorruptible(Collection<PoliticCard> selectedCards){
        this.checkCorrectlyInitialized();

        ArrayList<Color> councillorsColors = new ArrayList<>();
        ArrayList<PoliticCard> selectedCardsCopy = new ArrayList<>(selectedCards);

        for(Councillor c: this.councillors)
            councillorsColors.add(c.getColor());

        for(PoliticCard p : selectedCards){
            if(councillorsColors.remove(p.getColor()) || p.isJolly()){
                selectedCardsCopy.remove(p);
            }
        }

        return selectedCardsCopy.isEmpty() &&
                ( councillorsColors.size() - selectedCards.size() - PoliticCard.countJolly(selectedCards)) < COUNCILLORS_PER_BALCONY;
    }

    /**
     * Checks if balcony is correctly initialized.
     * Simply rises an exception if not.
     *
     * @throws GameModelException
     */
    private void checkCorrectlyInitialized() {
        if(this.councillors.size() != COUNCILLORS_PER_BALCONY)
            throw new GameModelException("Balcony must contain " + COUNCILLORS_PER_BALCONY + " councillors. Has " + this.councillors.size());
    }

    public MetaBalcony buildMetaBalcony(){
        ArrayList<MetaCouncillor> metaCouncillors = new ArrayList<>();
        for( Councillor c : this.councillors ){
            metaCouncillors.add( c.buildMetaCouncillor() );
        }

        return new MetaBalcony(metaCouncillors, this.getId());
    }
}
