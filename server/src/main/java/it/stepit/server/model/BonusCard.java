package it.stepit.server.model;

import it.stepit.cscommons.communication.IDableObject;
import it.stepit.cscommons.communication.metaobjects.MetaBonusCard;

import javax.annotation.Nullable;
import java.awt.*;

public class BonusCard extends IDableObject {
    private final Color color;
    private Reward reward;

    public BonusCard(Color color, @Nullable Reward reward) {
        if(color == null)
            throw new IllegalArgumentException("Illegal null value for color param");

        this.color = color;
        this.reward = reward;
    }

    public Color getColor() {
        return this.color;
    }

    public Reward getReward() {
        return this.reward;
    }

    public void setReward(@Nullable Reward reward) {
        this.reward = reward;
    }

    public boolean hasReward(){
        return this.reward != null;
    }

    public MetaBonusCard buildMetaBonusCard(){
        return new MetaBonusCard( this.color, this.reward.buildMetaReward(), this.getId() );
    }
}
