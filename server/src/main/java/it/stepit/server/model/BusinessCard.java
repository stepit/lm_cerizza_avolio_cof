package it.stepit.server.model;

import com.google.common.collect.ImmutableCollection;
import com.google.common.collect.ImmutableSet;
import it.stepit.cscommons.communication.IDableObject;
import it.stepit.cscommons.communication.metaobjects.MetaBusinessCard;
import it.stepit.server.model.map.Town;
import javax.annotation.Nullable;
import java.util.Arrays;

public class BusinessCard extends IDableObject {
    private final String[] availableTowns;
    private Reward reward;
    private boolean used = false;
    private String graphicalResourceId;

    public BusinessCard(String[] towns, @Nullable Reward reward){
        if(towns == null)
            throw new IllegalArgumentException("Illegal null value for towns param");

        this.availableTowns = towns;
        this.reward = reward;
    }

    /**
     * Checks if the business card can be applied to a specific town
     *
     * @param town The town to check the applicability for
     * @return True if appliable, false otherwise
     */
    public boolean isApplicableToTown(Town town){
        return Arrays.asList( this.availableTowns ).contains( town.getName() );
    }

    public ImmutableCollection<String> getAvailableTownsNames(){
        return ImmutableSet.copyOf( Arrays.asList( this.availableTowns ) );
    }

    public Reward getReward() {
        return this.reward;
    }

    public void setReward(@Nullable Reward reward) {
        this.reward = reward;
    }

    public boolean isUsed() {
        return this.used;
    }

    public void setUsed(boolean used) {
        this.used = used;
    }

    public boolean hasReward(){
        return this.reward != null;
    }

    public void setGraphicalResourceId(String graphicalResourceId) {
        if(graphicalResourceId == null || graphicalResourceId.isEmpty())
            throw new IllegalArgumentException("Illegal null or empty value for graphicalResourceId param");

        this.graphicalResourceId = graphicalResourceId;
    }

    public MetaBusinessCard buildMetaBusinessCard(){
        return new MetaBusinessCard(this.availableTowns, this.reward.buildMetaReward(),
                                    this.used, this.graphicalResourceId, this.getId());
    }
}
