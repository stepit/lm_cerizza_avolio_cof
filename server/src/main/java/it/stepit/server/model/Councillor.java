package it.stepit.server.model;

import it.stepit.cscommons.communication.IDableObject;
import it.stepit.cscommons.communication.metaobjects.MetaCouncillor;

import java.awt.*;

public class Councillor extends IDableObject {
    private Color color;

    public Councillor(Color color) {
        if(color == null)
            throw new IllegalArgumentException("Illegal null value for color param");
        
        this.color = color;
    }

    public Color getColor() {
        return this.color;
    }

    MetaCouncillor buildMetaCouncillor(){
        return new MetaCouncillor(this.color, this.getId());
    }
}
