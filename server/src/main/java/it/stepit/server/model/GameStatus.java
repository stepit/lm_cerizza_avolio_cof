package it.stepit.server.model;

import com.google.common.collect.ImmutableCollection;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import it.stepit.cscommons.communication.metaobjects.*;
import it.stepit.cscommons.game.RewardParamsRequest;
import it.stepit.cscommons.utils.ColorUtils;
import it.stepit.cscommons.utils.RichBoolean;
import it.stepit.server.exceptions.GameControllerException;
import it.stepit.server.exceptions.GameModelException;
import it.stepit.server.model.actions.GameAction;
import it.stepit.server.model.map.GameMap;
import it.stepit.server.model.map.Town;
import it.stepit.server.model.market.Market;
import it.stepit.server.model.market.MarketOffer;
import it.stepit.server.utils.Utils;

import java.awt.*;
import java.util.*;
import java.util.List;

public class GameStatus extends it.stepit.server.controller.Observable<String>{
    private static final int PLAYER_TIMEOUT = 60000 * 3;
    private Timer playerTimeout;

    private final String configurationName;
    private final boolean marketEnabled;
    private final int maxPlayers;
    private final int emporiumsPerPlayer;
    private final int politicsCardsPerPlayer;

    private Player currentPlayer;
    private  ArrayList<Player> players;
    private int mainActions = 1;
    private int quickActions = 1;

    private int freeAssistants;
    private final HashSet<Councillor> freeCouncillors;
    private final ArrayDeque<PoliticCard> politicsCardDeck;
    private final HashSet<BonusCard> bonusCards;
    private final ArrayList<NobiltyPathStep> nobilityPath;
    private final GameMap gameMap;
    private final King kingStatus;
    private final ImmutableSet<PoliticCard> allPoliticsCards;
    private final ImmutableSet<BusinessCard> allBusinessCards;
    private HashSet<PoliticCard> discardedPoliticsCards;

    private ArrayDeque<Reward> pendingActiveRewards;
    private Market market;
    private boolean handlingPendingActiveRewards;
    private boolean finalPhase = false;
    private boolean endend = false;
    private boolean aborted = false;


    public GameStatus(String configurationName, boolean isMarketEnabled, int maxPlayers, int emporiumsPerPlayer, int politicsCardsPerPlayer, int freeAssistants, Collection<Councillor> freeCouncillors, Collection<PoliticCard> politicsCardDeck, Collection<BusinessCard> businessCardsDeck, Collection<BonusCard> bonusCards, List<NobiltyPathStep> nobilityPath, GameMap gameMap, King kingStatus) {
        this.configurationName = configurationName;
        this.marketEnabled = isMarketEnabled;
        this.maxPlayers = maxPlayers;
        this.emporiumsPerPlayer = emporiumsPerPlayer;
        this.politicsCardsPerPlayer = politicsCardsPerPlayer;
        this.freeAssistants = freeAssistants;
        this.freeCouncillors = new HashSet<>( freeCouncillors );
        this.politicsCardDeck = new ArrayDeque<>( Utils.shuffleDeque( new ArrayDeque<>( politicsCardDeck ) ) );
        this.allPoliticsCards = ImmutableSet.copyOf(politicsCardDeck);
        this.allBusinessCards = ImmutableSet.copyOf(businessCardsDeck);
        this.bonusCards = new HashSet<>( bonusCards );
        this.nobilityPath = new ArrayList<>( nobilityPath );
        this.gameMap = gameMap;
        this.kingStatus = kingStatus;
        this.players = new ArrayList<>();
        this.discardedPoliticsCards = new HashSet<>();
        this.pendingActiveRewards = new ArrayDeque<>();
    }

    /********************
     * EVENTS LISTENERS *
     ********************/
    public void onMainActionApplied(GameAction gameAction){
        this.mainActions--;
        if(this.currentPlayer != null)
            this.notifyObservers( this.currentPlayer.getUsername() + " " + gameAction.getDescription());
        this.moveOn(false);
    }

    public void onQuickActionApplied(GameAction gameAction){
        this.quickActions--;
        if(this.currentPlayer != null)
            this.notifyObservers( this.currentPlayer.getUsername() + " " + gameAction.getDescription());
        this.moveOn(false);
    }

    void onRewardApplied(Reward r){
        if(this.getCurrentPlayer() != null)
            this.notifyObservers(this.getCurrentPlayer().getUsername() +  " received a reward: " + r.toString());
    }

    public void onGameStarted(){
        Collections.shuffle(this.players, new Random( System.nanoTime() ));
        this.currentPlayer = this.players.get(0);
        this.currentPlayer.addPoliticCard( this.getPoliticCard() );
        this.startPlayerTimeoutTimer();

    }

    public void onGameAborted(){
        this.aborted = true;
        this.stopPlayerTimeoutTimer();
    }

    /**
     * Method fired when a player builts an emporium in a town.
     * Here we apply all game bonus and logic.
     *
     * @param player The player who built the emporium
     * @param town The town in which the player has built the emporium
     */
    public void onEmporiumBuilt(Player player, Town town){
        if( player.getEmporiumsToWin() == 0 ){
            player.setVictoryPoints( player.getVictoryPoints()+3 );
            this.finalPhase = true;
        }

        if( town.getRegion().hasBonusCard() && town.getRegion().checkPlayerCompletion(player) ){
            Optional<BonusCard> regionBonusCardOptional = town.getRegion().getBonusCard();
            if(regionBonusCardOptional.isPresent()){
                BonusCard regionBonusCard = regionBonusCardOptional.get();
                player.addBonusCard(regionBonusCard);

                if(regionBonusCard.hasReward()) {
                    regionBonusCard.getReward().apply(player, this);
                }

                this.notifyObservers(player + " built an emporium in all " + town.getRegion().getName() + " towns and received a bonus card");
                this.assignKingBonusCard(player);
            }
        }

        if( checkTownColorCompletion(player, town.getColor()) ){
            Optional<BonusCard> bonusCard = this.getBonusCard(town.getColor());
            if(bonusCard.isPresent()){
                player.addBonusCard(bonusCard.get());

                if(bonusCard.get().hasReward()) {
                    bonusCard.get().getReward().apply(player, this);
                }

                this.notifyObservers(player + " built an emporium in all " + ColorUtils.getColorNameFromColor(town.getColor()) + " towns and received a bonus card");
                this.assignKingBonusCard(player);
            }
        }
    }

    /***********************
     * GETTERS AND SETTERS *
     ***********************/

    /**
     * Get the next <b>connected</b> player to play.
     *
     * @return The next player in the game flow
     */
    private Player getNextConnectedPlayer(){
        /* First search for connected players in currentPos+1 - maxPos */
        for(int i = this.players.indexOf(this.currentPlayer)+1; i < this.players.size(); i++){
            Player nextPlayer = this.players.get(i);
            if(nextPlayer.isConnected())
                return nextPlayer;
        }

        /* Search from start to currentPos-1 */
        for(int i = 0; i < this.players.indexOf(this.currentPlayer); i++){
            Player nextPlayer = this.players.get(i);
            if(nextPlayer.isConnected())
                return nextPlayer;
        }

        throw new GameControllerException("Game has only one connected player");
    }

    private boolean isLastPlayerToMove(Player p){
        int positionInArray = this.players.indexOf(p);

        return positionInArray != -1 && positionInArray == (this.players.size()-1);
    }

    /**
     * Returns a bonusCard of given color if it exists.
     * Removes the bonusCard from the available ones.
     *
     * @param cardColor     The color for the desidered bonusCard
     * @return              An optional containing the bonusCard
     */
    private Optional<BonusCard> getBonusCard(Color cardColor){
        for(BonusCard bonusCard : this.bonusCards){
            if( bonusCard.getColor().equals(cardColor) ){
                Optional<BonusCard> returnValue = Optional.of(bonusCard);
                this.bonusCards.remove(bonusCard);
                return returnValue;
            }
        }

        return Optional.empty();
    }

    public boolean hasPlayer(Player p){
        return this.players.contains(p);
    }

    public ImmutableList<Player> getPlayers(){
        return ImmutableList.copyOf(this.players);
    }

    public int getMaxPlayers() {
        return this.maxPlayers;
    }

    public int getMainActions(){
        return this.mainActions;
    }

    public int getQuickActions() {
        return quickActions;
    }

    public void setMainActions(int mainActions){
        this.mainActions = mainActions;
    }

    public int getFreeAssistants(){
        return this.freeAssistants;
    }

    public void setFreeAssistants(int freeAssistants){
        this.freeAssistants = freeAssistants;
    }

    public GameMap getGameMap(){
        return this.gameMap;
    }

    public ImmutableCollection<Councillor> getFreeCouncillors(){
        return ImmutableSet.copyOf( this.freeCouncillors );
    }

    public void addFreeCouncilor(Councillor councillor) {
        if(councillor == null)
            throw new IllegalArgumentException("Illegal null value for councillor param");

        if( this.freeCouncillors.contains( councillor ) )
            throw new GameModelException("The councillor is already present into freeCouncilors");

        this.freeCouncillors.add( councillor );
    }

    /**
     * Returns a PoliticCard from politic cards deck.
     * If deck is empty, discarded politics cards are shuffled.
     * If both available ones deck and discarded ones deck is empty returns null.
     *
     * @return A PoliticCard if available, null otherwise
     */
    public PoliticCard getPoliticCard(){
        if(this.politicsCardDeck.isEmpty()){
            if(!this.discardedPoliticsCards.isEmpty()){
                List<PoliticCard> discardedPoliticCardsList = new ArrayList<>( this.discardedPoliticsCards );
                Collections.shuffle(discardedPoliticCardsList);
                this.politicsCardDeck.addAll( discardedPoliticCardsList );
            }else{
                return null;
            }
        }

        return this.politicsCardDeck.pop();
    }

    /**
     * Returns a collection containing <strong>all</strong> politic cards of the game: the ones owned by players, the ones still available and the discarded ones.
     *
     * @return A collection containing all politic cards of the game.
     */
    public ImmutableCollection<PoliticCard> getAllPoliticCards(){
        return this.allPoliticsCards;
    }

    /**
     * Returns a collection containing </strong>all</strong> business cards of the game: the ones owned by pplayers, the ones still available and the discarded ones.
     *
     * @return A collection containing all politic cards of the game.
     */
    public ImmutableCollection<BusinessCard> getAllBusinessCards(){
        return this.allBusinessCards;
    }

    /**
     * Adds a politic card into discarded ones deck
     *
     * @param politicsCards The politic card to discard
     */
    public void addDiscardedPoliticsCards(Collection<PoliticCard> politicsCards){
        this.discardedPoliticsCards.addAll( politicsCards );
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    void addPendingActiveReward(Reward reward){
        this.pendingActiveRewards.add( reward );
    }

    public boolean isInMarketMode(){
        return this.market != null;
    }

    public boolean isEndend() { return this.endend; }

    /**
     * Checks if given councillor is a free councillor
     *
     * @param councillor The councillor to check
     */
    public boolean checkFreeCouncillor(Councillor councillor){
        return this.freeCouncillors.contains( councillor );
    }

    /**
     * Removes a councillor from free ones
     *
     * @param councillor
     */
    public void removeFreeCouncillor(Councillor councillor){
        if(this.freeCouncillors.contains(councillor)) {
            this.freeCouncillors.remove(councillor);
        }
    }

    public Balcony getKingBalcony(){
        return this.kingStatus.getBalcony();
    }

    public King getKing(){
        return this.kingStatus;
    }

    /******************
     * MARKET METHODS *
     ******************/
    private void startMarketMode(){
        if(!this.marketEnabled)
            throw new IllegalStateException("Can't start market mode if isMarketEnabled == false");

        this.market = new Market(this);
        this.notifyObservers("Market mode started! In the next " + this.market.getSellPhaseDuration() + " seconds you can sell game objects");
    }

    public RichBoolean publishMarketOffer(MarketOffer marketOffer){
        if(this.market == null)
            return new RichBoolean(false, "It's not market mode selling time!");
        else
            return this.market.publishOffer(marketOffer);
    }

    public void onMarketUpdate(String message){
        this.notifyObservers(message);
    }

    public void onMarketModeComplete(){
        if(!this.aborted)
            this.moveOn(true);
    }

    public Set<MarketOffer> getMarketOffers(){
        if(this.market != null)
            return new HashSet<>( this.market.getOffers() );
        else
            return Collections.emptySet();
    }

    public RichBoolean buyMarketOffer(Player buyer, UUID offerUUID){
        if(this.market != null)
            return this.market.buyOfferRequest(buyer, offerUUID);
        else
            return new RichBoolean(false, "It's not market mode time!");
    }

    public RichBoolean passMarketBuyPhase(Player player){
        if(this.market != null)
            return this.market.pass(player);
        else
            return new RichBoolean(false, "It's not market mode phase");
    }

    public boolean transferObject(Player fromPlayer, Player toPlayer, BusinessCard businessCard){
        if( fromPlayer.getBusinessCards().contains(businessCard) ){
            fromPlayer.removeBusinessCard(businessCard);
            toPlayer.addBusinessCard(businessCard);
            return true;
        }

        return false;
    }

    public boolean transferObject(Player fromPlayer, Player toPlayer, PoliticCard politicCard){
        if(fromPlayer.getPoliticCards().contains(politicCard)){
            fromPlayer.removePoliticCard(politicCard);
            toPlayer.addPoliticCard(politicCard);
            return true;
        }

        return false;
    }

    public boolean transferObject(Player fromPlayer, Player toPlayer, int assistants){
        if(fromPlayer.getAssistants() >= assistants){
            fromPlayer.setAssistants( fromPlayer.getAssistants() - assistants );
            toPlayer.setAssistants( fromPlayer.getAssistants() + assistants );
            return true;
        }
        return false;
    }

    /*****************
     * OTHER METHODS *
     *****************/

    public void assignFreeAssistants(Player player, int amount){
        player.setAssistants( player.getAssistants() + amount );
        this.freeAssistants -= amount;

        if(this.getFreeAssistants() < 0){
            player.setAssistants( player.getAssistants() - ( 0 - this.getFreeAssistants() ) );
            this.freeAssistants = 0;
        }
    }

    /**
     * Moves the game from one phase to the next one.
     * Switches currentPlayer and starts marketMode if enabled.
     *
     * @param forced A boolean indicating if the method was called because user timed out, market is ended or by game flow.
     */
    private void moveOn(boolean forced){
        this.stopPlayerTimeoutTimer();

        /* If there is any pending reward that requires user inputs handle it before next player */
        if( !this.pendingActiveRewards.isEmpty() ){
            if(forced && this.handlingPendingActiveRewards){
                this.pendingActiveRewards.clear();
                this.handlingPendingActiveRewards = false;
            }else {
                this.handleActiveRewards();
                return;
            }
        }

        if( (this.quickActions == 0 && this.mainActions == 0) || forced ){
            String updateMessage = "";
            if (this.isLastPlayerToMove(this.currentPlayer) ) {
                if (this.finalPhase) {
                    this.gameEnd();
                    return;
                } else if( this.marketEnabled && this.market == null) {
                    this.startMarketMode();
                    return;
                }
            }

            if( forced && this.market != null ){
                this.market = null;
                updateMessage = updateMessage.concat("Market mode ended. ");
            }else if( forced ){
                updateMessage = updateMessage.concat( this.currentPlayer.getUsername() + " timed-out. " );
            }

            this.currentPlayer = this.getNextConnectedPlayer();
            this.currentPlayer.addPoliticCard( this.getPoliticCard() );
            this.quickActions = 1;
            this.mainActions = 1;

            updateMessage = updateMessage.concat(this.currentPlayer.getUsername() + " moves!");

            this.notifyObservers(updateMessage);
        }

        this.startPlayerTimeoutTimer();
    }

    private void handleActiveRewards(){
        this.handlingPendingActiveRewards = true;
        int businessCardsToTake = 0;
        int rewardTokensToUse = 0;
        int businessCardsToUse = 0;

        for(Reward r : this.pendingActiveRewards){
            businessCardsToTake += r.getTakeBusinessCards();
            businessCardsToUse += r.getUseBusinessCards();
            rewardTokensToUse += r.getUseBusinessCards();
        }

        this.pendingActiveRewards.clear();
        RewardParamsRequest rewardParamsRequest = new RewardParamsRequest(businessCardsToTake, rewardTokensToUse, businessCardsToUse);
        if(this.currentPlayer != null) {
            this.currentPlayer.requestRewardParams(rewardParamsRequest);
            this.startPlayerTimeoutTimer();
        }
    }

    /**
     * Event fired when an users built its last emporiums and round endend.
     * Calculates final scores by editing players victory points.
     *
     * <i>This method is different from game standard specifics to best fit situations with an huge amount of players
     * Here's how points are calculated:
     * 1 - victory points
     * 2 - victory points on each player's bonus card
     * 3 - Players are sorted by their position in nobility path: each player takes (num_players - position) points
     * 4 - Players are sorted by the amount of their business card: each player takes (num_players - position) points
     * 5 - Players are sorted by (assistants+coins): each player takes (num</i>
     */
    private void gameEnd(){
        this.endend = true;
        this.stopPlayerTimeoutTimer();

        for(Player p : this.players){
            for(BonusCard pbc : p.getBonusCard()){
                if(pbc.hasReward())
                    p.setVictoryPoints( p.getVictoryPoints() + pbc.getReward().getVictoryPoints() );
            }
        }

        Comparator<Player> compareByNobilityPathPosition = (p1, p2) -> {
            if(p1.getNobilityPathPosition() > p2.getNobilityPathPosition())
                return 1;

            return -1;
        };

        Collections.sort(players,compareByNobilityPathPosition);
        this.givePointsByPosition();

        Comparator<Player> compareByBusinessCardAmount = (p1, p2) -> {
            if(p1.getBusinessCards().size() > p2.getBusinessCards().size())
                return 1;

            return -1;
        };

        Collections.sort(players,compareByBusinessCardAmount);
        this.givePointsByPosition();

        Comparator<Player> compareByAssistantsAndCoinsSum = (p1, p2) -> {
            if(p1.getAssistants()+p1.getCoins() > p2.getAssistants()+p2.getCoins())
                return 1;

            return -1;
        };

        Collections.sort(players,compareByAssistantsAndCoinsSum);
        this.givePointsByPosition();

        Comparator<Player> compareByVictoryPoints = (p1, p2) -> {

            if(p1.getVictoryPoints()>p2.getVictoryPoints())
                return 1;

            return -1;
        };

        Collections.sort(players,compareByVictoryPoints);
        this.notifyObservers("Game endend folks!");
    }

    private void givePointsByPosition(){
        for(int i = players.size(); i < players.size(); i++) {
            int currentPoints = players.get(players.size() - i).getVictoryPoints();
            players.get(players.size() - i).setVictoryPoints(currentPoints+i);
        }
    }

    private void assignKingBonusCard(Player player){
        Optional<BonusCard> kingBonusCard = this.kingStatus.getBonusCard();
        if(kingBonusCard.isPresent()){
            player.addBonusCard(kingBonusCard.get());

            if(kingBonusCard.get().hasReward())
                kingBonusCard.get().getReward().apply(player, this);

            this.notifyObservers(player.getUsername() + " received a King bonus card");
        }
    }

    /**
     * Checks if a player has built an emporium in each town of a given color
     * @param player        The player to check
     * @param townColor     The color to check
     * @return              True if the player has built an emporium in each town with the given color, false otherwise
     */
    private boolean checkTownColorCompletion(Player player, Color townColor){
        for(Town town : this.gameMap.getTowns().values()){
            if( town.getColor().equals(townColor) && !town.checkPlayerEmporium(player)){
                return false;
            }
        }

        return true;
    }

    /**
     * Adds a single player to the gameStatus and assigns initial objects
     *
     * @param newPlayer     The player object to add
     */
    public void addPlayer(Player newPlayer){
        newPlayer.setCoins( 10 + this.players.size() );
        newPlayer.setAssistants( 1 + this.players.size() );
        newPlayer.setEmporiumsToWin( this.emporiumsPerPlayer );

        for(int i = 0; i < this.politicsCardsPerPlayer; i++){
            newPlayer.addPoliticCard( this.politicsCardDeck.pop() );
        }

        players.add( newPlayer );
    }

    /**
     * Adds a collection of players to the gameStatus. Assigns initial objects to each player
     *
     * @param newPlayers A collection of players to add
     * @throws GameModelException
     */
    public void addPlayers(Collection<Player> newPlayers) {
        if( (this.players.size() + newPlayers.size() ) > this.maxPlayers)
            throw new GameModelException("This game configuration can contain max " + this.maxPlayers + " players");

        this.players.addAll(newPlayers);

        for(Player p : newPlayers){
            int position = this.players.indexOf(p);
            p.setCoins( 10 + position);
            p.setAssistants( 10 + position );
            p.setEmporiumsToWin( this.emporiumsPerPlayer );

            for(int i = 0; i < this.politicsCardsPerPlayer; i++){
                p.addPoliticCard( this.politicsCardDeck.pop() );
            }
        }
    }

    /**
     * Moves a player on the nobility path.
     *
     * @param player The player to move
     * @param steps The amount of steps
     */
    void movePlayerOnNobilityPath(Player player, int steps){
        if(steps == 0)
            return;

        int newPosition = player.getNobilityPathPosition() + steps;
        if(newPosition < this.nobilityPath.size()){
            player.setNobilityPathPosition(newPosition);
            NobiltyPathStep newStep = this.nobilityPath.get(newPosition);
            if(newStep.hasReward()){
                newStep.getReward().apply(player, this);
            }
        }
    }

    private void startPlayerTimeoutTimer(){
        if(playerTimeout != null)
            throw new IllegalStateException("PlayerTimeoutTimer is already running. Please, use stopPlayerTimeoutTimer() first.");

        this.playerTimeout = new Timer();
        this.playerTimeout.schedule(new java.util.TimerTask() {
            @Override
            public void run() {
                moveOn(true);
            }}, PLAYER_TIMEOUT );
    }

    private void stopPlayerTimeoutTimer(){
        if(this.playerTimeout != null) {
            this.playerTimeout.cancel();
            this.playerTimeout.purge();
            this.playerTimeout = null;
        }
    }

    public MetaGameStatus buildMetaGameStatus(){
        HashSet<MetaBonusCard> metaBonusCards = new HashSet<>();
        HashSet<MetaCouncillor> metaCouncillors = new HashSet<>();
        List<MetaNobilityPathStep> metaNobilityPath = new ArrayList<>();
        SortedMap<String, Integer> finalBoard = null;

        for(BonusCard b : this.bonusCards){
            metaBonusCards.add( b.buildMetaBonusCard() );
        }

        for(NobiltyPathStep npst : this.nobilityPath){
            metaNobilityPath.add(npst.buildMetaNobilityPathStep());
        }

        for(Councillor c : this.freeCouncillors){
            metaCouncillors.add(c.buildMetaCouncillor());
        }

        if( this.endend ){
            finalBoard = new TreeMap<>();
            for(Player p : this.players){
                finalBoard.put(p.getUsername(), p.getVictoryPoints());
            }
        }

        MetaPlayer currentMetaPlayer = null;
        if(this.currentPlayer != null)
            currentMetaPlayer = this.currentPlayer.buildMetaPlayer();

        return new MetaGameStatus( this.configurationName, currentMetaPlayer, this.freeAssistants,
                metaBonusCards, this.kingStatus.buildMetaKing(), this.gameMap.buildMetaGameMap(),
                metaNobilityPath, metaCouncillors, finalBoard);
    }
}