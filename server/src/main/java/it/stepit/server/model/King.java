package it.stepit.server.model;

import it.stepit.cscommons.communication.metaobjects.MetaBalcony;
import it.stepit.cscommons.communication.metaobjects.MetaBonusCard;
import it.stepit.cscommons.communication.metaobjects.MetaKing;
import it.stepit.cscommons.communication.metaobjects.map.MetaTown;
import it.stepit.server.model.map.Town;

import java.util.*;

public class King {
    private final Balcony balcony;
    private final ArrayDeque<BonusCard> bonusCards;
    private Town currentTown;
    private final int costPerRoad;

    public King(Deque<BonusCard> bonusCards, Town currentTown, int costPerRoad) {
        if(currentTown == null)
            throw new IllegalArgumentException("Illegal null value for currentTown param");

        if(costPerRoad < 0)
            throw new IllegalArgumentException("Illegal negative value for costPerRoad param");


        this.bonusCards = new ArrayDeque<>( bonusCards );
        this.balcony = new Balcony();
        this.currentTown = currentTown;
        this.costPerRoad = costPerRoad;
    }

    /***********************
     * GETTERS AND SETTERS *
     ***********************/

    public Balcony getBalcony() {
        return this.balcony;
    }

    public Town getCurrentTown() {
        return this.currentTown;
    }

    public void setCurrentTown(Town newTown){
        if(newTown == null)
            throw new IllegalArgumentException("Invalid null value for newTown param");

        this.currentTown = newTown;
    }

    public int getCostPerRoad(){
        return this.costPerRoad;
    }

    /**
     * If present, removes and returns the king bonus card on the top of the queue.
     *
     * @return An optional object containing the bonus card on top of bonus cards queue if present
     */
    Optional<BonusCard> getBonusCard(){
        if(!bonusCards.isEmpty())
            return Optional.of( this.bonusCards.pop() );

        return Optional.empty();
    }

    MetaKing buildMetaKing(){
        MetaBalcony metaBalcony = this.balcony.buildMetaBalcony();
        MetaTown currentMetaTown = this.currentTown.buildMetaTown();
        ArrayList<MetaBonusCard> metaBonusCards = new ArrayList<>();
        for(BonusCard kingBonusCard : this.bonusCards){
            metaBonusCards.add(kingBonusCard.buildMetaBonusCard());
        }

        return new MetaKing(metaBalcony, currentMetaTown, metaBonusCards );
    }
}
