package it.stepit.server.model;
import it.stepit.cscommons.communication.metaobjects.MetaNobilityPathStep;
import it.stepit.cscommons.communication.metaobjects.MetaReward;
import javax.annotation.Nullable;

public class NobiltyPathStep {
    private Reward reward;

    public NobiltyPathStep(@Nullable Reward reward){
        this.reward = reward;
    }

    /***********************
     * GETTERS AND SETTERS *
     ***********************/

    public Reward getReward() {
        return this.reward;
    }

    public void setReward(@Nullable Reward reward){
        this.reward = reward;
    }

    /********************
     * OBSERVER METHODS *
     ********************/

    public boolean hasReward(){
        return this.reward != null;
    }

    MetaNobilityPathStep buildMetaNobilityPathStep(){
        MetaReward metaReward = this.reward != null ? this.reward.buildMetaReward() : null;
        return new MetaNobilityPathStep( metaReward );
    }
}
