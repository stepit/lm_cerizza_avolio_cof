package it.stepit.server.model;

import com.google.common.collect.ImmutableCollection;
import com.google.common.collect.ImmutableSet;
import it.stepit.cscommons.communication.IDableObject;
import it.stepit.cscommons.communication.metaobjects.MetaBonusCard;
import it.stepit.cscommons.communication.metaobjects.MetaBusinessCard;
import it.stepit.cscommons.communication.metaobjects.MetaPlayer;
import it.stepit.cscommons.communication.metaobjects.MetaPoliticCard;
import it.stepit.cscommons.exceptions.ClientServerCommunicationException;
import it.stepit.cscommons.game.RewardParamsRequest;
import it.stepit.server.communication.OutboundCommunicationInterface;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

public class Player extends IDableObject{
    private static final Logger LOGGER = LoggerFactory.getLogger(Player.class);
    private final String username;
    private transient OutboundCommunicationInterface outboundInterface;

    private int coins;
    private int assistants;
    private int nobilityPathPosition;
    private int victoryPoints;
    private int emporiumsToWin;
    private final Collection<BusinessCard> businessCards;
    private final Collection<PoliticCard> politicsCards;
    private final Collection<BonusCard> bonusCards;

    public Player(String username, OutboundCommunicationInterface outboundInterface){
        this.username = username;
        this.outboundInterface = outboundInterface;

        this.coins = 0;
        this.nobilityPathPosition = 0;
        this.victoryPoints = 0;
        this.businessCards = new HashSet<>();
        this.politicsCards = new HashSet<>();
        this.bonusCards = new HashSet<>();
    }

    /***********************
     * GETTERS AND SETTERS *
     ***********************/

    public String getUsername(){
        return this.username;
    }

    public OutboundCommunicationInterface getOutboundInterface() {
        return this.outboundInterface;
    }

    public void setOutboundInterface(@Nullable OutboundCommunicationInterface outboundInterface){
        this.outboundInterface = outboundInterface;
    }

    public int getCoins() {
        return this.coins;
    }

    public void setCoins(int amount){
        if(amount < 0)
            throw new IllegalArgumentException("Can't set a negative amount of coins");

        this.coins = amount;
    }

    public int getAssistants() {
        return this.assistants;
    }

    public void setAssistants(int amount) {
        if(amount < 0)
            throw new IllegalArgumentException("Can't set a negative amount of assistants");

        this.assistants = amount;
    }

    public int getNobilityPathPosition() {
        return this.nobilityPathPosition;
    }

    public void setNobilityPathPosition(int nobilityPathPosition) {
        if(nobilityPathPosition < 0)
            throw new IllegalArgumentException("Can't set a negative path position");

        this.nobilityPathPosition = nobilityPathPosition;
    }

    public int getVictoryPoints() {
        return this.victoryPoints;
    }

    public void setVictoryPoints(int amount) {
        if(amount < 0)
            throw new IllegalArgumentException("Can't set a negative amount of victory points");

        this.victoryPoints = amount;
    }

    public int getEmporiumsToWin() {
        return this.emporiumsToWin;
    }

    public void setEmporiumsToWin(int amporiumsToWin) {
        if(amporiumsToWin < 0)
            throw new IllegalArgumentException("Can't set a negative amount of coins");

        this.emporiumsToWin = amporiumsToWin;
    }

    public boolean hasPoliticsCards(Collection<PoliticCard> politicsCards){
        return this.politicsCards.containsAll( politicsCards );
    }

    public void addBusinessCard(BusinessCard businessCard){
        if(businessCard == null)
            throw new IllegalArgumentException("Invalid null value for businessCard param");

        this.businessCards.add( businessCard );
    }

    public void removeBusinessCard(BusinessCard businessCard){
        this.businessCards.remove(businessCard);
    }

    public void addPoliticCard(PoliticCard politicCard){
        if(politicCard == null)
            throw new IllegalArgumentException("Invalid null value for politicCard param");

        this.politicsCards.add(politicCard);
    }

    public void addBonusCard(BonusCard bonusCard){
        if(bonusCard == null)
            throw new IllegalArgumentException("Invalid null value for bonusCard param");

        this.bonusCards.add(bonusCard);
    }

    public void removePoliticCards(Collection<PoliticCard> removePoliticsCard){
        this.politicsCards.removeAll( removePoliticsCard );
    }

    public void removePoliticCard(PoliticCard removePoliticsCard){
        this.politicsCards.remove( removePoliticsCard );
    }

    public boolean isConnected(){
        return this.outboundInterface != null;
    }

    public ImmutableCollection<BonusCard> getBonusCard(){
        return ImmutableSet.copyOf( this.bonusCards );
    }

    public ImmutableCollection<BusinessCard> getBusinessCards() {
        return ImmutableSet.copyOf( this.businessCards );
    }

    public ImmutableCollection<PoliticCard> getPoliticCards() {
        return ImmutableSet.copyOf( this.politicsCards );
    }

    /*****************
     * OTHER METHODS *
     *****************/

    public void requestRewardParams(RewardParamsRequest request){
        try {
            this.outboundInterface.requestRewardParams(request);
        } catch (ClientServerCommunicationException e) {
            LOGGER.error("Unable to deliver RewardParamsRequest", e);
        }
    }

    public MetaPlayer buildMetaPlayer(){
        ArrayList<MetaBonusCard> metaBonusCards = new ArrayList<>();
        ArrayList<MetaPoliticCard> metaPoliticsCards = new ArrayList<>();
        ArrayList<MetaBusinessCard> metaBusinessCards = new ArrayList<>();

        for(BonusCard bc : this.bonusCards){
            metaBonusCards.add( bc.buildMetaBonusCard() );
        }

        for(PoliticCard pc : this.politicsCards){
            metaPoliticsCards.add( pc.buildMetaPoliticsCard() );
        }

        for(BusinessCard buc : this.businessCards){
            metaBusinessCards.add( buc.buildMetaBusinessCard() );
        }

        return new MetaPlayer(this.username, metaBonusCards, metaBusinessCards,
                            metaPoliticsCards, this.emporiumsToWin, this.victoryPoints,
                            this.nobilityPathPosition, this.assistants, this.coins, this.getId());
    }
}
