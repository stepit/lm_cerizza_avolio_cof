package it.stepit.server.model;

import it.stepit.cscommons.communication.IDableObject;
import it.stepit.cscommons.communication.metaobjects.MetaPoliticCard;

import java.awt.*;
import java.util.Collection;

public class PoliticCard extends IDableObject {
    private final boolean isJolly;
    private final Color color;

    public PoliticCard(boolean isJolly, Color color){
        this.isJolly = isJolly;
        this.color = color;
    }

    boolean isJolly() {
        return isJolly;
    }

    public Color getColor() {
        return color;
    }

    public MetaPoliticCard buildMetaPoliticsCard(){
        return new MetaPoliticCard(this.isJolly, this.color, this.getId());
    }

    /******************
     * STATIC METHODS *
     ******************/

    /**
     * Given a collection of PoliticCards counts how many jolly are in it.
     *
     * @param politicCards The collection of PoliticCards to search into
     * @return The number of jolly in the collection
     */
    public static int countJolly(Collection<PoliticCard> politicCards){
        int count = 0;
        for(PoliticCard p : politicCards){
            if(p.isJolly())
                count++;
        }

        return count;
    }
}
