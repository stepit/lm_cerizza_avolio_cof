package it.stepit.server.model;

import it.stepit.cscommons.communication.metaobjects.MetaReward;
import java.io.Serializable;

public class Reward implements Serializable{
    private final int assistants;
    private final int coins;
    private final int nobiltyPathSteps;
    private final int victoryPoints;
    private final int takeBusinessCards;
    private final int doMoreMainActions;
    private final int useRewardTokens;
    private final int useBusinessCards;
    private final int takePoliticsCards;
    private String graphicalResourceId;

    public Reward(int assistants, int coins, int nobiltyPathSteps, int victoryPoints, int takeBusinessCards, int doMoreMainActions, int useRewardTokens, int useBusinessCards, int takePoliticsCards) {
        this.assistants = assistants;
        this.coins = coins;
        this.nobiltyPathSteps = nobiltyPathSteps;
        this.victoryPoints = victoryPoints;
        this.takeBusinessCards = takeBusinessCards;
        this.doMoreMainActions = doMoreMainActions;
        this.useRewardTokens = useRewardTokens;
        this.useBusinessCards = useBusinessCards;
        this.takePoliticsCards = takePoliticsCards;
    }

    public Reward() {
        this.assistants = 0;
        this.coins = 0;
        this.nobiltyPathSteps = 0;
        this.victoryPoints = 0;
        this.takeBusinessCards = 0;
        this.doMoreMainActions = 0;
        this.useRewardTokens = 0;
        this.useBusinessCards = 0;
        this.takePoliticsCards = 0;
    }

    /***********************
     * GETTERS AND SETTERS *
     ***********************/

    public int getVictoryPoints(){
        return this.victoryPoints;
    }

    public int getUseRewardTokens() {
        return useRewardTokens;
    }

    public int getUseBusinessCards() {
        return useBusinessCards;
    }

    public int getTakeBusinessCards() {
        return takeBusinessCards;
    }

    public String getGraphicalResourceId() {
        return graphicalResourceId;
    }

    public void setGraphicalResourceId(String graphicalResourceId) {
        this.graphicalResourceId = graphicalResourceId;
    }

    /**
     * Apply perks provided by the reward to a given player.
     * If the reward requires an user action (e.g. to select a politic card) it's added
     * into GameStatus and will be handled later.
     *
     * @param player The player to apply the reward to
     * @param gameStatus The gameStatus to apply the reward to
     */
    public void apply(Player player, GameStatus gameStatus){
        if( player == null || gameStatus == null )
            throw new IllegalArgumentException("Player and GameStatus can't be null");

        if(!gameStatus.hasPlayer(player))
            throw new IllegalArgumentException("Player not part of given GameStatus");

        if( this.takeBusinessCards > 0 || this.useRewardTokens > 0 || this.useBusinessCards > 0){
            gameStatus.addPendingActiveReward( this );
        }

        player.setCoins( player.getCoins() + this.coins );

        gameStatus.assignFreeAssistants(player, this.assistants);

        gameStatus.movePlayerOnNobilityPath(player, this.nobiltyPathSteps);

        player.setVictoryPoints( player.getVictoryPoints() + this.victoryPoints );

        gameStatus.setMainActions( gameStatus.getMainActions() + this.doMoreMainActions );

        for(int i = 0; i < takePoliticsCards; i++){
            player.addPoliticCard( gameStatus.getPoliticCard() );
        }

        gameStatus.onRewardApplied(this);
    }

    @Override
    public String toString() {
        String result = "";
        if(this.assistants != 0){
            result = result.concat("Assistants:" + this.assistants + " ");
        }

        if(this.coins != 0){
            result = result.concat("Coins:" + this.coins + " ");
        }

        if(this.victoryPoints != 0){
            result = result.concat("VictoryPoints:" + this.victoryPoints + " ");
        }

        if(this.nobiltyPathSteps != 0){
            result = result.concat("NobilityPathSteps:" + this.nobiltyPathSteps + " ");
        }

        if(this.doMoreMainActions != 0){
            result = result.concat("DoMoreMainActions:" + this.doMoreMainActions + " ");
        }

        if(this.takeBusinessCards != 0){
            result = result.concat("TakeBusinessCards:" + this.takeBusinessCards + " ");
        }

        if(this.useRewardTokens != 0){
            result = result.concat("UseRewardToken:" + this.useRewardTokens +  " ");
        }

        if(this.useBusinessCards != 0){
            result = result.concat("UseBusinessCards:" + this.useBusinessCards +  " ");
        }

        if(this.takePoliticsCards != 0){
            result = result.concat("TakePoliticCards:" + this.takePoliticsCards + " ");
        }

        return result;
    }

    /**
     * Builds a MetaReward
     *
     * @return An equivalent MetaReward
     */
    public MetaReward buildMetaReward(){
        return new MetaReward(this.assistants, this.coins, this.nobiltyPathSteps,
                                this.victoryPoints, this.takeBusinessCards, this.doMoreMainActions,
                                this.useRewardTokens, this.useBusinessCards, this.takePoliticsCards, this.graphicalResourceId);
    }
}
