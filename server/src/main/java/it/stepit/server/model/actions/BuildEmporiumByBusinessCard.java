package it.stepit.server.model.actions;

import it.stepit.cscommons.utils.RichBoolean;
import it.stepit.server.model.BusinessCard;
import it.stepit.server.model.GameStatus;
import it.stepit.server.model.Player;
import it.stepit.server.model.map.Town;

import java.util.List;

/**
 *  GameAction implementation for BuiltEmporiumByBysinessCard action.
 *  Receives as input the town to built into and the selected business card.
 *  Checks if the selected business card allows to built into the town,
 *  if the player has not alredy built in the town, if the card is not used and if the player has
 *  enough assistants to build in the town (one assistant for each emporium alredy built).
 */
public class BuildEmporiumByBusinessCard extends MainAction {
    private Town selectedTown;
    private BusinessCard selectedBusinessCard;

    public BuildEmporiumByBusinessCard(List<Object> params){
        this( (Town) params.get(0), (BusinessCard) params.get(1));
    }

    public BuildEmporiumByBusinessCard(Town selectedTown, BusinessCard selectedBusinessCard){
        this.selectedBusinessCard= selectedBusinessCard;
        this.selectedTown= selectedTown;

    }

    @Override
    public RichBoolean check(Player player, GameStatus gameStatus) {
        if( !super.check(player, gameStatus).getBooleanValue() )
            return super.check(player, gameStatus);

        if( !selectedBusinessCard.isApplicableToTown( selectedTown ) )
            return new RichBoolean(false, "You can't use those business card to build in this town");

        if( selectedTown.checkPlayerEmporium(player) )
            return new RichBoolean(false, "You already have an emporium built in this town");

        if( selectedBusinessCard.isUsed() )
            return new RichBoolean(false);

        return new RichBoolean(true);
    }

    @Override
    protected void performAction(Player player, GameStatus gameStatus) {
        player.setEmporiumsToWin( player.getEmporiumsToWin() - 1 );
        player.setAssistants( player.getAssistants() - this.selectedTown.countBuiltEmporiums() );

        this.selectedTown.buildEmporium(player);
        this.selectedBusinessCard.setUsed(true);

        if(this.selectedTown.hasReward())
            this.selectedTown.getRewardToken().apply(player, gameStatus);

        if(this.selectedBusinessCard.hasReward())
            this.selectedBusinessCard.getReward().apply(player, gameStatus);

        gameStatus.getGameMap().applyRewardOfConnectedTowns(this.selectedTown, player, gameStatus);
    }

    @Override
    public void onActionApplied(Player player, GameStatus gameStatus){
        gameStatus.onEmporiumBuilt(player, this.selectedTown);
        gameStatus.onMainActionApplied(this);
    }

    @Override
    public String getDescription() {
        return "built an emporium in " + this.selectedTown.getName() + " town using a business card";
    }
}
