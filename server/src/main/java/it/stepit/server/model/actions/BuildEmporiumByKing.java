package it.stepit.server.model.actions;

/**
 *  GameAction implementation for BuiltEmporiumByKing action.
 *  Receives as input the king status, and the politicCards selected for corrupting the king's balcony.
 *  Checks if the selected politicsCard can corrupt the balcony.
 *  once the balcony is corrupted, the player build his emporium paying one coin for each stranger emporiums
 *  built in the king's town.
 *  The king can be mooved in a new town paying 2 assistants for each town on the path to the new selected town
 */

import it.stepit.cscommons.utils.RichBoolean;
import it.stepit.server.model.GameStatus;
import it.stepit.server.model.King;
import it.stepit.server.model.Player;
import it.stepit.server.model.PoliticCard;
import it.stepit.server.model.map.Town;

import java.util.*;

public class BuildEmporiumByKing extends MainAction{
    private Set<PoliticCard> selectedCards;
    private Town selectedTown;
    private King kingStatus;
    private Optional<Integer> kingMovementLength;
    private int corruptionCost;
    private int totalMoveCost;

    public BuildEmporiumByKing(List<Object> params){
        this( (King) params.get(0), (Town) params.get(1), (Collection<PoliticCard>) params.get(2));
    }

    public BuildEmporiumByKing(King kingStatus, Town selectedTown, Collection<PoliticCard> selectedCards){
        this.selectedTown = selectedTown;
        this.kingStatus = kingStatus;
        this.selectedCards = new HashSet<>(selectedCards);

        if(this.selectedCards.size() == 3)
            corruptionCost = 4;
        else if(this.selectedCards.size() == 2)
            corruptionCost = 7;
        else if(this.selectedCards.size() == 1)
            corruptionCost = 10;

        this.corruptionCost += PoliticCard.countJolly(this.selectedCards);
    }

    @Override
    public RichBoolean check(Player player, GameStatus gameStatus){
        this.kingMovementLength = gameStatus.getGameMap().getPathLength(kingStatus.getCurrentTown(), selectedTown );

        if( !super.check(player,gameStatus).getBooleanValue() )
            return super.check(player, gameStatus);

        if( !this.kingStatus.getBalcony().isCorruptible( this.selectedCards ) )
            return new RichBoolean(false, "You can't corrupt king balcony with these politics cards");

        if( this.selectedTown.checkPlayerEmporium(player) )
            return new RichBoolean(false, "You already have an emporium built in this town");

        if( player.getAssistants() < this.selectedTown.countBuiltEmporiums() )
            return new RichBoolean(false, "You need " + this.selectedTown.countBuiltEmporiums() + " assistants to build an emporium in this town");

        if( !this.kingMovementLength.isPresent())
            return new RichBoolean(false, "You can't move the king to that town");

        int kingMovementCost = this.kingMovementLength.get() * this.kingStatus.getCostPerRoad();
        this.totalMoveCost =  kingMovementCost + this.corruptionCost;
        if( player.getCoins() < this.corruptionCost+kingMovementCost )
            return new RichBoolean(false, "You need " + this.totalMoveCost + " to perform this action: " + kingMovementCost +  " to move the king, " + this.corruptionCost + " to corrupt the balcony" );

        return new RichBoolean(true);
    }

    @Override
    public void performAction(Player player, GameStatus gameStatus) {
        this.kingStatus.setCurrentTown( this.selectedTown );

        this.selectedTown.buildEmporium( player );
        if(this.selectedTown.hasReward()) {
            this.selectedTown.getRewardToken().apply(player, gameStatus);
        }

        player.setEmporiumsToWin( player.getEmporiumsToWin() - 1 );
        player.setAssistants( player.getAssistants() - this.selectedTown.countBuiltEmporiums() );
        player.setCoins( player.getCoins() - this.kingMovementLength.get() * this.kingStatus.getCostPerRoad() );
        player.removePoliticCards( selectedCards );
        gameStatus.addDiscardedPoliticsCards( selectedCards );

        gameStatus.getGameMap().applyRewardOfConnectedTowns(this.selectedTown, player, gameStatus);
    }

    @Override
    public void onActionApplied(Player player, GameStatus gameStatus){
        gameStatus.onEmporiumBuilt(player, this.selectedTown);
        gameStatus.onMainActionApplied(this);
    }

    @Override
    public String getDescription() {
        return "moved the king to " + this.selectedTown.getName() + " town, built an emporium in it and paid " + this.totalMoveCost + " coins";
    }
}
