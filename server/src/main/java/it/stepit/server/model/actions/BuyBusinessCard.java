package it.stepit.server.model.actions;

import it.stepit.cscommons.utils.RichBoolean;
import it.stepit.server.model.BusinessCard;
import it.stepit.server.model.GameStatus;
import it.stepit.server.model.Player;
import it.stepit.server.model.PoliticCard;
import it.stepit.server.model.map.Region;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *  GameAction implementation for BuyBusinessCard action.
 *  Receives as input the region that has the balcony to corrupt, the businessCard to buy
 *  and a set of politics cards that the player wishes to use to corrupt the balcony.
 */
public class BuyBusinessCard extends MainAction {
    private Region region;
    private Set<PoliticCard> selectedPoliticsCards;
    private BusinessCard selectedBusinessCard;
    private int corruptionCost;

    public BuyBusinessCard(List<Object> params){
        this((Region) params.get(0), (BusinessCard) params.get(1), (Collection<PoliticCard>) params.get(2));
    }

    public BuyBusinessCard(Region region, BusinessCard selectedBusinessCard, Collection<PoliticCard> politicsCards){
        this.region = region;
        this.selectedBusinessCard = selectedBusinessCard;
        this.selectedPoliticsCards = new HashSet<>(politicsCards);

        if(selectedPoliticsCards.size() == 3)
            corruptionCost = 4;
        else if(selectedPoliticsCards.size() == 2)
            corruptionCost = 7;
        else if(selectedPoliticsCards.size() == 1)
            corruptionCost = 10;

        corruptionCost += PoliticCard.countJolly(this.selectedPoliticsCards);
    }

    @Override
    public RichBoolean check(Player player, GameStatus gameStatus) {
        if( !super.check(player, gameStatus).getBooleanValue() )
            return super.check(player, gameStatus);

        if( this.region.hasBusinessCards() )
            return new RichBoolean(false, "There are no more business cards in " + this.region.getName() +  " region");

        if( !player.hasPoliticsCards(selectedPoliticsCards) )
            return new RichBoolean(false, "You don't have selected politic cards");

        if( !region.getBalcony().isCorruptible(selectedPoliticsCards) )
            return new RichBoolean(false, "You can't corrupt region balcony with these politics cards");

        if( player.getCoins() < this.corruptionCost )
            return new RichBoolean(false, "You need " + this.corruptionCost + " coins to corrupt this balcony with these politics cards");

        if( !this.region.isBusinessCardBuyable( this.selectedBusinessCard ) )
            return new RichBoolean(false);

        return new RichBoolean(true);
    }

    @Override
    protected void performAction(Player player, GameStatus gameStatus) {
        player.addBusinessCard( this.selectedBusinessCard );
        player.setCoins(player.getCoins() - this.corruptionCost);
        player.removePoliticCards( this.selectedPoliticsCards );

        this.region.removeBusinessCard(this.selectedBusinessCard);

        gameStatus.addDiscardedPoliticsCards( this.selectedPoliticsCards );

        if(this.selectedBusinessCard.hasReward())
            this.selectedBusinessCard.getReward().apply(player, gameStatus);
    }

    @Override
    public String getDescription() {
        return "used " + this.selectedPoliticsCards.size() + " politics cards and paid " + this.corruptionCost + " coins to buy a business card in " + this.region.getName() + " region";
    }
}
