package it.stepit.server.model.actions;


/**
 * QuickAction that allows the player to shuffle the selected deck of businessCard and replace
 * the uncovered businesscard with the two card on the top of the shuffled deck
 */

import it.stepit.cscommons.utils.RichBoolean;
import it.stepit.server.model.GameStatus;
import it.stepit.server.model.Player;
import it.stepit.server.model.map.Region;

import java.util.List;

public class ChangeBusinessCards extends QuickAction {
    private static final int ASSISTANTS_COST = 1;
    private static final int CARDS_TO_CHANGE = 2;
    private Region region;

    public ChangeBusinessCards(List<Object> params){
        this( (Region) params.get(0));
    }

    public ChangeBusinessCards(Region region){
        this.region = region;
    }

    @Override
    public RichBoolean check(Player player, GameStatus gameStatus){
        if( !super.check(player,gameStatus).getBooleanValue() )
            return super.check(player, gameStatus);

        if( !region.canSwitchBusinessCards( CARDS_TO_CHANGE ) )
            return new RichBoolean(false, "Unable to change business cards in this region");

        if( player.getAssistants() < ASSISTANTS_COST )
            return new RichBoolean(false, "You need " + ASSISTANTS_COST +  " assistant(s) to change business cards");

        return new RichBoolean(true);
    }

    @Override
    public void performAction(Player player, GameStatus gameStatus) {
        player.setAssistants(player.getAssistants() - ASSISTANTS_COST);
        gameStatus.setFreeAssistants(gameStatus.getFreeAssistants()+1);

        this.region.switchBusinessCards( CARDS_TO_CHANGE );
    }

    @Override
    public String getDescription() {
        return "changed business cards in " +  this.region.getName() + " region";
    }

}
