package it.stepit.server.model.actions;

import it.stepit.cscommons.utils.RichBoolean;
import it.stepit.server.model.GameStatus;
import it.stepit.server.model.Player;

import java.util.List;

/**
 * permits the current player to do one more main action
 */
public class DoOneMoreMainAction extends QuickAction {
    private static final int ASSISTANTS_COST = 3;

    public DoOneMoreMainAction(List<Object> params){
        //No params needed
    }

    @Override
    public RichBoolean check(Player player, GameStatus gameStatus){
        if( !super.check(player,gameStatus).getBooleanValue() )
            return super.check(player, gameStatus);

        if(player.getAssistants() < ASSISTANTS_COST)
            return new RichBoolean(false, "You need " + ASSISTANTS_COST +  " to one more main action");

        return new RichBoolean(true);
    }

    @Override
    public void performAction(Player player, GameStatus gameStatus) {
        player.setAssistants( player.getAssistants() + ASSISTANTS_COST  );
        gameStatus.setFreeAssistants( gameStatus.getFreeAssistants() + ASSISTANTS_COST );
        gameStatus.setMainActions( gameStatus.getMainActions() + 1);
    }

    @Override
    protected void onActionApplied(Player player, GameStatus gameStatus) {
        gameStatus.onQuickActionApplied(this);
    }

    @Override
    public String getDescription() {
        return "payied " + ASSISTANTS_COST + " assistants to do one more main action";
    }

}
