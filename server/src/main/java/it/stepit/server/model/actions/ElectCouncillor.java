package it.stepit.server.model.actions;

import it.stepit.cscommons.utils.RichBoolean;
import it.stepit.server.model.Balcony;
import it.stepit.server.model.Councillor;
import it.stepit.server.model.GameStatus;
import it.stepit.server.model.Player;

import java.util.List;

/**
 * GameAction implementation for ElectCouncillor.
 * Receives as inputs THE BALCONY and THE COUNCILLOR to INSERT.
 * The given councillor is put in the first position of the balcony while the last on
 * is pushed into freeCouncilors.
 * The player receives a PLAYER_REWARD_FOR_ELECTION amount of coins.
 */
public class ElectCouncillor extends MainAction {
    private static final int PLAYER_COINS_REWARD_FOR_ELECTION = 4;
    private Balcony balcony;
    private Councillor councillor;

    public ElectCouncillor(List<Object> params){
        this( (Balcony) params.get(0), (Councillor) params.get(1) );
    }

    public ElectCouncillor(Balcony balcony, Councillor councillor) {
        this.balcony = balcony;
        this.councillor = councillor;
    }

    @Override
    public RichBoolean check(Player player, GameStatus gameStatus) {
        if( !super.check(player, gameStatus).getBooleanValue() )
            return super.check(player, gameStatus);

        if( !gameStatus.checkFreeCouncillor( this.councillor ) )
            return new RichBoolean(false);

        return new RichBoolean(true);
    }

    @Override
    protected void performAction(Player player, GameStatus gameStatus) {
        gameStatus.removeFreeCouncillor(councillor);
        Councillor removedCouncillor = this.balcony.shiftCouncillors( councillor );
        gameStatus.addFreeCouncilor( removedCouncillor );
        player.setCoins(player.getCoins() + PLAYER_COINS_REWARD_FOR_ELECTION );
    }

    @Override
    public String getDescription() {
        return "elected a councillor and received " + PLAYER_COINS_REWARD_FOR_ELECTION + " coins";
    }
}
