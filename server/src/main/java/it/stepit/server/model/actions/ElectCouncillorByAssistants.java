package it.stepit.server.model.actions;

import it.stepit.cscommons.utils.RichBoolean;
import it.stepit.server.model.Balcony;
import it.stepit.server.model.Councillor;
import it.stepit.server.model.GameStatus;
import it.stepit.server.model.Player;

import java.util.List;

/**
 *this class implements the QuickAction that allows the player to elect a councillor
 *  in a specified balcony, discarding an assistant
 */
public class ElectCouncillorByAssistants extends QuickAction {
    private static final int ASSISTANTS_COST = 1;
    private Balcony balcony;
    private Councillor councillor;

    public ElectCouncillorByAssistants(List<Object> params){
        this((Balcony) params.get(0), (Councillor) params.get(1));
    }

    public ElectCouncillorByAssistants(Balcony balcony, Councillor councillor){
        this.balcony=balcony;
        this.councillor=councillor;
    }

    @Override
    public RichBoolean check(Player player, GameStatus gameStatus){

        if( !super.check(player,gameStatus).getBooleanValue() )
            return super.check(player, gameStatus);

        if( player.getAssistants() < ASSISTANTS_COST )
            return new RichBoolean(false, "You need " + ASSISTANTS_COST +  " to elect a councillor");

        if( !gameStatus.checkFreeCouncillor(councillor) )
            return new RichBoolean(false);

        return new RichBoolean(true);

    }
    @Override
    public void performAction(Player player, GameStatus gameStatus) {
        player.setAssistants( player.getAssistants() - ASSISTANTS_COST );
        gameStatus.setFreeAssistants( gameStatus.getFreeAssistants() + ASSISTANTS_COST );
        this.balcony.shiftCouncillors(councillor);
    }

    @Override
    public String getDescription() {
        return "elected a councillor paying " + ASSISTANTS_COST + " assistant(s)";
    }
}
