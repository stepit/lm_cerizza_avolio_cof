package it.stepit.server.model.actions;

import it.stepit.cscommons.utils.RichBoolean;
import it.stepit.server.model.GameStatus;
import it.stepit.server.model.Player;

import java.util.List;

/**
 * This class implements the quickAction that allows the player to buy
 * an assistant for three coins
 */
public class EngageAssistant extends QuickAction {
    private static final int COINS_COST = 3;

    public EngageAssistant(List<Object> params){
        //No params needed
    }

    @Override
    public RichBoolean check(Player player, GameStatus gameStatus){
        
        if (!super.check(player,gameStatus).getBooleanValue() )
            return super.check(player, gameStatus);

        if(gameStatus.getFreeAssistants() == 0)
            return new RichBoolean(false, "There are no more free assistants");

        if(player.getCoins() < COINS_COST)
            return new RichBoolean(false, "You need " + COINS_COST + " coins to engage an assistant");

        return new RichBoolean(true);
    }

    @Override
    public void performAction(Player player, GameStatus gameStatus) {

        player.setCoins(player.getCoins() - COINS_COST);
        player.setAssistants(player.getAssistants() + 1);
        gameStatus.setFreeAssistants(gameStatus.getFreeAssistants() - 1);
    }

    @Override
    public String getDescription() {
        return "engaged an assistatn paying " + COINS_COST + " coins";
    }
}
