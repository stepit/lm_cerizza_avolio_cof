package it.stepit.server.model.actions;

import it.stepit.cscommons.utils.RichBoolean;
import it.stepit.server.exceptions.GameModelException;
import it.stepit.server.model.GameStatus;
import it.stepit.server.model.Player;

/**
 *  This abstract class provided a structure for game actions (main and quick ones).
 *  performAction should implement the logic of the action and should be private.
 *  apply is final and cannot be overrided: this metod first checks that check was called,
 *  then performs the action and the calls gameStatus listener for action complete.
 */
public abstract class GameAction {

    public RichBoolean check(Player player, GameStatus gameStatus) {
        if( player == null || gameStatus == null )
            return new RichBoolean(false);

        if(gameStatus.getCurrentPlayer() != null && !gameStatus.getCurrentPlayer().equals(player))
            return new RichBoolean(false, "It's not your turn!");

        if(gameStatus.isInMarketMode())
            return new RichBoolean(false, "You can't take a game action during market mode");

        return new RichBoolean(true);
    }

    public final void apply(Player player, GameStatus gameStatus){
        if( !this.check(player, gameStatus).getBooleanValue() )
            throw new GameModelException("Unable to performAction GameAction. Please use check() before apply()");

        this.performAction(player, gameStatus);
        this.onActionApplied(player, gameStatus);
    }

    protected abstract void performAction(Player player, GameStatus gameStatus);

    protected abstract void onActionApplied(Player player, GameStatus gameStatus);

    public abstract String getDescription();
}
