package it.stepit.server.model.actions;

import it.stepit.cscommons.utils.RichBoolean;
import it.stepit.server.model.GameStatus;
import it.stepit.server.model.Player;

abstract class MainAction extends GameAction{

    @Override
    public RichBoolean check(Player player, GameStatus gameStatus) {
        if(!super.check(player, gameStatus).getBooleanValue())
            return super.check(player, gameStatus);

        if (gameStatus.getMainActions() == 0)
            return new RichBoolean(false, "You alredy took all your main game action");

        return new RichBoolean(true);
    }

    @Override
    protected void onActionApplied(Player player, GameStatus gameStatus) {
        gameStatus.onMainActionApplied(this);
    }
}
