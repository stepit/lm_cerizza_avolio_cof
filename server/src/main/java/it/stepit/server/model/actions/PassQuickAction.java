package it.stepit.server.model.actions;

import it.stepit.cscommons.utils.RichBoolean;
import it.stepit.server.model.GameStatus;
import it.stepit.server.model.Player;

import java.util.List;

/**
 * This action is invocated when the player choose to pass the quickaction
 */
public class PassQuickAction extends QuickAction{

    public PassQuickAction(List<Object> params){
        //No params required
    }

    @Override
    public RichBoolean check(Player player, GameStatus gameStatus) {

        if(!super.check(player, gameStatus).getBooleanValue())
            return super.check(player, gameStatus);

        if( gameStatus.getMainActions() !=0 )
            return new RichBoolean(false, "You have still main actions to take");

        return new RichBoolean(true);
    }

    @Override
    public void performAction(Player player, GameStatus gameStatus) {
        //Nothing to do :) I'm just passing man!
    }

    @Override
    public String getDescription() {
        return "passed its turn";
    }
}
