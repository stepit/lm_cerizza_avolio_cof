package it.stepit.server.model.actions;

import it.stepit.cscommons.utils.RichBoolean;
import it.stepit.server.model.GameStatus;
import it.stepit.server.model.Player;

abstract class QuickAction extends GameAction{

    @Override
    public RichBoolean check(Player player, GameStatus gameStatus) {
        if(!super.check(player, gameStatus).getBooleanValue())
            return super.check(player, gameStatus);

        if (gameStatus.getQuickActions() == 0)
            return new RichBoolean(false, "You alredy took all your quick game action");

        return new RichBoolean(true);
    }

    @Override
    protected void onActionApplied(Player player, GameStatus gameStatus){
        gameStatus.onQuickActionApplied(this);
    }
}
