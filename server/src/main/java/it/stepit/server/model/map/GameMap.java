package it.stepit.server.model.map;

import com.google.common.collect.ImmutableMap;
import it.stepit.cscommons.communication.metaobjects.map.MetaGameMap;
import it.stepit.cscommons.communication.metaobjects.map.MetaRegion;
import it.stepit.cscommons.communication.metaobjects.map.MetaTown;
import it.stepit.server.model.GameStatus;
import it.stepit.server.model.Player;
import javafx.util.Pair;

import java.util.*;

public class GameMap {
    private final HashMap<String, Region> regions;
    private final HashMap<String, Town> towns;
    private final TownLinks townLinks;
    private String cliMap;

    public GameMap(Map<String, Region> regions, Map<String, Town> towns, TownLinks townLinks, String cliMap) {
        this.regions = new HashMap<>(regions);
        this.towns = new HashMap<>(towns);
        this.townLinks = townLinks;
        this.cliMap = cliMap;
    }

    public ImmutableMap<String, Town> getTowns() {
        return ImmutableMap.copyOf( this.towns );
    }

    public ImmutableMap<String, Region> getRegions(){
        return ImmutableMap.copyOf( this.regions );
    }

    /**
     * Given an origin town, a player and a gamestatus applies all the rewards
     * associated with the towns connected to the origin in which the player has built an emporium.
     *
     * @param origin The origin town
     * @param player The player to apply the rewards
     * @param gameStatus The gamestatus to apply the reward
     */
    public void applyRewardOfConnectedTowns(Town origin, Player player, GameStatus gameStatus){
        for(Town t : this.getConnectedTownsWithPlayerEmporium( origin, player ) ){
            if(t.hasReward())
                t.getRewardToken().apply( player, gameStatus );
        }
    }

    /**
     * Given a Town and a Player, this method returns a group of connected towns
     * in which the player has built an emporium.
     *
     * @param origin The origin town
     * @param player The player
     * @return  A set of connected towns with an emporium built by the player
     */
    public Set<Town> getConnectedTownsWithPlayerEmporium(Town origin, Player player){
        Set<Town> result = new HashSet<>();
        HashSet<Town> visited = new HashSet<>();
        ArrayDeque<Town> toVisitQueue = new ArrayDeque<>();
        toVisitQueue.addAll( this.townLinks.getDirectlyConnectedTowns(origin) );

        while( !toVisitQueue.isEmpty() ){
            Town t = toVisitQueue.pop();

            if( t.checkPlayerEmporium(player) && !visited.contains(t) ){
                result.add(t);
                Set<Town> ct = new HashSet<>( this.townLinks.getDirectlyConnectedTowns(t) );
                ct.removeAll(visited);
                toVisitQueue.addAll( ct );
            }

            visited.add(t);
        }

        return result;
    }

    /**
     * Given an origin and a destination town returns the number of towns
     * beetwen the two in the shortest path on the map. It returns null if
     * no path is possible beetween the towns.
     *
     * @param origin The origin town
     * @param destination The destination town
     * @return The path length if a path is possibile, null otherwise
     */
    public Optional<Integer> getPathLength(Town origin, Town destination){
        HashSet<Town> visited = new HashSet<>();
        ArrayDeque< Pair<Town, Integer> > toVisitQueue = new ArrayDeque<>();
        toVisitQueue.add( new Pair<>(origin, 0) );

        while( !toVisitQueue.isEmpty() ){
            Pair pair = toVisitQueue.pop();
            Town town = (Town) pair.getKey();
            int distance = (int) pair.getValue();

            if( town.equals(destination) ){
                return Optional.of(distance);
            }

            for( Town t: this.townLinks.getDirectlyConnectedTowns(town) ){
                if( !visited.contains(t) ){
                    toVisitQueue.add( new Pair<>(t, distance+1) );
                }
            }

            visited.add( town );
        }

        return Optional.empty();
    }

    public MetaGameMap buildMetaGameMap(){
        HashMap<String, MetaRegion> metaRegions = new HashMap<>();
        HashMap<String, MetaTown> metaTowns = new HashMap<>();
        for(Region r : this.regions.values()){
            metaRegions.put(r.getName(), r.buildMetaRegion());
        }
        for(Town t : this.towns.values()){
            metaTowns.put(t.getName(), t.buildMetaTown());
        }

        return new MetaGameMap(metaRegions, metaTowns, this.cliMap, false);
    }
}
