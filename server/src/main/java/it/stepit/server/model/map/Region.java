package it.stepit.server.model.map;

import com.google.common.collect.ImmutableCollection;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import it.stepit.cscommons.communication.IDableObject;
import it.stepit.cscommons.communication.metaobjects.MetaBalcony;
import it.stepit.cscommons.communication.metaobjects.MetaBonusCard;
import it.stepit.cscommons.communication.metaobjects.MetaBusinessCard;
import it.stepit.cscommons.communication.metaobjects.map.MetaRegion;
import it.stepit.cscommons.communication.metaobjects.map.MetaTown;
import it.stepit.server.exceptions.GameModelException;
import it.stepit.server.model.Balcony;
import it.stepit.server.model.BonusCard;
import it.stepit.server.model.BusinessCard;
import it.stepit.server.model.Player;

import javax.annotation.Nullable;
import java.util.*;

public class Region extends IDableObject{
    private final String name;
    private final ArrayList<BusinessCard> businessCards;
    private final Balcony balcony;
    private final HashMap<String, Town> towns;
    private BonusCard bonusCard;

    public Region(String name,@Nullable BonusCard bonusCard){
        if(name == null)
            throw new IllegalArgumentException("Illegal null value for name param");

        this.name = name;
        this.bonusCard = bonusCard;
        this.towns = new HashMap<>();
        this.businessCards = new ArrayList<>();
        this.balcony = new Balcony();
    }

    public String getName(){
        return this.name;
    }

    public void addTown(Town town){
        if(town == null)
            throw new IllegalArgumentException("Illegal null value for town param");

        this.towns.put(town.getName(), town);
    }

    public void addBusinessCards(Collection<BusinessCard> businessCards){
        if(businessCards == null)
            throw new IllegalArgumentException("Illegal null value for businessCards param");

        this.businessCards.addAll( businessCards );
    }

    public boolean hasBusinessCards(){
        return this.businessCards.isEmpty();
    }

    public Balcony getBalcony(){
        return this.balcony;
    }

    public ImmutableList<BusinessCard> getBusinessCards(){
        return ImmutableList.copyOf( this.businessCards );
    }

    public ImmutableCollection<Town> getTowns(){
        return ImmutableSet.copyOf(this.towns.values());
    }

    /**
     * Removes and returns the bonus card of the region.
     * @return  The bonus card of the region
     */
    public Optional<BonusCard> getBonusCard(){
        if( this.bonusCard != null ){
            Optional<BonusCard> returnValue = Optional.of(this.bonusCard);
            this.bonusCard = null;
            return returnValue;
        }

        return Optional.empty();
    }

    public boolean hasBonusCard(){
        return this.bonusCard != null;
    }

    /**
     * Checks if the given businessCard is buyable from the deck of the region
     * In other words, it checks if the card is the first or the second in the deck.
     *
     * @return  True if buyable, false if not
     */
    public boolean isBusinessCardBuyable(BusinessCard businessCard){
        return businessCard.equals( this.businessCards.get(0) ) ||
                businessCard.equals( this.businessCards.get(1) );
    }

    public void removeBusinessCard(BusinessCard businessCard){
        if(!this.isBusinessCardBuyable(businessCard))
            throw new GameModelException("Can't remove given business card from Region. Only buyable business cards can be removed.");

        this.businessCards.remove(businessCard);
    }

    public Town getTownByName(String townName){
        return this.towns.get(townName);
    }

    /**
     * Checks if it is possible to move the first two business cards of the region
     * to the bottom of the deck and show two new business cards
     *
     * @param cardsToSwitch The amount of cards to switch
     * @return True if the switch is possible, false otherwise
     */
    public boolean canSwitchBusinessCards(int cardsToSwitch){
        return this.businessCards.size() >= cardsToSwitch * 2;
    }

    /**
     * Switches an amount of cardsToSwitch from the top to the bottom
     * of the business cards queue.
     *
     * @param cardsToSwitch     The amount of cards to switch
     */
    public void switchBusinessCards(int cardsToSwitch) {
        if( !this.canSwitchBusinessCards(cardsToSwitch) )
            throw new GameModelException("Can't switch business cards in this region. Please use canSwitchBusinessCards() first.");

        for(int i = 0; i < cardsToSwitch; i++){
            this.businessCards.add( this.businessCards.remove(0) );
        }
    }

    /**
     * Checks if a player has built an emporium in each town of the region.
     * @param builder The player to check
     * @return True if the player has built an emporium in each town of the region, false otherwise
     */
    public boolean checkPlayerCompletion(Player builder){
        for(Town town : this.getTowns()){
            if( !town.checkPlayerEmporium(builder) ){
                return false;
            }
        }

        return true;
    }

    MetaRegion buildMetaRegion(){
        MetaBonusCard metaBonusCard = this.bonusCard != null ? this.bonusCard.buildMetaBonusCard() : null;
        MetaBalcony metaBalcony = this.balcony.buildMetaBalcony();
        HashMap<String, MetaTown> metaTowns = new HashMap<>();
        List<MetaBusinessCard> metaBusinessCards = new ArrayList<>();
        for(Town town : this.towns.values()){
            metaTowns.put(town.getName(), town.buildMetaTown() );
        }

        for(BusinessCard bc : this.businessCards){
            metaBusinessCards.add(bc.buildMetaBusinessCard());
        }

        return new MetaRegion(this.name, metaBonusCard, metaBusinessCards,
                                metaBalcony, metaTowns, this.getId());
    }
}
