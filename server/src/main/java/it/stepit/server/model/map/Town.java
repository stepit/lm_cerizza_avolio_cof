package it.stepit.server.model.map;

import it.stepit.cscommons.communication.IDableObject;
import it.stepit.cscommons.communication.metaobjects.MetaPlayer;
import it.stepit.cscommons.communication.metaobjects.map.MetaTown;
import it.stepit.server.exceptions.GameModelException;
import it.stepit.server.model.Player;
import it.stepit.server.model.Reward;
import javax.annotation.Nullable;
import java.awt.*;
import java.util.HashSet;

public class Town extends IDableObject {
    private final String name;
    private final Color color;
    private final HashSet<Player> emporiums;
    private Reward rewardToken;
    private Region region;

    public Town(String name, Color color, Region region){
        if(name == null)
            throw new IllegalArgumentException("Illegal null value for name param");

        if(color == null)
            throw new IllegalArgumentException("Illegal null value for color param");

        if(region == null)
            throw new IllegalArgumentException("Illegal null value for region param");

        this.name = name;
        this.color = color;
        this.region = region;
        this.emporiums= new HashSet<>();
    }

    public String getName() {
        return name;
    }

    public Color getColor() {
        return color;
    }

    public Reward getRewardToken() {
        return this.rewardToken;
    }

    public Region getRegion() {
        return region;
    }

    public void setRewardToken(@Nullable Reward rewardToken){
        this.rewardToken = rewardToken;
    }

    public boolean hasReward(){
        return this.rewardToken != null;
    }

    public boolean checkPlayerEmporium(Player p){
        return this.emporiums.contains(p);
    }

    public void buildEmporium(Player builder) {
        if(builder == null)
            throw new IllegalArgumentException("Illegal null value for builder param");

        if( this.emporiums.contains(builder) ){
            throw new GameModelException("This town already contains an emporium for player " + builder.toString());
        }

        this.emporiums.add(builder);
    }

    public int countBuiltEmporiums(){
        return this.emporiums.size();
    }

    public MetaTown buildMetaTown(){
        HashSet<MetaPlayer> metaEmporiums = new HashSet<>();
        for(Player p : this.emporiums){
            metaEmporiums.add( p.buildMetaPlayer() );
        }

        return new MetaTown(this.name,
                            this.color,
                            this.rewardToken != null ? this.rewardToken.buildMetaReward() : null,
                            metaEmporiums,
                            this.getId());
    }
}
