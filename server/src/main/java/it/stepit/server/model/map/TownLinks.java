package it.stepit.server.model.map;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;

/**
 * This class represents and support links between map towns.
 * Towns are represented as graph nodes. Towns connections are bidirectional by definition.
 */
public class TownLinks {
    private HashMap<Town, HashSet<Town>> connectionMap;

    public TownLinks(){
        this.connectionMap = new HashMap<>();
    }

    /**
     * Checks if exists a <b>direct</b> connection between two towns.
     *
     * @param town1 Origin town
     * @param town2 Destination town
     * @return True if a direct connection between towns exists. False otherwise or if towns are not in internal map rapresentation
     */
    public boolean checkConnection(Town town1, Town town2) {
        if(town1 == null)
            throw new IllegalArgumentException("Illegal null value for town1 param");

        if(town2 == null)
            throw new IllegalArgumentException("Illegal null value for town2 param");

        return connectionMap.get(town1).contains(town2) &&
                connectionMap.get(town2).contains(town1);
    }

    public void addConnection(Town town1, Town town2) {
        if(town1 == null)
            throw new IllegalArgumentException("Illegal null value for town1 param");

        if(town2 == null)
            throw new IllegalArgumentException("Illegal null value for town2 param");

        if( !connectionMap.containsKey(town1) ){
            connectionMap.put(town1, new HashSet<>() );
        }

        if( !connectionMap.containsKey(town2) ){
            connectionMap.put(town2, new HashSet<>() );
        }

        if( !connectionMap.get(town1).contains(town2) ){
            connectionMap.get(town1).add(town2);
        }

        if( !connectionMap.get(town2).contains(town1) ){
            connectionMap.get(town2).add(town1);
        }
    }

    public void removeConnection(Town town1, Town town2) {
        if( !connectionMap.containsKey(town1) || !connectionMap.containsKey(town2) ){
            return;
        }

        connectionMap.get(town1).remove(town2);
        connectionMap.get(town2).remove(town1);
    }

    /**
     * Returns towns directly connected to given town.
     *
     * @param origin The origin town
     * @return A collection of towns directly connected to given town
     */
    public Collection<Town> getDirectlyConnectedTowns(Town origin){
        if(origin == null)
            throw new IllegalArgumentException("Illegal null value for origin town");

        return this.connectionMap.get(origin);
    }
}
