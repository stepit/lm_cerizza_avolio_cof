package it.stepit.server.model.market;

import com.google.common.collect.ImmutableCollection;
import com.google.common.collect.ImmutableSet;
import it.stepit.cscommons.utils.RichBoolean;
import it.stepit.server.model.GameStatus;
import it.stepit.server.model.Player;
import it.stepit.server.utils.Utils;

import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * This class manages a marketMode session.
 * An instance of Market object should be instanciated foreach MarketMode session.
 */
public class Market {
    private static final int MARKETMODE_SELL_PHASE = 0;
    private static final int MARKETMODE_BUY_PHASE = 1;

    private static final long MARKETMODE_SELL_PHASE_DURATION = 50000L * 2;
    private static final long MARKETMODE_BUY_PHASE_DURATION = 50000L * 2;

    private final GameStatus gameStatus;
    private final Map<UUID, MarketOffer> offers;
    private int marketModePhase;
    private Deque<Player> playersOrder;
    private Player currentBuyer;

    public Market(GameStatus gameStatus) {
        this.marketModePhase = MARKETMODE_SELL_PHASE;
        this.gameStatus = gameStatus;
        this.offers=  new HashMap<>();

        new Timer().schedule(new java.util.TimerTask() {
            @Override
            public void run() {
                switchToBuyingMode();
            }}, MARKETMODE_SELL_PHASE_DURATION );
    }

    /*************
     * OBSERVERS *
     *************/

    private boolean canSell(){
        return this.marketModePhase == MARKETMODE_SELL_PHASE;
    }

    private boolean canBuy(){
        return this.marketModePhase == MARKETMODE_BUY_PHASE;
    }

    public ImmutableCollection<MarketOffer> getOffers(){
        return ImmutableSet.copyOf(this.offers.values());
    }

    /**
     * Returns the duration of market sell phase in <b>seconds</strong>
     *
     * @return Market Mode sell phase duration seconds
     */
    public long getSellPhaseDuration(){
        return TimeUnit.MILLISECONDS.toSeconds(MARKETMODE_SELL_PHASE_DURATION);
    }

    /**
     * Returns the duration of market buy phase in <b>seconds</strong> (per player)
     *
     * @return Market mode buy phase duration seconds per player
     */
    public long getBuyPhaseDuration(){
        return TimeUnit.MILLISECONDS.toSeconds(MARKETMODE_BUY_PHASE_DURATION);
    }

    /*************
     * MODIFIERS *
     *************/

    public RichBoolean publishOffer(MarketOffer newMarketOffer){
        if(newMarketOffer == null)
            throw new IllegalArgumentException("Illegal null value for newMarketOffer parm");

        if(this.canSell()){
            this.offers.put(newMarketOffer.getId(), newMarketOffer);
            return new RichBoolean(true, "Offer registered!");
        }else{
            return new RichBoolean(false, "You can't sell during this phase");
        }
    }

    public RichBoolean buyOfferRequest(Player player, UUID offerUUID){
        if(player == null)
            throw new IllegalArgumentException("Illegal null value for player param");

        MarketOffer offer = this.offers.get(offerUUID);
        if( offer != null ){

            if(player.equals( offer.getSeller() ))
                return new RichBoolean(false, "You can't buy your own market offer!");

            if( !this.canBuy() )
                return new RichBoolean(false, "You can't buy during this market phase!");

            if( !player.equals(this.currentBuyer))
                return new RichBoolean(false, "It's not your turn!");

            return this.buyOffer(player, offer);
        }

        return new RichBoolean(false, "Can't find requested offer");
    }

    private RichBoolean buyOffer(Player player, MarketOffer offer){
        boolean result;

        if(player.getCoins() < offer.getCost())
            return new RichBoolean(false, "You need " + offer.getCost() + " to buy this object from market");

        if(offer.getPoliticCardOnSale() != null)
            result = this.gameStatus.transferObject(offer.getSeller(), player, offer.getPoliticCardOnSale());
        else if(offer.getBusinessCardOnSale() != null)
            result = this.gameStatus.transferObject(offer.getSeller(), player, offer.getBusinessCardOnSale());
        else
            result = this.gameStatus.transferObject(offer.getSeller(), player, offer.getAssistantOnSale());

        if(result) {
            player.setCoins(player.getCoins() - offer.getCost());
            Player seller = offer.getSeller();
            seller.setCoins( seller.getCoins() + offer.getCost() );
        }

        this.offers.remove(offer.getId());
        this.gameStatus.onMarketUpdate( player.getUsername() + " bought " + offer.getSeller().getUsername() + " offer" );
        return new RichBoolean(result);
    }

    public RichBoolean pass(Player player){
        if(player == null)
            throw new IllegalArgumentException("Illegal null value for player param");

        if( !this.canBuy() )
            return new RichBoolean(false, "it's not market buy phase");

        if( !this.currentBuyer.equals(player) )
            return new RichBoolean(false, "It's not your turn");

        this.moveOn();
        return new RichBoolean(true);
    }

    private void switchToBuyingMode(){
        this.marketModePhase = MARKETMODE_BUY_PHASE;

        if( !this.offers.isEmpty() ){
            this.playersOrder = new ArrayDeque<>( this.gameStatus.getPlayers() );
            this.playersOrder = Utils.shuffleDeque(playersOrder);

            this.currentBuyer = this.playersOrder.pop();
            this.gameStatus.onMarketUpdate( "Starting market mode buying session! " + this.currentBuyer.getUsername() + " starts buying." );

            if(!this.currentBuyer.isConnected()){
                this.moveOn();
                return;
            }

            new Timer().schedule(new java.util.TimerTask() {
                @Override
                public void run() {
                    moveOn();
                }}, MARKETMODE_BUY_PHASE_DURATION );
        }else{
            this.gameStatus.onMarketUpdate( "Can't continue with market mode: no offers received" );
            this.gameStatus.onMarketModeComplete();
        }
    }

    private void moveOn(){
        if( !this.playersOrder.isEmpty() && !this.offers.isEmpty() ){
            this.currentBuyer = this.playersOrder.pop();
            if(!this.currentBuyer.isConnected()){
                this.moveOn();
                return;
            }

            this.gameStatus.onMarketUpdate( this.currentBuyer.getUsername() + " can buy now" );

            new Timer().schedule(new java.util.TimerTask() {
                @Override
                public void run() {
                    moveOn();
                }}, MARKETMODE_BUY_PHASE_DURATION );
        }else{
            this.gameStatus.onMarketModeComplete();
        }
    }
}
