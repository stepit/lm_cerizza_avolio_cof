package it.stepit.server.model.market;

import it.stepit.cscommons.communication.IDableObject;
import it.stepit.cscommons.communication.metaobjects.MetaBusinessCard;
import it.stepit.cscommons.communication.metaobjects.MetaMarketOffer;
import it.stepit.cscommons.communication.metaobjects.MetaPoliticCard;
import it.stepit.server.model.BusinessCard;
import it.stepit.server.model.Player;
import it.stepit.server.model.PoliticCard;

/**
 * Created by stefano on 04/06/2016.
 */
public class MarketOffer extends IDableObject{
    private final Player seller;
    private final BusinessCard businessCardOnSale;
    private final PoliticCard politicCardOnSale;
    private final int assistantOnSale;
    private final int cost;

    public MarketOffer(Player seller, PoliticCard politicCardOnSale, int cost) {
        if(politicCardOnSale == null)
            throw new IllegalArgumentException("Illegal null value for politicCardOnSale param");

        if(seller == null)
            throw new IllegalArgumentException("Illegal null value for seller param");

        if(cost < 0)
            throw new IllegalArgumentException("Illegal negative value cost param");

        this.businessCardOnSale = null;
        this.assistantOnSale = 0;
        this.seller = seller;
        this.politicCardOnSale = politicCardOnSale;
        this.cost = cost;
    }

    public MarketOffer(Player seller, BusinessCard businessCardOnSale, int cost) {
        if(businessCardOnSale == null)
            throw new IllegalArgumentException("Illegal null value for businessCardOnSale param");

        if(seller == null)
            throw new IllegalArgumentException("Illegal null value for seller param");

        if(cost < 0)
            throw new IllegalArgumentException("Illegal negative value cost param");

        this.politicCardOnSale = null;
        this.assistantOnSale = 0;
        this.seller = seller;
        this.businessCardOnSale = businessCardOnSale;
        this.cost = cost;
    }

    public MarketOffer(Player seller, int assistantOnSale, int cost) {
        if(assistantOnSale < 0)
            throw new IllegalArgumentException("Illegal negative value assistantsOnSale param");

        if(seller == null)
            throw new IllegalArgumentException("Illegal null value for seller param");

        if(cost < 0)
            throw new IllegalArgumentException("Illegal negative value cost param");

        this.politicCardOnSale = null;
        this.businessCardOnSale = null;
        this.seller = seller;
        this.assistantOnSale = assistantOnSale;
        this.cost = cost;
    }

    public int getCost() {
        return cost;
    }

    public int getAssistantOnSale() {
        return assistantOnSale;
    }

    public PoliticCard getPoliticCardOnSale() {
        return politicCardOnSale;
    }

    public BusinessCard getBusinessCardOnSale() {
        return businessCardOnSale;
    }

    public Player getSeller() {
        return seller;
    }

    public MetaMarketOffer buildMetaMarketOffer(){
        MetaBusinessCard metaBusinessCardOnSale = this.businessCardOnSale != null ? this.businessCardOnSale.buildMetaBusinessCard() : null;
        MetaPoliticCard metaPoliticCardOnSale = this.politicCardOnSale != null ? this.politicCardOnSale.buildMetaPoliticsCard() : null;

        return new MetaMarketOffer( this.seller.buildMetaPlayer(), metaBusinessCardOnSale, metaPoliticCardOnSale,
                                        this.assistantOnSale, this.cost, this.getId());
    }
}
