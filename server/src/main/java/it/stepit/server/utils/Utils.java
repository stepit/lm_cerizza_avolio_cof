package it.stepit.server.utils;

import java.util.*;

public class Utils {

    private Utils(){
        //Private constructor to hide default one
    }

    /**
     * Shuffles the given deque
     *
     * @param deque The deque to shuffle
     * @return The shuffled deque
     */
    public static <T> Deque<T> shuffleDeque(Deque<T> deque){
        List<T> l = new ArrayList<>(deque);
        Collections.shuffle(l, new Random( System.nanoTime() ));

        return new ArrayDeque<>(l);
    }
}
