package it.stepit.server.configuration;

import it.stepit.server.exceptions.GameConfigurationParseException;
import org.junit.Test;

import static org.junit.Assert.*;

public class GameConfigurationsManagerTest {
    @Test
    public void preloadDefaultConfiguration() throws Exception {
        GameConfigurationsManager.getInstance().preloadDefaultConfiguration("inbox.xml");
        GameConfigurationsManager.getInstance().getConfiguration("inbox.xml");

        Exception expectedException = null;

        try {
            GameConfigurationsManager.getInstance().preloadDefaultConfiguration("inesistente.xml");
        }catch(GameConfigurationParseException e){
            expectedException = e;
        }

        assertNotNull(expectedException);
    }
}