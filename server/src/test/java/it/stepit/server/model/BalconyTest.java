package it.stepit.server.model;

import org.junit.Test;

import java.awt.*;
import java.util.Collection;
import java.util.HashSet;

public class BalconyTest {

    @Test(expected = NullPointerException.class)
    public void testConstructorNullpointerExceptio(){
        new Balcony(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void textConstructorIllegalArgumentException(){
        Collection<Councillor> councillors = new HashSet<>();
        for(int i = 0; i < 6; i++){
            councillors.add( new Councillor( new Color(255, 255, 255)) );
        }

        new Balcony(councillors);
    }
}