package it.stepit.server.model;

import org.junit.Test;
import java.awt.*;
import static org.junit.Assert.*;

public class BonusCardTest {

    @Test(expected = IllegalArgumentException.class)
    public void testConstructorWithWrongParam(){
        new BonusCard(null, null);
    }

    @Test
    public void testHasRewardWithNullValue(){
        BonusCard bonusCard = new BonusCard( new Color(255, 255, 255), null );
        assertFalse(bonusCard.hasReward());
    }
}