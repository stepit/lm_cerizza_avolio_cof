package it.stepit.server.model;

import java.awt.*;
import it.stepit.server.model.map.Region;
import it.stepit.server.model.map.Town;
import org.junit.Test;
import static org.junit.Assert.*;

public class BusinessCardTest {

    @Test(expected = IllegalArgumentException.class)
    public void testConstructorWithNullValue(){
        new BusinessCard(null, null);
    }

    @Test
    public void testApplicability(){
        Town newTown = new Town("A", new Color(255, 255, 255), new Region("Ciao", null));
        Town newTown2 = new Town("B", new Color(255, 255, 255), new Region("Ciao", null));
        BusinessCard businessCard = new BusinessCard(new String[]{"A"}, null);

        assertTrue(businessCard.isApplicableToTown(newTown));
        assertFalse(businessCard.isApplicableToTown( newTown2 ));
    }

    @Test(expected = NullPointerException.class)
    public void TestApplicabilityWithNullValue(){
        BusinessCard businessCard = new BusinessCard(new String[]{"A"}, null);
        assertFalse(businessCard.isApplicableToTown(null));
    }
}
