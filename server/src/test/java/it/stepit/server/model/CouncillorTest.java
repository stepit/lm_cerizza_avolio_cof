package it.stepit.server.model;

import org.junit.Test;

import static org.junit.Assert.*;

public class CouncillorTest {

    @Test(expected = IllegalArgumentException.class)
    public void testConstructorWithNullValue(){
        new Councillor(null);
    }
}