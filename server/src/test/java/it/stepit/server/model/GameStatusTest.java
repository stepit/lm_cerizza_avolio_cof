package it.stepit.server.model;

import it.stepit.server.configuration.GameConfigurationsManager;
import it.stepit.server.model.actions.GameAction;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by stefano on 09/07/2016.
 */
public class GameStatusTest {

    GameStatus gs;


    public GameStatusTest(){

    }

    @Test
    public void addFreeCouncillorKo(){
        GameStatus gs = GameConfigurationsManager.getInstance().getConfiguration("inbox.xml");

        Councillor freeCouncillor=null;
        int i=0;
        for(Councillor c:gs.getFreeCouncillors()){
            if(i==0)
                freeCouncillor=c;
            i++;
        }
        try {
            gs.addFreeCouncilor(freeCouncillor);
        }catch(Exception e){
            assertEquals("The councillor is already present into freeCouncilors",e.getMessage());
        }
    }

    /**
     * test the insertion of too many players in a Game... catch the exception
     */
    @Test
    public void toManyPlayers() {
        GameStatus gs = GameConfigurationsManager.getInstance().getConfiguration("inbox.xml");

        ArrayList<Player> players= new ArrayList<>();
        for (int i = 0; i < 150; i++)
            players.add((new Player("p" + i, null)));
        try{
            gs.addPlayers(players);

        }catch (Exception e){
            assertTrue(e.getMessage().contains("This game configuration"));
        }

    }
    /**
     * test the insertion of too many players in a Game... catch the exception
     */
    @Test
    public void addPlayersOK() {
        GameStatus gs = GameConfigurationsManager.getInstance().getConfiguration("inbox.xml");

        ArrayList<Player> players= new ArrayList<>();
        for (int i = 0; i < 5; i++)
            players.add((new Player("p" + i, null)));

            gs.addPlayers(players);


    }

}