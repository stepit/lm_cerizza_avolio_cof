package it.stepit.server.model;

import it.stepit.server.model.map.Region;
import it.stepit.server.model.map.Town;
import org.junit.Test;
import java.awt.*;
import java.util.ArrayDeque;

import static org.junit.Assert.*;

public class KingTest {

    @Test(expected = NullPointerException.class)
    public void testConstructorWithNullBonusCards(){
        new King(null, new Town("A", new Color(255, 255, 255), new Region("Test", null)), 0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testConstructorWithNullCurrentTown(){
        new King(null, null, 0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testConstructorWithNegativeCostPerRoad(){
        new King( new ArrayDeque<>(), new Town("A", new Color(255, 255, 255), new Region("Test", null)), -1);
    }
}