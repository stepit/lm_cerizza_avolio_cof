package it.stepit.server.model.actions;

import com.google.common.collect.ImmutableList;
import it.stepit.server.configuration.GameConfigurationsManager;
import it.stepit.server.model.map.Town;
import it.stepit.server.model.BusinessCard;
import it.stepit.server.model.GameStatus;
import it.stepit.server.model.Player;
import it.stepit.server.model.Reward;
import org.junit.Test;

import java.awt.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by stefano on 28/05/2016.
 */
public class BuildEmporiumByBusinessCardTest {

    /**
     * test of an invalid BusinessCard for the selected Town
     * @throws  Exception
     **/
    @Test
    public void invalidBusinessCard() throws Exception {
        GameStatus gs = GameConfigurationsManager.getInstance().getConfiguration("inbox.xml");
        Player p= new Player("stefano",null);

        BusinessCard selectedBusinessCard = new BusinessCard( new String[]{"false"} , new Reward());
        selectedBusinessCard.setUsed(true);

        Town selectedTown = new Town("a",new Color(0,0,0), gs.getGameMap().getRegions().get("Sea"));

        GameAction buildEmporiumByBusinessCard = new BuildEmporiumByBusinessCard(selectedTown,selectedBusinessCard);
        gs.onEmporiumBuilt(p,selectedTown);
        assertFalse(buildEmporiumByBusinessCard.check(p,gs).getBooleanValue());

    }


    /**
     * test the selection of an used BusinessCard to build an emporium
     * @throws Exception
     */
    @Test
    public void usedBusinessCard() throws Exception {
        GameStatus gs = GameConfigurationsManager.getInstance().getConfiguration("inbox.xml");
        Player p= new Player("stefano",null);

        BusinessCard selectedBusinessCard = new BusinessCard(new String[]{"false"}, new Reward());
        selectedBusinessCard.setUsed(true);
        Town selectedTown = gs.getGameMap().getRegions().get("Sea").getTownByName("A");

        GameAction buildEmporiumByBusinessCard = new BuildEmporiumByBusinessCard(selectedTown,selectedBusinessCard);
        assertFalse(buildEmporiumByBusinessCard.check(p,gs).getBooleanValue());
    }

    /**
     * test when the player has already built his emporium in the selected town
     * @throws Exception
     */
    @Test
    public void emporiumAlreadyBuiltInTheSelectedTown() throws Exception {
        GameStatus gs = GameConfigurationsManager.getInstance().getConfiguration("inbox.xml");
        Player p= new Player("stefano",null);
        gs.addPlayer(p);

        BusinessCard selectedBusinessCard = gs.getGameMap().getRegions().get("Sea").getBusinessCards().get(0);
        String townName = ImmutableList.copyOf(selectedBusinessCard.getAvailableTownsNames()).get(0);
        selectedBusinessCard.setUsed(false);

        Town selectedTown =  gs.getGameMap().getRegions().get("Sea").getTownByName(townName);

        GameAction buildEmporiumByBusinessCard = new BuildEmporiumByBusinessCard(selectedTown,selectedBusinessCard);
        buildEmporiumByBusinessCard.apply(p,gs);
        gs.setMainActions(gs.getMainActions()+1);

        assertFalse(buildEmporiumByBusinessCard.check(p,gs).getBooleanValue());
    }

    /**
     * test the correctness of the results after the execution of the gameAction
     * -the player's emporium is now present in to the selected town
     * -decrease the number emporiums to build for the player
     * -the selectedBusinesscard is set as used
     * @throws Exception
     */
    @Test
    public void performAction() throws Exception {

        GameStatus gs = GameConfigurationsManager.getInstance().getConfiguration("inbox.xml");
        Player p= new Player("stefano",null);
        gs.addPlayer(p);
        int emporiumToWin= p.getEmporiumsToWin();

        BusinessCard selectedBusinessCard = gs.getGameMap().getRegions().get("Sea").getBusinessCards().get(0);
        String townName = ImmutableList.copyOf(selectedBusinessCard.getAvailableTownsNames()).get(0);
        selectedBusinessCard.setUsed(false);

        Town selectedTown =  gs.getGameMap().getRegions().get("Sea").getTownByName(townName);

        //assertFalse(selectedTown.checkPlayerEmporium(p));

        GameAction buildEmporiumByBusinessCard = new BuildEmporiumByBusinessCard(selectedTown,selectedBusinessCard);
        buildEmporiumByBusinessCard.apply(p,gs);

        assertTrue(selectedTown.checkPlayerEmporium(p));
        assertEquals(p.getEmporiumsToWin(),emporiumToWin-1);
        assertTrue(selectedBusinessCard.isUsed());
    }

}