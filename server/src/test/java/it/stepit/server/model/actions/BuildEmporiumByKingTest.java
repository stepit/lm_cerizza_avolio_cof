package it.stepit.server.model.actions;

import it.stepit.server.configuration.GameConfigurationsManager;
import it.stepit.server.model.GameStatus;
import it.stepit.server.model.Player;
import it.stepit.server.model.PoliticCard;
import org.junit.Test;

import java.awt.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

import static org.junit.Assert.*;

/**
 * Created by stefano on 05/07/2016.
 */
public class BuildEmporiumByKingTest {

    /**
     * test the uncorrected selection of PoliticsCards
     * @throws Exception
     */
    @Test
    public void wrongPoliticsCardSelected() throws Exception {

        GameStatus gs = GameConfigurationsManager.getInstance().getConfiguration("inbox.xml");
        Player p= new Player("stefano",null);
        gs.addPlayer(p);

        Collection<PoliticCard> selectedCards = new HashSet<>();
        selectedCards.add(new PoliticCard(false, new Color(255,0,0)));
        selectedCards.add(new PoliticCard(false, new Color(88,55,0)));
        GameAction buildEmporiumByKing = new BuildEmporiumByKing(gs.getKing(), gs.getGameMap().getRegions().get("Plain").getTownByName("J"), (HashSet<PoliticCard>) selectedCards);

        assertFalse(buildEmporiumByKing.check(p,gs).getBooleanValue());

    }

    /**
     *  test when the player has already built the emporium in the selected town
     * @throws Exception
     */
    @Test
    public void emporiumAlreadyBuilt() throws Exception {

        GameStatus gs = GameConfigurationsManager.getInstance().getConfiguration("inbox.xml");
        Player p= new Player("stefano",null);
        p.setCoins(100);
        gs.addPlayer(p);

        Collection<PoliticCard> selectedCards = new HashSet<>();
        selectedCards.add(new PoliticCard(false, gs.getKing().getBalcony().getCouncillors().get(0).getColor()));

        GameAction buildEmporiumByKing = new BuildEmporiumByKing(gs.getKing(), gs.getGameMap().getRegions().get("Plain").getTownByName("H"), (HashSet<PoliticCard>) selectedCards);

        buildEmporiumByKing.check(p,gs).getBooleanValue();
        buildEmporiumByKing.performAction(p, gs);


        assertFalse(buildEmporiumByKing.check(p,gs).getBooleanValue());
    }

    /**
     * /test when the player has no assistants enough to build an emporium where an other player did it
     * @throws Exception
     */
    @Test
    public void buildWhereOtherPlayers() throws Exception {

        GameStatus gs = GameConfigurationsManager.getInstance().getConfiguration("inbox.xml");
        Player p= new Player("stefano",null);
        gs.addPlayer(p);
        gs.getPlayers().get(0).setAssistants(0);

        Player p2= new Player("pietro",null);
        gs.addPlayer(p2);


        Collection<PoliticCard> selectedCards = new HashSet<>();
        selectedCards.add(new PoliticCard(false, gs.getKing().getBalcony().getCouncillors().get(0).getColor()));

        GameAction buildEmporiumByKing = new BuildEmporiumByKing(gs.getKing(), gs.getGameMap().getRegions().get("Plain").getTownByName("H"), (HashSet<PoliticCard>) selectedCards);

        buildEmporiumByKing.check(p2,gs);
        buildEmporiumByKing.performAction(p2,gs);

        assertFalse(buildEmporiumByKing.check(p,gs).getBooleanValue());
    }
   /**
    *  test when the player has not coins enough to pay the movement of the king to the selected newTown after the emporium is built
    *  @throws Exception
    * */
    @Test
    public void pathTooLongToPay() throws Exception {

        GameStatus gs = GameConfigurationsManager.getInstance().getConfiguration("inbox.xml");
        Player p= new Player("stefano",null);
        p.setCoins(10);
        gs.addPlayer(p);

        Collection<PoliticCard> selectedCards = new HashSet<>();
        selectedCards.add(new PoliticCard(false, gs.getKing().getBalcony().getCouncillors().get(0).getColor()));

        GameAction buildEmporiumByKing = new BuildEmporiumByKing(gs.getKing(), gs.getGameMap().getRegions().get("Sea").getTownByName("A"), (HashSet<PoliticCard>) selectedCards);

        assertFalse(buildEmporiumByKing.check(p,gs).getBooleanValue());
    }

    /**
     * test the correctness of the results after the execution of the gameAction:
     * -decrease the number of emporium that the player has to build
     * -increase the number of emporium built from the player
     * -the player's emporium is now present into the town
     * - the PoliticsCard that are used are not present into player's hand now
     * @throws Exception
     */
    @Test
    public void performAction() throws Exception {

        GameStatus gs = GameConfigurationsManager.getInstance().getConfiguration("inbox.xml");
        Player p= new Player("stefano",null);
        p.setCoins(10);
        gs.addPlayer(p);

        Collection<PoliticCard> selectedCards = new HashSet<>();
        PoliticCard firstCard;
        PoliticCard secondCard;
        selectedCards.add(firstCard = new PoliticCard(false, gs.getKing().getBalcony().getCouncillors().get(0).getColor()));
        selectedCards.add(secondCard = new PoliticCard(false, gs.getKing().getBalcony().getCouncillors().get(1).getColor()));

        GameAction buildEmporiumByKing = new BuildEmporiumByKing(gs.getKing(), gs.getGameMap().getRegions().get("Plain").getTownByName("I"), (HashSet<PoliticCard>) selectedCards);

        int emporiumsBefore= gs.getKing().getCurrentTown().countBuiltEmporiums();
        int emporiumsToBuildBefore=p.getEmporiumsToWin();

        buildEmporiumByKing.apply(p,gs);
        assertEquals(gs.getKing().getCurrentTown().countBuiltEmporiums(),emporiumsBefore+1);
        assertEquals(p.getEmporiumsToWin(),emporiumsToBuildBefore-1);
        assertTrue(gs.getKing().getCurrentTown().checkPlayerEmporium(p));
        assertTrue( !p.getPoliticCards().contains(firstCard) && !p.getPoliticCards().contains(secondCard));

    }

}