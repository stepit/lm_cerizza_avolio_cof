package it.stepit.server.model.actions;

import it.stepit.server.model.PoliticCard;
import it.stepit.server.model.map.Region;
import it.stepit.server.configuration.GameConfigurationsManager;
import it.stepit.server.model.BusinessCard;
import it.stepit.server.model.GameStatus;
import it.stepit.server.model.Player;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class BuyBusinessCardTest {
    GameStatus gs;
    Player p;
    ArrayList<PoliticCard> politicsCards = new ArrayList<>();
    Region r;
    BusinessCard businessCard;

    @Before
    public void init() throws Exception {
        gs = GameConfigurationsManager.getInstance().getConfiguration("inbox.xml");
        p = new Player("pietro", null);
        gs.addPlayer( p );
        r = gs.getGameMap().getRegions().get("Sea");
        businessCard = r.getBusinessCards().get(0);

        for( int i = 0; i < 4; i++){
            PoliticCard newPoliticCard =  new PoliticCard(false, r.getBalcony().getCouncillors().get(i).getColor());
            politicsCards.add(newPoliticCard );
            p.addPoliticCard( newPoliticCard );
        }
    }

    /**
     * test the success of the check method when the BusinessCard is buyable because of
     * Coins and PoliticsCards
     * @throws Exception
     */

    @Test
    public void buyableBusinessCard() throws Exception {
        GameAction buyBusinessCard = new BuyBusinessCard(r, businessCard, politicsCards);
        p.setCoins( 999 );

        assertTrue( buyBusinessCard.check(p, gs).getBooleanValue() );
    }

    /**
     * test when the player has not coin enough to buy the BusinessCard
     * @throws Exception
     */
    @Test
    public void unbuyableBusinessCard() throws Exception {
        p.setCoins( 0 );
        politicsCards.remove(0);
        GameAction buyBusinessCard = new BuyBusinessCard(r, businessCard, politicsCards);

        assertFalse( buyBusinessCard.check(p, gs).getBooleanValue() );
    }

    @Test
    public void performAction() throws Exception {

        GameAction buyBusinessCard = new BuyBusinessCard(r, businessCard, politicsCards);
        p.setCoins( 999 );
        buyBusinessCard.apply(p,gs);
        assertTrue(p.getBusinessCards().contains(businessCard));
        //assertFalse(r.getBusinessCards().contains(businessCard));
    }
}