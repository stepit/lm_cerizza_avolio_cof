package it.stepit.server.model.actions;

import it.stepit.server.configuration.GameConfigurationsManager;
import it.stepit.server.model.BusinessCard;
import it.stepit.server.model.GameStatus;
import it.stepit.server.model.Player;
import it.stepit.server.model.map.Region;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;

/**
 * Created by stefano on 30/05/2016.
 */
public class ChangeBusinessCardsTest {

    /**
     * test when player has not assistants enough to apply this action
     * @throws Exception
     */
    @Test
    public void noAssistatsEnough() throws Exception {//no assistant enough
        GameStatus gs = GameConfigurationsManager.getInstance().getConfiguration("inbox.xml");
        Player p= new Player("stefano",null);
        Region selectedRegion= gs.getGameMap().getRegions().get("Sea");

        GameAction changeBusinessCard= new ChangeBusinessCards(selectedRegion);
        assertFalse(changeBusinessCard.check(p,gs).getBooleanValue());
    }

    /**
     * test the correctness of the results after the execution of the gameAction
     * @throws Exception
     */
    @Test
    public void performAction() throws Exception {
        GameStatus gs = GameConfigurationsManager.getInstance().getConfiguration("inbox.xml");
        Player p= new Player("stefano",null);
        gs.addPlayer(p);
        p.setAssistants(5);
        Region selectedRegion= gs.getGameMap().getRegions().get("Sea");
        BusinessCard firstCard=selectedRegion.getBusinessCards().get(0);
        BusinessCard secondCard=selectedRegion.getBusinessCards().get(1);

        GameAction changeBusinessCard= new ChangeBusinessCards(selectedRegion);
        changeBusinessCard.apply(p,gs);

        assertNotEquals(firstCard,selectedRegion.getBusinessCards().get(0));
        assertNotEquals(secondCard,selectedRegion.getBusinessCards().get(1));

    }

}