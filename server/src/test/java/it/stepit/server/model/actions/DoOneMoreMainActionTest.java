package it.stepit.server.model.actions;

import it.stepit.server.configuration.GameConfigurationsManager;
import it.stepit.server.model.GameStatus;
import it.stepit.server.model.Player;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by stefano on 30/05/2016.
 */
public class DoOneMoreMainActionTest {
    private static final int ASSISTANTS_COST = 3;

    /**
     * test when the player has not assistants enough to apply the action
     * @throws Exception
     */
    @Test
    public void noAssistantsEnough() throws Exception {

        GameStatus gs = GameConfigurationsManager.getInstance().getConfiguration("inbox.xml");
        Player p= new Player("stefano",null);

        p.setAssistants(1);
        DoOneMoreMainAction doOneMoreMainAction= new DoOneMoreMainAction(null);

        assertFalse(doOneMoreMainAction.check(p,gs).getBooleanValue());
    }

    /**
     * test when the player can actually perform the action because he has assistants enough
     * @throws Exception
     */
    @Test
    public void actionExecutable() throws Exception {

        GameStatus gs = GameConfigurationsManager.getInstance().getConfiguration("inbox.xml");
        Player p= new Player("stefano",null);

        p.setAssistants(3);
        DoOneMoreMainAction doOneMoreMainAction= new DoOneMoreMainAction(null);

        assertTrue(doOneMoreMainAction.check(p,gs).getBooleanValue());
    }

    /**
     * test the correctness of the results after the execution of the gameAction
     * @throws Exception
     */
    @Test
    public void performAction() throws Exception {
        GameStatus gs = GameConfigurationsManager.getInstance().getConfiguration("inbox.xml");
        Player p= new Player("stefano",null);
        gs.addPlayer(p);
        p.setAssistants(3);
        DoOneMoreMainAction doOneMoreMainAction= new DoOneMoreMainAction(null);
        int previousMainAction= gs.getMainActions();

        doOneMoreMainAction.apply(p,gs);

        assertEquals(previousMainAction+1, gs.getMainActions());
    }

}