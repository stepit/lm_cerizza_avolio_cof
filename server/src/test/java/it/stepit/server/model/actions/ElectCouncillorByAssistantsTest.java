package it.stepit.server.model.actions;

import it.stepit.server.configuration.GameConfigurationsManager;
import it.stepit.server.model.Balcony;
import it.stepit.server.model.Councillor;
import it.stepit.server.model.GameStatus;
import it.stepit.server.model.Player;
import org.junit.Test;

import java.awt.*;

import static org.junit.Assert.*;

/**
 * Created by stefano on 30/05/2016.
 */
public class ElectCouncillorByAssistantsTest {

    /**
     * test when the player has not assistant enugh
     * @throws Exception
     */
    @Test
    public void noAssitantsenough() throws Exception {
        GameStatus gs = GameConfigurationsManager.getInstance().getConfiguration("inbox.xml");
        Player p= new Player("stefano",null);

        Balcony selectedBalcony= gs.getGameMap().getRegions().get("Sea").getBalcony();
        Councillor selectedCouncillor= new Councillor(new Color(0,0,0));

        for(Councillor c : gs.getFreeCouncillors()){
            selectedCouncillor=c;
            break;
        }

        p.setAssistants(0);

        ElectCouncillorByAssistants electCouncillorByAssistant=  new ElectCouncillorByAssistants(selectedBalcony,selectedCouncillor);
        assertFalse(electCouncillorByAssistant.check(p,gs).getBooleanValue());
    }

    /**
     * test when the gamestatus is empty of freeCouncillor ad the player can't select anyone
     * @throws Exception
     */
    @Test
    public void noFreeCouncillor() throws Exception {
        GameStatus gs = GameConfigurationsManager.getInstance().getConfiguration("inbox.xml");
        Player p= new Player("stefano",null);

        Balcony selectedBalcony= gs.getGameMap().getRegions().get("Sea").getBalcony();
        Councillor selectedCouncillor= new Councillor(new Color(45,0,0));

        p.setAssistants(3);

        ElectCouncillorByAssistants electCouncillorByAssistant=  new ElectCouncillorByAssistants(selectedBalcony,selectedCouncillor);
        assertFalse(electCouncillorByAssistant.check(p,gs).getBooleanValue());
    }

    /**
     * test when the action is executable from the player because he has assistants enough and has any mainActions to do
     * @throws Exception
     */
    @Test
    public void actionExecutable() throws Exception {
        GameStatus gs = GameConfigurationsManager.getInstance().getConfiguration("inbox.xml");
        Player p= new Player("stefano",null);
        Balcony selectedBalcony= gs.getGameMap().getRegions().get("Sea").getBalcony();

        Councillor selectedCouncillor= new Councillor(new Color(0,0,0));

        for(Councillor c : gs.getFreeCouncillors()){
            selectedCouncillor=c;
            break;
        }
        p.setAssistants(3);

        ElectCouncillorByAssistants electCouncillorByAssistant=  new ElectCouncillorByAssistants(selectedBalcony,selectedCouncillor);
        assertTrue(electCouncillorByAssistant.check(p,gs).getBooleanValue());
    }


    /**
     * test the correctness of the results after the execution of the gameAction
     * @throws Exception
     */
    @Test
    public void performAction() throws Exception {
        GameStatus gs = GameConfigurationsManager.getInstance().getConfiguration("inbox.xml");
        Player p= new Player("stefano",null);
        Balcony balconyBefore= gs.getGameMap().getRegions().get("Sea").getBalcony();
        p.setAssistants(3);
        int assistantBefore= p.getAssistants();

        Councillor selectedCouncillor= new Councillor(new Color(0,0,0));

        for(Councillor c : gs.getFreeCouncillors()){
            selectedCouncillor=c;
            break;
        }


        ElectCouncillorByAssistants electCouncillorByAssistant=  new ElectCouncillorByAssistants(gs.getGameMap().getRegions().get("Sea").getBalcony(),selectedCouncillor);
        balconyBefore.shiftCouncillors(selectedCouncillor);
        electCouncillorByAssistant.performAction(p,gs);
        assertEquals(balconyBefore,gs.getGameMap().getRegions().get("Sea").getBalcony());
        assertEquals(p.getAssistants(),assistantBefore-1);


    }

}