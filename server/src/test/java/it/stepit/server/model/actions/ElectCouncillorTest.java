package it.stepit.server.model.actions;

import it.stepit.server.configuration.GameConfigurationsManager;
import it.stepit.server.model.Balcony;
import it.stepit.server.model.Councillor;
import it.stepit.server.model.GameStatus;
import it.stepit.server.model.Player;
import org.junit.Test;
import java.awt.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class ElectCouncillorTest {
    /**
     * test when the action is not executable because gamestatus id empty of freeCouncillors7
     * and the player can't select anyone
     * @throws Exception
     */
    @Test
    public void noFreeCouncillor() throws Exception {
        GameStatus gs = GameConfigurationsManager.getInstance().getConfiguration("inbox.xml");
        Player p= new Player("stefano",null);

        Councillor c= new Councillor(new Color(255,102,0));

        GameAction electCouncillor = new ElectCouncillor(gs.getGameMap().getRegions().get("Sea").getBalcony(), c);
        assertFalse(electCouncillor.check(p,gs).getBooleanValue());

    }

    /**
     * test when the player has on mainAction to do
     * @throws Exception
     */
    @Test
    public void noAvailableMainAction() throws Exception{
        GameStatus gs = GameConfigurationsManager.getInstance().getConfiguration("inbox.xml");
        Player p= new Player("stefano",null);
        gs.setMainActions(0);

        Councillor councillor= new Councillor(new Color(0,0,0));
        for(Councillor c : gs.getFreeCouncillors()){
            councillor=c;
            break;
        }


        GameAction electCouncillor = new ElectCouncillor(gs.getGameMap().getRegions().get("Sea").getBalcony(), councillor);
        assertFalse(electCouncillor.check(p,gs).getBooleanValue());
    }

    /**
     * test check method when the action is executable
     * @throws Exception
     */
    @Test
    public void actionExecutable() throws Exception {
        GameStatus gs = GameConfigurationsManager.getInstance().getConfiguration("inbox.xml");
        Player p= new Player("stefano",null);

        Councillor councillor= new Councillor(new Color(0,0,0));
        for(Councillor c : gs.getFreeCouncillors()){
            councillor=c;
            break;
        }

        GameAction electCouncillor = new ElectCouncillor(gs.getGameMap().getRegions().get("Sea").getBalcony(),councillor);
        assertTrue(electCouncillor.check(p,gs).getBooleanValue());
    }
    /**
     * test the correctness of the results after the execution of the gameAction
     * @throws Exception
     */
    @Test
    public void performAction() throws Exception {
        GameStatus gs = GameConfigurationsManager.getInstance().getConfiguration("inbox.xml");
        Player p= new Player("stefano",null);
        gs.addPlayer(p);
        int mainAction=gs.getMainActions();


        Councillor councillor= new Councillor(new Color(0,0,0));
        for(Councillor c : gs.getFreeCouncillors()){
            councillor=c;
            break;
        }
        Balcony b= gs.getGameMap().getRegions().get("Sea").getBalcony();
        b.shiftCouncillors(councillor);

        GameAction electCouncillor = new ElectCouncillor(gs.getGameMap().getRegions().get("Sea").getBalcony(),councillor);
        electCouncillor.apply(p, gs);
        assertEquals(b, gs.getGameMap().getRegions().get("Sea").getBalcony());
        assertEquals(mainAction-1, gs.getMainActions());
    }
}