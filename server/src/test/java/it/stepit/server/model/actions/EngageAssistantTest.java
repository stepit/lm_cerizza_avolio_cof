package it.stepit.server.model.actions;

import it.stepit.server.configuration.GameConfigurationsManager;
import it.stepit.server.model.GameStatus;
import it.stepit.server.model.Player;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by stefano on 30/05/2016.
 */
public class EngageAssistantTest {

    /**
     * test when the player has not coins enough to buy an assistant
     * @throws Exception
     */
    @Test
    public void noCoinEnough() throws Exception {
        GameStatus gs = GameConfigurationsManager.getInstance().getConfiguration("inbox.xml");
        Player p= new Player("stefano",null);

        EngageAssistant engageAssistant= new EngageAssistant(null);
        p.setCoins(1);

        assertFalse(engageAssistant.check(p,gs).getBooleanValue());
    }

    /**
     * test when there are not freeAssistant to engage
     * @throws Exception
     */
    @Test
    public void noFreeAssistantsAvailable() throws Exception {
        GameStatus gs = GameConfigurationsManager.getInstance().getConfiguration("inbox.xml");
        Player p = new Player("stefano", null);

        EngageAssistant engageAssistant = new EngageAssistant(null);
        gs.setFreeAssistants(0);

        assertFalse(engageAssistant.check(p, gs).getBooleanValue());
    }
    /**
     * test the correctness of the results after the execution of the gameAction
     * @throws Exception
     */
    @Test
    public void performAction() throws Exception {
        GameStatus gs = GameConfigurationsManager.getInstance().getConfiguration("inbox.xml");
        Player p = new Player("stefano", null);
        gs.addPlayer(p);
        p.setCoins(4);
        int coinsBefore=p.getCoins();
        int assistantsBefore=p.getAssistants();

        EngageAssistant engageAssistant= new EngageAssistant(null);

        engageAssistant.apply(p,gs);

        assertEquals(p.getCoins(),coinsBefore-3);
        assertEquals(p.getAssistants(),assistantsBefore+1);


    }

}