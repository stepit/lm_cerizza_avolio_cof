package it.stepit.server.model.map;

import it.stepit.server.configuration.GameConfigurationsManager;
import it.stepit.server.model.GameStatus;
import it.stepit.server.model.Player;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashSet;
import java.util.Optional;

import static org.junit.Assert.*;


public class GameMapTest {
    @Test
    public void getConnectedTownsWithPlayerEmporium() throws Exception{
        GameStatus gs = GameConfigurationsManager.getInstance().getConfiguration("inbox.xml");

        Player mario = new Player("mario", null);
        Player giovanni = new Player("giovanni", null);

        gs.addPlayer(mario);
        gs.addPlayer(giovanni);

        HashSet<Town> expectedResult = new HashSet<>();
        expectedResult.add( gs.getGameMap().getTowns().get("B") );
        expectedResult.add( gs.getGameMap().getTowns().get("C") );
        expectedResult.add( gs.getGameMap().getTowns().get("D") );

        gs.getGameMap().getTowns().get("B").buildEmporium(mario);
        gs.getGameMap().getTowns().get("C").buildEmporium(mario);
        gs.getGameMap().getTowns().get("D").buildEmporium(mario);

        Assert.assertEquals(expectedResult, gs.getGameMap().getConnectedTownsWithPlayerEmporium( gs.getGameMap().getTowns().get("A"), mario));
        Assert.assertNotEquals(expectedResult, gs.getGameMap().getConnectedTownsWithPlayerEmporium( gs.getGameMap().getTowns().get("A"), giovanni));
    }

    @Test
    public void getPathLength() throws Exception {
        GameStatus gs = GameConfigurationsManager.getInstance().getConfiguration("inbox.xml");
        Town origin = gs.getGameMap().getTowns().get("A");
        Town destination = gs.getGameMap().getTowns().get("M");

        Optional<Integer> pathLength = gs.getGameMap().getPathLength(origin, destination);

        assertEquals( (int) 4, (int) pathLength.get() );
    }
}