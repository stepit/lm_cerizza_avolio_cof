package it.stepit.server.model.market;

import com.google.common.collect.ImmutableCollection;
import it.stepit.cscommons.utils.RichBoolean;
import it.stepit.server.configuration.GameConfigurationsManager;
import it.stepit.server.model.*;
import org.junit.Test;

import java.awt.*;
import java.util.*;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by stefano on 09/07/2016.
 */
public class MarketTest {
    GameStatus gs;
    Player pSeller, pBuyer;

    public MarketTest() {
        this.gs = GameConfigurationsManager.getInstance().getConfiguration("inbox.xml");
        this.pSeller = new Player("stefano", null);
        this.pBuyer = new Player("pietro", null);
    }

    /**
     * test the correct execution of PoliticsCard sell and verify that the offer is on Market
     * @throws Exception
     */
    @Test
    public void getOffersTestPoliticsCardOK() throws Exception {
        MarketOffer firstOffer;
        Market market = new Market(gs);
        market.publishOffer(firstOffer = new MarketOffer(pSeller, new PoliticCard(true, new Color(255, 255, 0)), 5));
        assertTrue(market.getOffers().contains(firstOffer));
    }

    /**
     * test the correct Exception of PoliticsCard sell when the cost is negative
     * @throws Exception
     */
    @Test
    public void getOffersTestPoliticsCardNoForCostNegative() throws Exception {
        MarketOffer firstOffer;
        Market market = new Market(gs);
        try{
            market.publishOffer(firstOffer = new MarketOffer(pSeller, new PoliticCard(true, new Color(255, 255, 0)), -5));
        }catch (Exception e){
            assertEquals("Illegal negative value cost param",e.getMessage());
        }
    }


    @Test
    public void buyOfferRequestTestNoForPhase() throws Exception {
        MarketOffer firstOffer;
        PoliticCard selectedCard;

        pBuyer.setCoins(100);
        Market market = new Market(gs);
        market.publishOffer(firstOffer = new MarketOffer(pSeller,
                selectedCard = new PoliticCard(true, new Color(255, 255, 0)), 5));
        assertFalse(market.buyOfferRequest(pBuyer, firstOffer.getId()).getBooleanValue());
    }

    /**
     * test the correct execution of PoliticsCard sell and verify that the offer is on Market
     * @throws Exception
     */
    @Test
    public void PublishOffersTestAssistantsOKNumber() throws Exception {
        MarketOffer firstOffer;
        Market market = new Market(gs);
        market.publishOffer(firstOffer = new MarketOffer(pSeller, 2  , 5));
        assertTrue(market.getOffers().contains(firstOffer));
    }
    /**
     * test the  exception thrown for an invalid negative number of assistants
     * @throws Exception
     */
    @Test
    public void PublishOffersTestAssistantsNoNumber() throws Exception {
        MarketOffer firstOffer;
        Market market = new Market(gs);
        try {
            gs.publishMarketOffer(firstOffer = new MarketOffer(pSeller, -2, 5));
        }catch (Exception e){
            assertEquals("Illegal negative value assistantsOnSale param",e.getMessage());
        }
    }
    /**
     * test the correct execution of Business sell and verify that the offer is on Market
     * @throws Exception
     */
    @Test
    public void getOffersTestBusinessCardOK() throws Exception {
        MarketOffer firstOffer;
        String towns="J H";
        BusinessCard selectedBusinessCard= new BusinessCard(towns.split(" "), new Reward());
        Market market = new Market(gs);
        market.publishOffer(firstOffer = new MarketOffer(pSeller, selectedBusinessCard  , 5));
        assertTrue(market.getOffers().contains(firstOffer));
    }
    /**
     * test the Exception Thrown for illegal seller (null)
     * @throws Exception
     */
    @Test
    public void PublishOffersTestBusinessCardNoForIllegalSeller() throws Exception {
        MarketOffer firstOffer;
        Market market = new Market(gs);
        try{market.publishOffer(firstOffer = new MarketOffer(null, 2  , 5));
            }
        catch (Exception e){
            assertEquals("Illegal null value for seller param",e.getMessage());
        }
    }
    /**
     * test the thrown when it's not MarketTime
     * @throws Exception
     */
    @Test
    public void notMarketTime() throws Exception {
        MarketOffer firstOffer;
        Market market = new Market(gs);
        try{gs.publishMarketOffer(firstOffer = new MarketOffer(pSeller, 2  , 5));}
        catch(Exception e){
            assertEquals("It's not market mode time!",e.getMessage());
        }
    }

}